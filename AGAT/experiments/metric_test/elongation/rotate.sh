#!/bin/bash
for file in $( ls |grep tif|cut -dA -f2); do
	echo $file 
  convert "$file" -rotate 90 -colorspace gray -type truecolor "${file%.tif}"_rotated.tif
done