package agat.tree;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.TreeSet;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.tc33.jheatchart.HeatChart;

import fr.unistra.pelican.ByteImage;
import fr.unistra.pelican.DoubleImage;
import fr.unistra.pelican.Image;
import fr.unistra.pelican.IntegerImage;
import fr.unistra.pelican.algorithms.histogram.Equalization;
import fr.unistra.pelican.algorithms.io.ImageSave;
import fr.unistra.pelican.algorithms.segmentation.labels.DrawFrontiersOnImage;
import fr.unistra.pelican.algorithms.segmentation.labels.FrontiersFromSegmentation;
import fr.unistra.pelican.algorithms.segmentation.labels.LabelsToRandomColors;
import agat.util.Log;
import agat.util.TreeSave;
import agat.util.image.TiffImage;
import agat.util.image.sbc.SegmentByConnexityRaw;
import agat.consensus.support.Consensus;
import agat.consensus.support.ConsensusFactory;
import agat.consensus.support.Consensus.ConsensusStrategy;
import agat.metric.support.Metric.TypeOfMetric;
import agat.tree.structure.Adjacency;
import agat.tree.structure.Node;
import agat.tree.structure.Node.TypeOfNode;
import agat.tree.structure.RAG;
import agat.tree.structure.ListW;

/**
 * 
 * MULTI-FEATURES BINARY PARTITION TREE
 * 
 * @author T.J.F.R.
 *
 */
public class MBPT {

	public enum Context{
		AGAT,
		MBPT,
		RAG_UPDATE,
		REGROWING,
		CUTTING
	}
	
	/**
	 * All attributes of the MBPT.
	 */
	public Node root; // ROOT OF THE TREE.
	public RAG rag; // REGION ADJACENCY GRAPH.
	public ArrayList<TiffImage> listOfImages; // LIST OF IMAGES LINKED TO THE TREE.	
	public List<ListW> listOfLists; // LIST OF ALL LISTS CORRESPONDING TO ALL COUPLES OF IMAGES AND METRICS.
	public int[][] indexationMatrix; // MATRIX HELPING THE LABELIZATION OF EACH REGIONS.
	public String mBptFileNamePath; // NAME OF THE FILE WHERE THE CREATION OF THE TREE IS STORED.
	public long fusionTime; // TIME OF FUSION IN MS.
	public TreeSave treeSave; // OBJECT WRITING THE BPT IN A FILE.
	public TiffImage preSegFile; // PRE-SEGMENTED IMAGE.
	public ArrayList<Consensus> consensusStrategies = new ArrayList<Consensus>(); // CONSENSUS STRATEGIES TO USE.
	public TreeSet<Node> activeNodes = new TreeSet<Node>(); // REMEMBER ACTIVE NODES.
	
	/**
	 * 
	 * Create an empty MBPT tree.
	 * 
	 * @param bptFileName Path and name of the output <b>.mbpt</b> file containing information about the tree.
	 * @param saving If <b> true </b>, the tree will be stored in a file during the creation. If <b> false </b> no saving process in engaged.
	 */
	public MBPT(String bptFileName, boolean saving){
		
		this.mBptFileNamePath = bptFileName;
		this.listOfImages = new ArrayList<TiffImage>();
		this.listOfLists = new ArrayList<ListW>();
		Log.printLvl1(String.valueOf(String.valueOf(Context.MBPT)), "Empty tree created!");
		
		/* init the tree saving system */
		this.treeSave = new TreeSave(this, saving);

	}

	/**
	 * Register an image as one of the source image.
	 * @param tiffImg
	 */
	public void registerImage(TiffImage tiffImg) {
		
		tiffImg.index = this.listOfImages.size();
		this.listOfImages.add(tiffImg);

	}

	/**
	 * Link a metric to an image and create the corresponding listW.
	 * @param tiffImg Identification of the image in the list of source images of the tree.
	 * @param metricType Type of metric to use.
	 */
	public void linkMetricToAnImage(TiffImage tiffImg, TypeOfMetric metricType) {
		
		ListW list = new ListW(this.listOfLists.size(), tiffImg, metricType);
		this.listOfLists.add(list);
		
	}
	
	/**
	 * Set the consensus strategy to use and precise if only a specified first ranks in the lists are considered.
	 * @param consensusStrategy Consensus strategy to use.
	 * @param consensusRange Range of the 1sts adjacencies to consider in all listW(s). (!) If this argument is not set, all of the list content (i.e. 100%) will be considered.
	 */
	public void setConsensusStrategy(ConsensusStrategy consensusStrategy, int... consensusRange) {
		
		int range;
		if(consensusRange.length > 0) range = consensusRange[0];
		else range = 100; // BY DEFAULT, CONSIDER 100% OF THE LIST CONTENT. 
		this.consensusStrategies.add(ConsensusFactory.buildonsensusStrategy(consensusStrategy, range));
		
	}

	/**
	 * Building the tree.
	 */
	public void grow() {

		Log.printLvl1(String.valueOf(String.valueOf(Context.MBPT)), "Number of listW(s): "+ this.listOfLists.size());
		
		/**
		 * Prepare the indexation matrix according to the 1st image
		 */
		this.indexationMatrix = new int[this.listOfImages.get(0).width][this.listOfImages.get(0).height];
		
		/**
		 * Fill the indexation matrix with the initial labels. (!) At this point, the initialization is from the pixels or labels from a pre-segmentation.
		 */
		if(this.preSegFile == null) this.preSegFromPixels();
		else this.preSegFromRandomColorFile();
		Log.printLvl1(String.valueOf(String.valueOf(Context.MBPT)), "Indexation matrix prepared!");
		
		/**
		 * Build the initial RAG (~ REGION ADJACENCY GRAPH)
		 */
		this.rag = new RAG(this);

		/*
		 * Updating ranks of the adjacencies in each list if the number of lists is more than one. 
		 */
		if(this.listOfLists.size() > 1)
			this.listOfLists
			.parallelStream()
			.forEach(lw->{
				int r = 1;
				Adjacency prevTmp = null;
				for(Entry<Adjacency, Integer> entry: lw.structure.entrySet()){
					Adjacency a = entry.getKey();
					a.ranks[lw.index] = r++;
					a.previous[lw.index] = prevTmp;
					if(prevTmp != null) prevTmp.next[lw.index] = a; 
					prevTmp = a;
				}
			});
		
		/***********************************************************************************************
		 * At this stage, all adjacencies should have already been stored in all listW(s).
		 ***********************************************************************************************/
		Log.printLvl2(String.valueOf(Context.MBPT), "Nb adja in RAG after its creation: "+ Adjacency.adjacencyCounter);
		Log.printLvl2(String.valueOf(Context.MBPT), "Nb adja in ListWs after RAG's creation: "+ this.listOfLists.get(0).getNumberOfContainedAdjacencies());

		/**
		 * Merge nodes (~ regions) while the list of adjacencies of the RAG is not empty. 
		 */

		int nbFusions = 0;
		int estimatedFusions = this.rag.nbLeaves - 1;
		float progression;
		ThreadMXBean fusionTimeThread = ManagementFactory.getThreadMXBean();
		long startingTime = fusionTimeThread.getCurrentThreadCpuTime();
		Log.printLvl1(String.valueOf(Context.MBPT), "Start node mergings!");
		/*
		 * Start saving nodes.
		 */
		this.treeSave.write("],\"nodes\":[");
		while(!this.listOfLists.get(0).structure.isEmpty()){

			Node newNode;
			Adjacency chosenAdja;
			Node leftNode;
			Node rightNode;
			List<Adjacency> listAdjaToDelete;
			List<Adjacency> listAdjaToAdd;
			double nodeMerginScore;
			
			/**
			 * Select the best adjacency that will determine the two nodes (~ regions) to merge.
			 */
			if(this.listOfLists.size() == 1){ // STANDARD BPT -> GET THE LAST ADJACENCY OF THE LIST WHICH MUST HAVE THE LOWER SCORE (~ DISTANCE) VALUE.
				chosenAdja = this.listOfLists.get(0).structure.lastKey();
				nodeMerginScore = 1.0;
			}else{// APPLY A CONSENSUAL STRATEGY THAT WILL HELP THE CHOICE.
				chosenAdja = this.consensusStrategies.get(0).apply(this.listOfLists); // CONSENSUS STRATEGY.
				nodeMerginScore = chosenAdja.consensusScore; // MERGING SCORE CHOSEN BY THE CONSENSUS STRATEGY.
			}
			
			/**
			 * Merge the neighboring nodes determined by the chosen adjacency.
			 * As a result, we get a new node (~ region).
			 */
			newNode = new Node(chosenAdja.leftNode, chosenAdja.rightNode, this.listOfLists);
			newNode.merginScore = nodeMerginScore; 
			this.rag.listOfNodes[newNode.index] = newNode;
			
			/**
			 * Remove the children from the list of active nodes and add the father in it.
			 */
			this.activeNodes.remove(chosenAdja.leftNode);
			this.activeNodes.remove(chosenAdja.rightNode);
			this.activeNodes.add(newNode);
			
			/*
			 * Save node.
			 */
			this.treeSave.write(new StringBuilder("{\"n\":").append(newNode.index).append(",\"l\":").append(chosenAdja.leftNode.index).append(",\"r\":").append(chosenAdja.rightNode.index).append(",\"s\":").append(newNode.merginScore).append(",\"ls\":").append(newNode.sizeOfRAGWhenMerged).append("}").toString());
			
			/***********************************************************************************************
			 * Update the RAG and all listW(s) contents.
			 * - Find the affected adjacencies.
			 * - Delete affected adjacencies.
			 * - Create new adjacencies.
			 ***********************************************************************************************/

			leftNode = chosenAdja.leftNode;
			rightNode = chosenAdja.rightNode;
			listAdjaToDelete = new ArrayList<Adjacency>();
			listAdjaToAdd = new ArrayList<Adjacency>();
			
			/**
			 * Treat firstly the chosen adjacency's left child's neighboring nodes (~ regions).
			 * Done in a parallel way.
			 */
			leftNode.neighborsLink.remove(chosenAdja); // REMOVE THE CHOSEN ADJACENCY DIRECTLY IN THE CHOSEN ADJACENCY'S LEFT NODE'S ADJACENCIES LIST.
			leftNode.neighborsLink
			.stream()
			.forEach(leftNodeAdja->{
				
				listAdjaToDelete.add(leftNodeAdja);
				this.rag.removeAdjacency(leftNodeAdja); // REMOVE ADJACENCY FROM THE RAG.
				
				/**
				 *  Identify the neighbor of the chosen adjacency's left node.
				 */
				Node leftNodeNeighbor;
				if(leftNodeAdja.leftNode.index == leftNode.index) leftNodeNeighbor = leftNodeAdja.rightNode;
				else leftNodeNeighbor = leftNodeAdja.leftNode;
				
				/**
				 * - Create a new adjacency between the new node and the affected region.
				 * - Add it in the RAG
				 * - Add it in the list of adjacencies to add. 
				 */
				boolean adjaSpecialTreat = true;
				if(this.listOfLists.size() == 1) adjaSpecialTreat = false;
				Adjacency newAdja = new Adjacency(leftNodeNeighbor, newNode, this.listOfLists, adjaSpecialTreat); // (!) THE NEW ADJACENCY IS ADDED AUTOMATICALLY IN ALL LISTW(S).
				this.rag.addAdjacency(newAdja); // (!) ADDING A NEW ADJACENCY IN THE RAG MUST BE DONE MANUALLY.
				listAdjaToAdd.add(newAdja);
				
				/**
				 * Let the neighbor forget the link (~ adjacency) between it and the left node.
				 * (!) It also has to be done at the other side (ie. remove the link in the list of neighbors of the left node object)
				 */
				leftNodeNeighbor.neighborsLink.remove(leftNodeAdja);	
				
			});

			/**
			 * Clear the adjacencies list allowing the link between the chosen adjacency's left node and its neighbors.
			 * (!) For freeing memory purpose.
			 */
			leftNode.neighborsLink = null;
			
			/**
			 * Treat secondly the chosen adjacency's right child's neighboring nodes (~ regions).
			 */
			rightNode.neighborsLink.remove(chosenAdja); // REMOVE THE CHOSEN ADJACENCY DIRECTLY IN THE CHOSEN ADJACENCY'S RIGHT NODE'S ADJACENCIES LIST.
			rightNode.neighborsLink
			.stream()
			.forEach(rightNodeAdja->{
				/**
				 * - Identify the adjacency allowing the link between the chosen adjacency's right node and its neighbor.
				 * - Add it in the list of adjacencies to delete from the listW(s).
				 * - Remove it from the RAG.
				 */
				listAdjaToDelete.add(rightNodeAdja);
				this.rag.removeAdjacency(rightNodeAdja); // REMOVE ADJACENCY FROM THE RAG.
				
				/**
				 *  Identify the neighbor of the chosen adjacency's right node.
				 */
				Node rightNodeNeighbor;
				if(rightNodeAdja.leftNode.index == rightNode.index) rightNodeNeighbor = rightNodeAdja.rightNode;
				else rightNodeNeighbor = rightNodeAdja.leftNode;
				
				/**
				 * (!) If the potential new adjacency is not already in the list after the treatment of the chosen adjacency's left node.
				 * - Create a new adjacency between the new node and the affected region.
				 * - Add it in the RAG.
				 * - Add it in the list of adjacencies to add. 
				 */
				if(!listAdjaToAdd.contains(new Adjacency(rightNodeNeighbor, newNode))){
					boolean adjaSpecialTreat = true;
					if(this.listOfLists.size() == 1) adjaSpecialTreat = false;
					Adjacency newAdja = new Adjacency(rightNodeNeighbor, newNode, this.listOfLists, adjaSpecialTreat); // (!) THE NEW ADJACENCY IS ADDED AUTOMATICALLY IN ALL LISTW(S).
					this.rag.addAdjacency(newAdja); // (!) ADDING A NEW ADJACENCY IN THE RAG MUST BE DONE MANUALLY.
					listAdjaToAdd.add(newAdja);
				}
				
				/**
				 * Let the neighbor forget the link (~ adjacency) between it and the right node.
				 * (!) It also has to be done at the other side (ie. remove the link in the list of neighbors of the right node object)
				 */
				rightNodeNeighbor.neighborsLink.remove(rightNodeAdja);
			});
			
			/**
			 * Clear the adjacencies list allowing the link between the chosen adjacency's right node and its neighbors.
			 * (!) For freeing memory purpose.
			 */
			rightNode.neighborsLink = null;
			
			/**
			 * - Delete the chosen adjacency from the listW(s).
			 * - Delete all the adjacencies that meant to be removed from all the listW(s).
			 */
			listAdjaToDelete.add(chosenAdja);
			listAdjaToDelete
			.stream()
			.forEach(adjaToDelete->{				
				/**
				 * - Remove an adjacency from the listW.
				 * - Done in a parallel fashion.
				 */
				this.listOfLists
				.parallelStream()
				.forEach(listW->{
					listW.structure.remove(adjaToDelete);
					listW.numberOfElements--;
				});
			});

			/**
			 * Update ranks if the number of lists is higher than 1. (!)
			 */
			if(this.listOfLists.size() > 1)
				this.listOfLists
				.parallelStream()
				.forEach(listW->{
					listW.structure.updateRanks();
				});
			
			/**
			 * - Delete the chosen adjacency from the RAG.
			 */
			this.rag.removeAdjacency(chosenAdja);
					
			Log.printLvl2(String.valueOf(Context.RAG_UPDATE), "Fused nodes: <"+ chosenAdja.leftNode.index +","+ chosenAdja.rightNode.index +">");
			Log.printLvl2(String.valueOf(Context.RAG_UPDATE), "Nb adja in RAG after the update: "+ Adjacency.adjacencyCounter);
			Log.printLvl2(String.valueOf(Context.RAG_UPDATE), "Nb adja in ListWs after the update: "+ this.listOfLists.get(0).getNumberOfContainedAdjacencies());
			Log.printLvl2(String.valueOf(Context.RAG_UPDATE), "Nb elements to sort in the 1st ListW: "+ this.listOfLists.get(0).structure.size());
			
			/**
			 * Show merging progression.
			 */
			nbFusions++;
			progression = (float) nbFusions * 100 / estimatedFusions;
//			if(nbFusions%10000==0)
//				System.gc();
			Log.printLvl1(String.valueOf(Context.MBPT), String.format("%.2f", progression) +"% ("+ nbFusions +" fusions)");
			
			/* -TODO remove unused nodes */
//			this.rag.listOfNodes[chosenAdja.leftNode.index] = null;
//			this.rag.listOfNodes[chosenAdja.rightNode.index] = null;
			chosenAdja = null;
			
		}	
		Log.printLvl1(String.valueOf(Context.MBPT), "Number of fusions: "+ nbFusions);
		
		long endingTime = fusionTimeThread.getCurrentThreadCpuTime();
		this.fusionTime = (endingTime - startingTime)/1000000; // TIME OF FUSION IN MS.
		Log.printLvl1(String.valueOf(Context.MBPT), "Fusion time: "+ this.fusionTime +" ms");
		Log.printLvl1(String.valueOf(Context.MBPT), "Multi-featured Binary Partition Tree created!");
		
		/**
		 * Define the root.
		 * At this stage, the only active node should be the root.
		 */
		this.root = this.rag.listOfNodes[this.rag.listOfNodes.length-1];
		this.root.type = TypeOfNode.ROOT;
		
		/*
		 * Save root.
		 */
		this.treeSave.write(new StringBuilder("],\"root\":").append(this.root.index).append(",\"time\":").append(this.fusionTime).append("}").toString());
		System.out.println(this.listOfImages.get(0).width +"x"+ this.listOfImages.get(0).height +"-"+ this.fusionTime +"ms");
		
		/*
		 * Close the buffer used for saving the tree in a file.
		 */
		this.treeSave.closeBufferedWriter();
	}
	
	/**
	 * Rebuild a tree from a <b>.mbpt</b> file that have to be precised when creating an empty tree.
	 * (!) The rebuild tree does not contain all information as the original one. This kin of tree is often used for a potential cutting.
	 */
	@SuppressWarnings("unchecked")
	public void reGrow(){
		
		FileReader fr = null;
		JSONObject jsMbpt = null;
		JSONParser mbptParser = new JSONParser();
		JSONArray listNodes = null;
		JSONObject jsNode;
		JSONArray listLeaves = null;
		int nbLeaves = 0;
		JSONObject jsLeaf;
		JSONArray data = null;
		JSONArray consensusArray = null;
		
		/**
		 * Parse the content of the file and prepare the rebuilding process.
		 */
		try {
			
			Log.printLvl1(String.valueOf(Context.REGROWING), "Start parsing \""+ this.mBptFileNamePath +"\" ...");
			fr = new FileReader(this.mBptFileNamePath);
			jsMbpt = (JSONObject) mbptParser.parse(fr);
			
			consensusArray = (JSONArray) jsMbpt.get("consensus");
			nbLeaves = ((Long) jsMbpt.get("nbLeaves")).intValue();
			data = (JSONArray) jsMbpt.get("data");		
			listLeaves = (JSONArray) jsMbpt.get("pixels");
			listNodes = (JSONArray) jsMbpt.get("nodes");
			
			fr.close();
			
		} catch (IOException | ParseException e) {
			e.printStackTrace();
			System.err.println("["+ String.valueOf(Context.REGROWING) +"] Process aborted! File not found: \""+ this.mBptFileNamePath +"\".");
			System.err.println("[HINT] Create a MPBT and save it in a '.mbpt' file.");
			System.exit(1);
		}

		/**
		 * Rebuild consensus strategies objects.
		 */
		Iterator<JSONObject> consensusIterator = consensusArray.iterator();
		JSONObject consensusParams;
		String consensusStrategy;
		int consensusRange;
		while(consensusIterator.hasNext()){
			
			consensusParams = consensusIterator.next();
			consensusStrategy = (String) consensusParams.get("c");
			consensusRange = ((Long) consensusParams.get("p")).intValue();
			if(consensusStrategy.compareTo("none") != 0) this.setConsensusStrategy(ConsensusStrategy.valueOf(consensusStrategy), consensusRange);
			
		}
		
		/**
		 * - Register an image by linking it with the tree.
		 * - Associate a metric with the image.
		 */
		Iterator<JSONObject> dataIterator = data.iterator();
		ArrayList<String> metricType;
		JSONObject oneData;
		TiffImage tiffImg;
		while(dataIterator.hasNext()){

			oneData = dataIterator.next();
			tiffImg = new TiffImage(oneData.get("img").toString());
			metricType = (ArrayList<String>) oneData.get("metrics");
			this.registerImage(tiffImg);

			for(int i = 0; i < metricType.size(); ++i){

				this.linkMetricToAnImage(tiffImg, TypeOfMetric.valueOf(metricType.get(i)));

			}

		}

		/**
		 * Create a pseudo RAG.
		 */
		this.rag = new RAG(this, nbLeaves);

		/**
		 * Rebuild leaves;
		 */
		Iterator<JSONObject> leafIterator = listLeaves.iterator();
		int leafIndex, x, y;
		Node leaf;
		Log.printLvl1(String.valueOf(Context.REGROWING), "Start redefining leaves! ...");
		while(leafIterator.hasNext()){

			jsLeaf = leafIterator.next();
			leafIndex = ((Long) jsLeaf.get("leaf")).intValue();
			x = ((Long) jsLeaf.get("x")).intValue();
			y = ((Long) jsLeaf.get("y")).intValue();
			leaf = this.rag.listOfNodes[leafIndex];
			if(leaf == null){
				leaf = new Node(leafIndex, this.listOfLists);
				this.rag.listOfNodes[leafIndex] = leaf;

				/**
				 * Add the new leaf in the list of active nodes. 
				 */
				this.activeNodes.add(leaf);
			}

			leaf.points.add((y * this.listOfImages.get(0).width) + x);
			leaf.size++;

		}
		Log.printLvl1(String.valueOf(Context.REGROWING), "All leaves redefined!");
		
		/**
		 * Free some spaces.
		 */
		fr = null;
		jsMbpt = null;
		mbptParser = null;
		listLeaves = null;
		jsLeaf = null;
		data = null;
		System.gc();
		
		/**
		 * Rebuild the tree by replaying the fusions order (~ node mergings).
		 */
		ThreadMXBean fusionTimeThread = ManagementFactory.getThreadMXBean();
		long startingTime = fusionTimeThread.getCurrentThreadCpuTime();
		
		Iterator<JSONObject> nodeIterator = listNodes.iterator();
		int nodeIndex, lIndex, rIndex;
		Node node, leftNode, rightNode;
		int nbFusions = 0;
		int estimatedFusions = this.rag.nbLeaves - 1;
		float progression;
		while(nodeIterator.hasNext()){
			
			jsNode = nodeIterator.next();
			nodeIndex = ((Long) jsNode.get("n")).intValue();
			
			lIndex = ((Long) jsNode.get("l")).intValue();
			rIndex= ((Long) jsNode.get("r")).intValue();
			leftNode = this.rag.listOfNodes[lIndex];
			rightNode = this.rag.listOfNodes[rIndex];
			
			node = new Node(leftNode, rightNode, this.listOfLists);
			this.rag.listOfNodes[nodeIndex] = node;
			node.merginScore = (double) jsNode.get("s");
			node.sizeOfRAGWhenMerged = (int)(long)jsNode.get("ls");

			/**
			 * Show merging simulation progression.
			 */
			nbFusions++;
			progression = (float) nbFusions * 100 / estimatedFusions;
			Log.printLvl1(String.valueOf(Context.REGROWING), String.format("%.2f", progression) +"%");
			
		}
		
		long endingTime = fusionTimeThread.getCurrentThreadCpuTime();
		long resultGenerationTime = (endingTime - startingTime)/1000000; // TIME OF FUSION SIMULATION IN MS.
		Log.printLvl1(String.valueOf(Context.REGROWING), "MBPT regrowing done in  "+ resultGenerationTime +" ms");
		
		/**
		 * Define the root.
		 * At this stage, the only active node should be the root.
		 */
		this.root = this.rag.listOfNodes[this.rag.listOfNodes.length-1];
		if(this.root != null)
			this.root.type = TypeOfNode.ROOT;
		
	}
	
	/**
	 * Pre-segmentation from pixels -> 1 label for 1 pixel.
	 * 
	 */
	public void preSegFromPixels() {
		
		int label = 0;
		for(int y = 0; y < this.indexationMatrix[0].length; ++y){
			for(int x = 0; x < this.indexationMatrix.length; ++x){
				this.indexationMatrix[x][y] = label;
				label++;
			}
		}
		Log.printLvl1(String.valueOf(Context.MBPT), "Pre-segmentation from pixels!");
		
	}
	
	/**
	 * Pre-segmentation from a random color file.
	 */
	public void preSegFromRandomColorFile(){
		
		SegmentByConnexityRaw sbc = new SegmentByConnexityRaw(this.preSegFile);
		Image preSegImage = sbc.runForFullImage();
		for(int y = 0; y < this.indexationMatrix[0].length; ++y){
			for(int x = 0; x < this.indexationMatrix.length; ++x){
				this.indexationMatrix[x][y] = preSegImage.getPixelXYInt(x, y);
			}
		}
		
		/*
		 * Set the number of leaves.
		 */
		this.preSegFile.nbRegions = (int) preSegImage.getProperty("nbRegions");
		Log.printLvl1(String.valueOf(Context.MBPT), "Pre-segmentation from a random color image!");
		
	}
	
	/**
	 * Perform a simple cut leading to the specific number of regions.
	 * Generates: </br>
	 * - a label image based on a random color table, </br>
	 * - a border image corresponding to the segmentation, </br>
	 * - a heat map image (for a conflict tracking purpose), </br>
	 * - a graph showing some information (for instance used for a conflict tracking purpose).
	 * @param folderPath Path and name of a (new) folder where the image results will be stored. If set to <b>null</b>, a default output folder will be created.
	 * @param args Various arguments that can be used for the simple cutting method. The number of arguments (~ args) must be 1, 2 or 3:</br></br>
	 * if (1): </br>
	 * - 1st argument: number of wanted regions; </br></br>
	 * if (2): </br>
	 * - 1st argument: number of wanted regions; </br>
	 * - 2nd argument: in order to draw a conflict tracking chart, the interval of the values on the x axis must be specified; </br></br>
	 * if (3): </br>
	 * - 1st argument: number of regions where the cutting will be started; </br>
	 * - 2nd argument: after starting the cutting process from the specified number of regions, results will be generated according to this interval value. </br>
	 * - 3rd argument: in order to draw a conflict tracking chart, the interval of the values on the x axis must be specified;
	 */
	public void cut(String folderPath, boolean equalize, int... args) {
		
		/**
		 * Abort the program if no arguments are set.
		 * (i.e.) size of args = 0.
		 */
		int numberOfArguments = args.length;
		int ctgInterval = this.rag.nbLeaves;
		int startingSaveRegion = -1;
		int savingInterval = -1;
		switch(numberOfArguments){
			case 0:
				System.err.println("["+ String.valueOf(Context.CUTTING) +"] Unvalid number of arguments in the method 'agat.tree.MBPT.cut(String folderPath, int... args)'. The args must contain at least one argument.");
				System.exit(1);
			case 1:
				startingSaveRegion = args[0];
				savingInterval = startingSaveRegion;
				break;
			case 2:
				startingSaveRegion = args[0];
				savingInterval = startingSaveRegion;
				ctgInterval = args[1]; // THE CONFLICT TRACKING INTERVAL OF THE VALUES RECORDING MUST BE THE LAST PARAMETER.
				break;
			case 3:
				startingSaveRegion = args[0];
				savingInterval = args[1];
				ctgInterval = args[2]; // THE CONFLICT TRACKING INTERVAL OF THE VALUES RECORDING MUST BE THE LAST PARAMETER.
				break;
			default:
				System.err.println("["+ String.valueOf(Context.CUTTING) +"] Unvalid number of arguments in the method 'agat.tree.MBPT.cut(String folderPath, int... args)'. The number of arguments must be 1, 2 or 3.");
				System.exit(1);
		}
		
		/**
		 * Create a folder for the results.
		 */
		String[] mBptPathSplit = this.mBptFileNamePath.split("//");
		String mBptFileName = mBptPathSplit[mBptPathSplit.length-1];
		String[] mBptFileNameSplit = mBptFileName.split(".mbpt");
		String mBptFileNameNoExt = mBptFileNameSplit[0];
		
		if(folderPath == null) folderPath = this.mBptFileNamePath +"-results"; // DEFAULT AUTO-GENERATED OUTPUT FOLDER NAME.
			
		new File(folderPath).mkdir();
		Log.printLvl1(String.valueOf(Context.CUTTING), "Result folder \""+ folderPath +"\" created!");
		
		/**
		 * Clear the list of active nodes and set all leaves active.
		 */
		this.activeNodes.clear();
		for(int il = 0; il < this.rag.nbLeaves; ++il){
			this.activeNodes.add(this.rag.listOfNodes[il]);
		}
		
		/*
		 * Prepare the conflict tracking graph. 
		 */
		XYSeriesCollection chartData = new XYSeriesCollection();
		final XYSeries series1 = new XYSeries("Chosen merging scores");
		final XYSeries series2 = new XYSeries("Number of adjacencies");
		final XYSeries propSeries = new XYSeries("Proportions");
		
		/**
		 * Browse the node merging fusions.
		 */
		ThreadMXBean fusionTimeThread = ManagementFactory.getThreadMXBean();
		long startingTime = fusionTimeThread.getCurrentThreadCpuTime();
		
		Node node, leftNode, rightNode;
		int nbFusions = 0;
		int estimatedFusions = this.rag.nbLeaves - 1;
		float progression;
		String addToName;
		int numberOfActiveNodes;
		for(int ni = this.rag.nbLeaves; ni < this.rag.listOfNodes.length; ++ni){
						
			node = this.rag.listOfNodes[ni];
			leftNode = node.leftChild;
			rightNode = node.rightChild;

			this.activeNodes.remove(leftNode);
			this.activeNodes.remove(rightNode);
			this.activeNodes.add(node);

			/*
			 * Show merging simulation progression.
			 */
			nbFusions++;
			progression = (float) nbFusions * 100 / estimatedFusions;
			Log.printLvl1(String.valueOf(Context.CUTTING), String.format("%.2f", progression) +"%");
			
			/**
			 * Generate the segmentation image results.
			 */
			numberOfActiveNodes = this.activeNodes.size();
			if(numberOfActiveNodes <= startingSaveRegion && (numberOfActiveNodes % savingInterval) == 0){
				addToName = ""+ this.activeNodes.size();
				this.generateImageResults(folderPath, addToName, equalize);
				Log.printLvl1(String.valueOf(Context.CUTTING), "Cut leading to "+ this.activeNodes.size() +" regions.");
			}
			
			/* 
			 * Fill the values of the conflict tracking graph.
			 */
			if((nbFusions % ctgInterval) == 0){ // FOR EVERY X FUSIONS.
				double mergingScore = node.merginScore;
				System.out.println(mergingScore);
				if(mergingScore > node.sizeOfRAGWhenMerged){ // THE STRATEGY USED WAS JUST SUM BUT NOT MEAN.
					/* Find the real values */
					mergingScore /= this.listOfLists.size();
					mergingScore = node.sizeOfRAGWhenMerged - mergingScore + 1;
				}
				series1.add(nbFusions, mergingScore);
				series2.add(nbFusions, node.sizeOfRAGWhenMerged);
				propSeries.add(nbFusions, (mergingScore / node.sizeOfRAGWhenMerged)*100);
			}
		}
				
		/* 
		 * Save the conflict tracking graph in an image. 
		 */
		if(ctgInterval < this.rag.nbLeaves){
			chartData.addSeries(series1);
			chartData.addSeries(series2);
			IntervalXYDataset dataset = chartData;
			JFreeChart chart = ChartFactory.createXYBarChart(
					"Conflict tracking for "+ mBptFileName, // CHART TITLE.
					"Node merging", // X AXIS LABEL.
					false,
					"Merging score", // Y AXIS LABEL.
					dataset, // DATA.
					PlotOrientation.VERTICAL,
					true, // INCLUDE LEGENS.
					true, // TOOLTIPS.
					false // URLS.
					);
			chart.setBackgroundPaint(Color.white);
			final XYPlot plot = chart.getXYPlot();
			plot.setBackgroundPaint(Color.lightGray);
			plot.setDomainGridlinePaint(Color.white);
			plot.setRangeGridlinePaint(Color.white);
			final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
			final XYBarRenderer renderer2 = new XYBarRenderer();
			renderer2.setShadowVisible(false);
			renderer2.setBarPainter(new StandardXYBarPainter());
			renderer2.setSeriesPaint(0, Color.darkGray);
			renderer2.setMargin(0.00001);

			XYSeriesCollection chartData2 = new XYSeriesCollection(propSeries);
			IntervalXYDataset dataset2 = chartData2;
			plot.setDataset(1, dataset2);
			plot.setRenderer(0, renderer);
			plot.setRenderer(1, renderer2);		
			plot.mapDatasetToRangeAxis(1, 1);

			final ValueAxis axis2 = new NumberAxis("Proportion");
			plot.setRangeAxis(1, axis2);
			axis2.setRange(0, 100);

			final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
			rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
			try {
				final ChartRenderingInfo info = new ChartRenderingInfo(new StandardEntityCollection());
				ChartUtilities.saveChartAsPNG(new File(folderPath +"//Ct-"+ mBptFileNameNoExt +".png"), chart, 800, 800, info);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		long endingTime = fusionTimeThread.getCurrentThreadCpuTime();
		long resultGenerationTime = (endingTime - startingTime)/1000000; // TIME OF FUSION SIMULATION IN MS.
		Log.printLvl1(String.valueOf(Context.CUTTING), "Cut performed in "+ resultGenerationTime +" ms");
	}
	
	/**
	 * Generate image results.
	 * @param folderPath Path of the folder storing the results.
	 * @param addToName Information to add to the name of the result file.
	 * @param equalize 
	 */
	public void generateImageResults(String folderPath, String addToName, boolean equalize){

		Queue<Node> zones = new LinkedList<>(this.activeNodes);
		zones.addAll(this.activeNodes);
		
		Node n;
		TreeSet<Integer> points;
		
		int x, y;
		int xp, yp;
		
		/**
		 * - Prepare the indexation matrix.
		 * - Prepare the heatmap matrix.
		 */
		int imgWidth = this.listOfImages.get(0).width;
		int imgHeight = this.listOfImages.get(0).height;
		this.indexationMatrix = new int[imgWidth][imgHeight];
		double[][] heatMapMatrix = new double[imgHeight][imgWidth];
		
		/**
		 * Fill the indexation matrix and heatMap matrix.
		 */
		while(!zones.isEmpty()){
			
			n = zones.remove();

			points = n.getPoints();

			for(Integer p: points){
				
				xp = p.intValue() % imgWidth;
				yp = p.intValue() / imgWidth;
				this.indexationMatrix[xp][yp] = n.index;
				heatMapMatrix[yp][xp] = n.merginScore; // (!) ACCORDING TO THE HEAT MAP LIBRARY USED, THE INDEX ORDER IS NOT A MISTAKE. 
				
			}

		}
		
		/**
		 * Prepare a visualization image based on the first image. This will be used as the background where the borders will be mapped.
		 */
		Image imageVisu = this.constructVisuImage(this.listOfImages.get(0).getPiafImage(), true, equalize);
		
		/**
		 * Random color image based on the result of the cut (~ segmentation).
		 */
		IntegerImage segRes = new IntegerImage(imageVisu.getXDim(), imageVisu.getYDim(), 1, 1, 1);
		for (y = 0; y < imageVisu.getYDim(); ++y){
			for (x = 0; x < imageVisu.getXDim(); ++x){
				segRes.setPixelXYBInt(x, y, 0, this.indexationMatrix[x][y]);
			}
		}
		Image randomColors = LabelsToRandomColors.exec(segRes);
		
//		/**
//		 * Frontier image based on the result of the cut (~ segmentation).
//		 * (!) Yellow border color.
//		 */
//		double [] borderColor=new double[3];
//		borderColor[0]=1.0;
//		borderColor[1]=1.0;
//		borderColor[2]=0.0;
		/**
		 * Frontier image based on the result of the cut (~ segmentation).
		 * (!) Purple border color.
		 */
		double [] borderColor=new double[3];
		borderColor[0]=1.0;
		borderColor[1]=0.0;
		borderColor[2]=1.0;
		Image frontiers = DrawFrontiersOnImage.exec(imageVisu, FrontiersFromSegmentation.exec(segRes), borderColor);

		/**
		 * Heat map image based on the result of the cut (~ segmentation).
		 */
		HeatChart map = new HeatChart(heatMapMatrix);
		map.setHighValueColour(Color.red);
		map.setLowValueColour(Color.yellow);
		map.setColourScale(0.5);
		map.setShowXAxisValues(false);
		map.setShowYAxisValues(false);
		map.setCellSize(new Dimension(1,1));
		map.setChartMargin(0);
		map.setAxisThickness(0);
		
		/**
		 * Mapping frontiers on the heat map.
		 */
		BufferedImage hmBuff = (BufferedImage) map.getChartImage();
		Image hmBdVisu = new DoubleImage(hmBuff.getWidth(), hmBuff.getHeight(), 1, 1, 3);
		double factor = 1.0 / 255;
		Color c;
		for(int xb = 0; xb < hmBuff.getWidth(); ++xb){
			for(int yb = 0; yb < hmBuff.getHeight(); ++yb){
				c = new Color(hmBuff.getRGB(xb, yb));
				hmBdVisu.setPixelXYBDouble(xb, yb, 0, c.getRed() * factor);
				hmBdVisu.setPixelXYBDouble(xb, yb, 1, c.getGreen() * factor);
				hmBdVisu.setPixelXYBDouble(xb, yb, 2, c.getBlue() * factor);
			}
		}
		borderColor[0]=0.0;
		borderColor[1]=0.0;
		borderColor[2]=0.0;
		Image hmFrontiers = DrawFrontiersOnImage.exec(hmBdVisu, FrontiersFromSegmentation.exec(segRes), borderColor);
		
		/**
		 * Overlap the first image and the heat map image with all frontiers.
		 */
		BufferedImage image1 = new BufferedImage(imgWidth, imgHeight, BufferedImage.TYPE_INT_RGB);
		int rcol , gcol, bcol;
		int col;
		for(int xv=0; xv<imageVisu.xdim; ++xv){
			for(int yv=0; yv<imageVisu.ydim; ++yv){
				rcol = imageVisu.getPixelXYBByte(xv, yv, 0);
				gcol = imageVisu.getPixelXYBByte(xv, yv, 1);
				bcol = imageVisu.getPixelXYBByte(xv, yv, 2);
				col = (rcol << 16) | (gcol << 8) | bcol;
				image1.setRGB(xv, yv, col);
			}
		}
		BufferedImage image2 = new BufferedImage(imgWidth, imgHeight, BufferedImage.TYPE_INT_RGB);
		for(int xv=0; xv<hmFrontiers.xdim; ++xv){
			for(int yv=0; yv<hmFrontiers.ydim; ++yv){
				rcol = hmFrontiers.getPixelXYBByte(xv, yv, 0);
				gcol = hmFrontiers.getPixelXYBByte(xv, yv, 1);
				bcol = hmFrontiers.getPixelXYBByte(xv, yv, 2);
				col = (rcol << 16) | (gcol << 8) | bcol;
				image2.setRGB(xv, yv, col);
			}
		}
		BufferedImage combinedImage = new BufferedImage(imgWidth,imgHeight, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = (Graphics2D) combinedImage.getGraphics();
		g.drawImage(image1, 0, 0, null);
		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) 0.6));
		g.drawImage(image2, 0, 0, null);
		
		/**
		 * Save image results.
		 */
		try{
			
			ImageSave.exec(randomColors, folderPath +"//RC-"+ addToName +".tif");
			ImageSave.exec(frontiers, folderPath +"//BD-"+ addToName +".tif");
//			map.saveToFile(new File(folderPath +"//HM-"+ addToName +".tif"));
//			ImageSave.exec(hmFrontiers, folderPath +"//HM-BD-"+ addToName +".tif");
//			ImageIO.write(combinedImage, "PNG", new File(folderPath +"//FHM-"+ addToName +".png"));
			Log.printLvl1(String.valueOf(Context.CUTTING), "Image segmentation results generated!");
		}catch(Exception e){
			System.err.println("["+ String.valueOf(Context.CUTTING) +"] Failed to generate image segmentation results!");
		}
		
	}
	
	/**
	 * Prepare a visualization image.
	 * @param image
	 * @param readingMode
	 * @return a visualization image.
	 * @author C. Kurtz
	 * @param equalize 
	 */
	public Image constructVisuImage(Image image, Boolean readingMode, boolean equalize) {

		int xDim = image.getXDim();
		int yDim = image.getYDim();
		int bDim = image.getBDim();
		int x, y;

		Image res = null;

		switch(bDim){
		case 1: 
			res = new ByteImage(xDim, yDim, 1,1, 3);
			for (y = 0; y < yDim; ++y){
				for (x = 0; x < xDim; ++x){
					res.setPixelByte( x,y,0,0,0, 
							image.getPixelByte( x,y,0,0,0) );

					res.setPixelByte( x,y,0,0,1, 
							image.getPixelByte( x,y,0,0,0) );

					res.setPixelByte( x,y,0,0,2, 
							image.getPixelByte( x,y,0,0,0) );
				}
			}
			break;
		case 2: 
			res = new ByteImage(xDim, yDim, 1,1, 3);
			for (y = 0; y < yDim; ++y){
				for (x = 0; x < xDim; ++y){
					res.setPixelByte( x,y,0,0,0, 
							image.getPixelByte( x,y,0,0,0) );

					res.setPixelByte( x,y,0,0,1, 
							image.getPixelByte( x,y,0,0,0) );

					res.setPixelByte( x,y,0,0,2, 
							image.getPixelByte( x,y,0,0,0) );
				}
			}
			break;	

		case 3:	
			res = image;
			break;
		case 4:	
			res = new ByteImage(xDim, yDim, 1,1, 3);
			for (y = 0; y < yDim; ++y){
				for (x = 0; x < xDim; ++x){

					if(readingMode){
						/*
						 * If the whole image has to be read, there is a chance that this is a MSR ...
						 */
						res.setPixelByte( x,y,0,0,0, 
								image.getPixelByte( x,y,0,0,0) );

						res.setPixelByte( x,y,0,0,1, 
								image.getPixelByte( x,y,0,0,1) );

						res.setPixelByte( x,y,0,0,2, 
								image.getPixelByte( x,y,0,0,2) );
					}else{
						/*
						 * Else, there is a chance that this is a HSR ...
						 */
						res.setPixelByte( x,y,0,0,0, 
								image.getPixelByte( x,y,0,0,2) );

						res.setPixelByte( x,y,0,0,1, 
								image.getPixelByte( x,y,0,0,1) );

						res.setPixelByte( x,y,0,0,2, 
								image.getPixelByte( x,y,0,0,0) );

					}	
				}
			}	
			break;

		case 5:	
			res = new ByteImage(xDim, yDim, 1,1, 3);
			for (y = 0; y < yDim; ++y){
				for (x = 0; x < xDim; ++x){

					if(readingMode){
						/*
						 * If the whole image has to be read, there is a chance that this is a MSR ...
						 */
						res.setPixelByte( x,y,0,0,0, 
								image.getPixelByte( x,y,0,0,0) );

						res.setPixelByte( x,y,0,0,1, 
								image.getPixelByte( x,y,0,0,1) );

						res.setPixelByte( x,y,0,0,2, 
								image.getPixelByte( x,y,0,0,2) );
					}else{
						/*
						 * Else, there is a chance that this is a HSR ...
						 */
						res.setPixelByte( x,y,0,0,0, 
								image.getPixelByte( x,y,0,0,2) );

						res.setPixelByte( x,y,0,0,1, 
								image.getPixelByte( x,y,0,0,1) );

						res.setPixelByte( x,y,0,0,2, 
								image.getPixelByte( x,y,0,0,0) );

					}	
				}
			}	
			break;


		case 6:	
			res = new ByteImage(xDim, yDim, 1,1, 3);
			for (y = 0; y < yDim; ++y){
				for (x = 0; x < xDim; ++x){

					if(readingMode){
						/*
						 * If the whole image has to be read, there is a chance that this is a MSR ...
						 */
						res.setPixelByte( x,y,0,0,0, 
								image.getPixelByte( x,y,0,0,0) );

						res.setPixelByte( x,y,0,0,1, 
								image.getPixelByte( x,y,0,0,1) );

						res.setPixelByte( x,y,0,0,2, 
								image.getPixelByte( x,y,0,0,2) );
					}else{
						/*
						 * Else, there is a chance that this is a HSR ...
						 */
						res.setPixelByte( x,y,0,0,0, 
								image.getPixelByte( x,y,0,0,2) );

						res.setPixelByte( x,y,0,0,1, 
								image.getPixelByte( x,y,0,0,1) );

						res.setPixelByte( x,y,0,0,2, 
								image.getPixelByte( x,y,0,0,0) );

					}	
				}
			}	
			break;

		}

		/**
		 * Using equalization changes the picture's visualization color.
		 */
		if(equalize) res = Equalization.exec(res);

		return res;	

	}
	
}
