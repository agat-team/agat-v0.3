package agat.tree.structure;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.TreeSet;

import agat.metric.support.ToolsMetric;
import agat.util.Log;

/**
 * 
 * A region in an image is represented by a Node in the tree.
 * 
 * @author T.J.F.R.
 *
 */
public class Node implements Comparable<Node>{

	/**
	 * 
	 * Each node is defined by a precise type that compose a tree.
	 *
	 */
	public static enum TypeOfNode{
		
		LEAF,
		NODE,
		ROOT
		
	}
	
	/**
	 * All attributes of the Node.
	 */
	public TypeOfNode type; // TYPE OF THE NODE.
	public int index; // INDEX OF THE REGION THAT IS CONTAINED IN THE INDEXATION MATRIX OF THE MBPT. (!) THE INDEX CAN BE INTERPRETED AS AN IDENTIFICATION (~ INDEX) OF THE NODE.
	public Node leftChild; // FIRST REGION.
	public Node rightChild; // SECOND REGION.
	public Node father; // PARTITION CONTAINING THE NODE.
	public TreeSet<Integer> points; // LIST OF POINTS (~ PIXELS) CONTAINED IN THE NODE (~ REGION). (!) THE POINTS ARE JUST CONTAINED BY THE LEAVES.
	public int size; // NUMBER OF POINTS (~ PIXELS).
	public ArrayList<Adjacency> neighborsLink = new ArrayList<Adjacency>();
	public ArrayList<Double> metricFeatures = new ArrayList<Double>(); // LIST OF METRIC FEATURES CORRESPONDING TO A SPECIFIED METRIC. (!) THE METRIC OBJECT KNOWS WHERE TO FIND ITS CORRESPONDING FEATURES.
	public double merginScore = 1; // SCORE THAT ALLOWED THE CREATION OF THIS NODE.
	public int sizeOfRAGWhenMerged; // NUMBER OF ADJACENCIES DURING THE FUSION (~ SIZE LIST OF ADJACENCIES IN THE RAG).
	public static int nodeCounter = 0;
	public int [] boundingBox = new int[4]; // minX maxX minY maxY 
	public TreeSet<Integer> borderPoints = new TreeSet<Integer>(); // LIST OF POINTS (~ PIXELS) CONTAINED IN THE BORDER OF THE NODE (~ REGION).
	
	/**
	 * Attributes for UOA computation.
	 */
	public double homogeneity; // [0,1] (0- homogeneous, 1- heterogeneous)
	public int phi; // 1 (over-segmentation), -1 (under-segmentation), 0 (well isolated)
	
	/**
	 * Create a new leaf.
	 * @param index Identification of the leaf wich is an initial region.
	 * @param listOfLists List of list <img, metric> contained in the tree owning this leaf.
	 */
	public Node(int index, List<ListW> listOfLists){

		/**
		 * Initializing all attributes of the leaf.
		 */
		this.type = TypeOfNode.LEAF;
		this.index = index;
		Node.nodeCounter++;
		
		this.points = new TreeSet<Integer>();
		this.size = 0;
		
		/**
		 * Prepare the Metric Features (MF).
		 * (!) For now, this cannot be done in a parallel fashion.
		 */
		listOfLists.get(0).metric.prepareMF(this);
		listOfLists.subList(1, listOfLists.size())
		.stream()
		.forEach(lw->lw.metric.prepareMF(this));

//		for(int il = 0; il < listOfLists.size(); ++il){
//			listOfLists.get(il).metric.prepareMF(this);
//		}
		
		Log.printLvl3(String.valueOf(this.type), "new leaf: "+ this.index);
		
	}

	/**
	 * Create a new node.
	 * @param leftNode Left child.
	 * @param rightNode Right child.
	 * @param listOfLists List of list <img, metric> contained in the tree owning this leaf.
	 */
	public Node(Node leftNode, Node rightNode, List<ListW> listOfLists) {
		
		/**
		 * Initializing all attributes of the node.
		 */
		this.type = TypeOfNode.NODE;
		this.index = Node.nodeCounter;
		Node.nodeCounter++;
		
		this.leftChild = leftNode;
		this.leftChild.father = this;

		this.rightChild = rightNode;
		this.rightChild.father = this;
		
		this.points=new TreeSet<Integer>();
		
		this.size = this.leftChild.size + this.rightChild.size;
		
		/** Compute bonding box of the node */
/*TEMPORARY COMMENT - ! MAY BE USEFULL FOR MBPT */
  		ToolsMetric.computeRegionBoundingBox(this, listOfLists.get(0).metric.img.width, listOfLists.get(0).metric.img.height);

		/** Compute border of the node */
/*TEMPORARY COMMENT - ! MAY BE USEFULL FOR MBPT */
  		ToolsMetric.computeBorderPixels(this, listOfLists.get(0).metric.img.width, listOfLists.get(0).metric.img.height);
 
		
		/**
		 * Prepare the Metric Features (MF) and set the right values.
		 * (!) For now, this cannot be done in a parallel fashion.
		 */
		/* Treating the first element of the list. */
		listOfLists.get(0).metric.prepareMF(this);
		listOfLists.get(0).metric.updateMF(this); 
		/* Treating the second element of the list to the end in a parallel way. */
		listOfLists.subList(1, listOfLists.size())
		.stream()
		.forEach(lw->{
			lw.metric.prepareMF(this);
			lw.metric.updateMF(this);
		});	
		
		/**
		 * Move the pixels from the children to the father 
		 */
/*		TEMPORARY COMMENT - ! MAY BE USEFULL FOR MBPT */
  		this.points.addAll(this.leftChild.points);
		this.points.addAll(this.rightChild.points);
		
		if(this.leftChild.type != Node.TypeOfNode.LEAF)
			this.leftChild.points=null;
		if(this.rightChild.type != Node.TypeOfNode.LEAF)
			this.rightChild.points=null;

		
		/**
		 * Remember the size of the RAG during the fusion.
		 */
		this.sizeOfRAGWhenMerged = listOfLists.get(0).getNumberOfContainedAdjacencies();
		
		Log.printLvl3(String.valueOf(this.type), "new node: "+ this.index);
	}

	public Node() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Get all points (~ points) contained in a node (~ region).
	 * @return a list of all points contained in the node (~ region).
	 */
	public TreeSet<Integer> getPoints() {

		if(this.points==null){
			
			TreeSet<Integer> points = new TreeSet<Integer>();
			Stack<Node> pile = new Stack<Node>();
			pile.push(this);
			
			/**
			 * Go deep and find all the included leaves.
			 * (!) Points (~ pixels) are only stored at the leaves level.
			 */
			Node n;
			while(!pile.isEmpty()){
				
				n = pile.pop();
				
				if(n.type == TypeOfNode.LEAF){
					points.addAll(n.points);
				}else{
					
					pile.push(n.leftChild);
					pile.push(n.rightChild);
					
				}
				
			}
			
			return points;
		}else{
			return this.points;
		}
		
	}
	
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* 
* IMPLEMENTED METHOD FROM THE COMPARABLE INTERFACE
* 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	@Override
	public int compareTo(Node n) {

		/**
		 * The comparison is relying the node index.
		 */
		if(this.index < n.index) return -1;
		if(this.index == n.index) return 0;
		return 1;
		
	}
	
	@Override
	public boolean equals(Object o){
		
		return this.index == ((Node)o).index;
		
	}

}
