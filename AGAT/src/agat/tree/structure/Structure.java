package agat.tree.structure;

import java.util.Comparator;
import java.util.TreeMap;

import agat.tree.structure.Adjacency.SpecialState;

public class Structure<K, V> extends TreeMap<K, V>{

	public Adjacency updateStart;

	/**
	 * 
	 */
	private static final long serialVersionUID = -6836156337588795513L;
	public int listIndex;
	
	public Structure(int listIndex, Comparator<? super K> comparator) {
		super(comparator);
		this.listIndex = listIndex;
	}
	
	@Override
	public V remove(Object o) {

		/*
		 * Shift the remain of the listW.
		 */
		Adjacency a = (Adjacency) o;
		a.specialTreat[this.listIndex] = false;
		V res = super.remove(a);

		if(res != null){
			
			if(a.next[this.listIndex] != null) a.next[this.listIndex].previous[this.listIndex] = a.previous[this.listIndex];
			if(a.previous[this.listIndex] != null) a.previous[this.listIndex].next[this.listIndex] = a.next[this.listIndex];

			a.specialTreat[this.listIndex] = false;
			a.state[this.listIndex] = SpecialState.REMOVED;
			
		}
		this.setUpdateStart(a);
		return res;
	}
	
	@Override
	public boolean containsKey(Object o) {
		
		Adjacency a = (Adjacency) o;
		boolean tmpTreat = a.specialTreat[this.listIndex];
		a.specialTreat[this.listIndex] = false;
		a.state[this.listIndex] = SpecialState.CHECKED;
		boolean res = super.containsKey(a);
		a.specialTreat[this.listIndex] = tmpTreat;
		return res;
		
	}
	
	@Override
	public V put(K key, V value) {
		
		Adjacency a = (Adjacency) key;
		V res = super.put(key, value);
		a.specialTreat[this.listIndex] = false;
		a.state[this.listIndex] = SpecialState.ADDED;
		this.setUpdateStart(a);
		return res;
		
	}
	
	public void setUpdateStart(Adjacency adja1){
		
		if(updateStart != null){
			if(updateStart.scores[this.listIndex] < adja1.scores[this.listIndex] || (updateStart.scores[this.listIndex] == adja1.scores[this.listIndex] && updateStart.compareTo(adja1) == -1)){
				updateStart = adja1;
			}
		}else this.updateStart = adja1;
		
	}
	
	public void updateRanks(){
		
		if(this.updateStart != null){
			
			Adjacency current = this.updateStart;
			Adjacency next = this.updateStart.next[this.listIndex];

			switch(this.updateStart.state[this.listIndex]){
			case ADDED:
				while(next != null){
					
					next.previous[this.listIndex] = current;
					next.ranks[this.listIndex] = next.previous[this.listIndex].ranks[this.listIndex] + 1;
					current = next;
					next = current.next[this.listIndex];

				}
				break;
			case REMOVED:
				while(next != null && current.state[this.listIndex]==SpecialState.REMOVED){
					
					next.previous[this.listIndex] = current.previous[this.listIndex];
					if(next.previous[this.listIndex] == null) next.ranks[this.listIndex] = 1;
					else next.ranks[this.listIndex] = next.previous[this.listIndex].ranks[this.listIndex] + 1;
					current = next;
					next = current.next[this.listIndex];
					
				}
				while(next != null){
					
					next.ranks[this.listIndex] = next.previous[this.listIndex].ranks[this.listIndex] + 1;
					next = next.next[this.listIndex];

				}
				break;
			default:
				break;
			}
			
			this.updateStart = null;
			
		}
	}
}
