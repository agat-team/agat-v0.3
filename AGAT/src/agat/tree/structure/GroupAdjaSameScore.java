package agat.tree.structure;

import java.util.TreeSet;

/**
 * This class is designed to store a set of adjacencies having the same score in a specified listW.
 * @author T.J.F.R.
 *
 */
public class GroupAdjaSameScore implements Comparable<GroupAdjaSameScore>{

	/*
	 * All attributes of the group of adjacencies having the same score.
	 */
	public double theScore; // THE SCORE OF THE ALL STORED ADJACENCIES.
	public TreeSet<Adjacency> content; // LIST OF ADJACENCIES HAVING THE SAME SCORE ACCORDING TO A SPECIFIED LISTW.

	/**
	 * Create an empty group of adjacency that will have the same score.
	 * @param focusedScore The score of all the future content.
	 */
	public GroupAdjaSameScore(double focusedScore) {
		this.theScore = focusedScore;
		content = new TreeSet<Adjacency>();
	}

	/**
	 * Adding an adjacency in the group of adjacencies having the same score.
	 * @param adjacency
	 */
	public void add(Adjacency adjacency) {
		this.content.add(adjacency);
	}

	/**
	 * Remove an adjacency from the group of adjacencies having the same score.
	 * @param adjaToDelete
	 */
	public void remove(Adjacency adjaToDelete) {
		this.content.remove(adjaToDelete);
	}
	
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* 
* IMPLEMENTED METHOD FROM THE COMPARABLE INTERFACE
* 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	
	@Override
	public int compareTo(GroupAdjaSameScore o) {

		/**
		 * The comparison is relying the node score.
		 */
		if(this.theScore < o.theScore) return -1;
		if(this.theScore == o.theScore) return 0;
		return 1;

	}

	@Override
	public boolean equals(Object o){
		
		if(!(o instanceof GroupAdjaSameScore)) return false;
		if(o == this) return true;
		return this.theScore == ((GroupAdjaSameScore)o).theScore;
		
	}

}
