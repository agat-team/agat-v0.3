package agat.tree.structure;

import java.util.Comparator;

/**
 * Comparator allowing the sorting of the listW.
 * @author T.J.F.R.
 *
 */
public class AdjacencyScoreComparator implements Comparator<Adjacency>{
	
	public int listIndex;
	public AdjacencyScoreComparator(int listIndex){
		this.listIndex = listIndex;
	}
	
	@Override
	public int compare(Adjacency adja1, Adjacency adja2) {
				
		if(!adja1.specialTreat[this.listIndex]){
			
			if(adja1.equals(adja2)) return 0;
			if(adja1.scores[this.listIndex] < adja2.scores[this.listIndex]
					|| (adja1.compareTo(adja2) == -1 && adja1.scores[this.listIndex] == adja2.scores[this.listIndex])) return 1;
			return -1;
			
		}else{

			if(adja1.equals(adja2)) return 0;
			if((adja1.scores[this.listIndex] < adja2.scores[this.listIndex]) || (adja1.scores[this.listIndex] == adja2.scores[this.listIndex] && adja1.compareTo(adja2)==-1)){
				
				if(adja2.next[this.listIndex] != null){
					if((adja1.scores[this.listIndex] > adja2.next[this.listIndex].scores[this.listIndex])
							|| (adja1.scores[this.listIndex] == adja2.next[this.listIndex].scores[this.listIndex] && adja1.compareTo(adja2.next[this.listIndex]) == 1)){

						adja1.next[this.listIndex] = adja2.next[this.listIndex];
						adja1.previous[this.listIndex] = adja2;
						adja1.ranks[this.listIndex] = adja1.next[this.listIndex].ranks[this.listIndex] + 1;
						if(adja1.next[this.listIndex] != null) adja1.next[this.listIndex].previous[this.listIndex] = adja1;
						adja2.next[this.listIndex] = adja1;

					}
				}else{

					adja1.next[this.listIndex] = adja2.next[this.listIndex];
					adja1.previous[this.listIndex] = adja2;
					if(adja1.previous[this.listIndex] == null)
						adja1.ranks[this.listIndex] = 1;
					else adja1.ranks[this.listIndex] = adja1.previous[this.listIndex].ranks[this.listIndex] + 1; 
					adja2.next[this.listIndex] = adja1;

				}
				
//				System.out.println("Superior: "+ adja1.index +" - "+ adja2.index);
				return 1;
				
			}

			/*
			 * If the 'adja1' is higher than 'adja2', place it before 'adja2'.
			 */
			adja1.next[this.listIndex] = adja2;
			adja1.ranks[this.listIndex] = adja2.ranks[this.listIndex];
			adja2.ranks[this.listIndex]++;
			
			if(adja2.previous[this.listIndex] != null){
				
				if((adja2.previous[this.listIndex].scores[this.listIndex] > adja1.scores[this.listIndex]
							|| ((adja2.previous[this.listIndex].scores[this.listIndex] == adja1.scores[this.listIndex]) && adja2.previous[this.listIndex].compareTo(adja1) == 1))){
					adja2.previous[this.listIndex] = adja1;
				}
				
			}else adja2.previous[this.listIndex] = adja1;
			
//			System.out.println("Inferior: "+ adja1.index +" - "+ adja2.index);
			return -1;
			
		}

	}
	
}
