package agat.tree.structure;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * Class representing a link (adjacency) between two neighboring nodes (~ neighboring regions).
 * When an adjacency is created, it is added directly in all listW.
 * 
 * @author T.J.F.R.
 *
 */
public class Adjacency implements Comparable<Adjacency>{

	/**
	 * All attributes of the Adjacency.
	 */
	public int index; // IDENTIFICATION OF THE ADJACENCY.
	public Node leftNode; // 1ST NODE (~ REGION).
	public Node rightNode; // 2ND NODE (~ REGION). (!) THE RIGHT NODE SHOULD HAVE HIGHER INDEX THAN THE LEFT ONE.
	public double scores[]; // FOR EACH LISTW, THIS ATTRIBUTE STORE THE SCORE (~ DISTANCE) COMPUTATION BETWEEN 2 REGIONS (leftNode and rightNode).
	public int ranks[]; // IN EACH LISTW, THIS ATTRIBUTE STORE THE RANK OF THE ADJACENCY.
	public double consensusScore;
	public Adjacency previous[];
	public Adjacency next[];
	public boolean specialTreat[];
	public static int adjacencyCounter = 0; // COUNT ALL ISTANCIATED ADJACENCY OBJECT.
	
	public enum SpecialState{
		NORMAL,
		ADDED,
		REMOVED,
		CHECKED
	}
	public SpecialState state[];
	
	/**
	 * Create a link between a node and its neighbor.
	 * @param node 1st Node representing a region.
	 * @param neighbor 2nd Node representing a neighboring region of the 1st one.
	 * @param listOfLists List of list <img, metric> contained in the tree owning this adjacency.
	 * @param specialTreatInit Flag determining how the adjacency will be treated when inserting it in a listW. If <b> true </b>, the adjacency's treatment (~ comparing, adding, removing) in the listW will be 'special'.
	 * For knowing more, refer to the class <b> agat.tree.structure.AdjacencyScoreComparator </b>.
	 */
	public Adjacency(Node node, Node neighbor, List<ListW> listOfLists, boolean specialTreatInit) {
		
//		this.index = Adjacency.adjacencyCounter;
//		Adjacency.adjacencyCounter++;
		
		/**
		 * (!) The left node's index must be inferior to the right node's.
		 */
		if(node.index < neighbor.index){
			this.leftNode = node;
			this.rightNode = neighbor;
		}else{
			this.leftNode = neighbor;
			this.rightNode = node;
		}
				
		/**
		 * Let the nodes (~ regions) know that they are neighbors.
		 */
		node.neighborsLink.add(this);
		neighbor.neighborsLink.add(this);
		
		/**
		 * - Allocate memory for the scores.
		 * - Allocate memory for future ranks in each listW.
		 * - For each listW (~ metric), compute the scores (~ distances) and store them.
		 * - Add Also add the adjacency in all listW(s).
		 */
		int listLength = listOfLists.size();
		this.scores = new double[listLength];
		this.ranks = new int[listLength];
		Arrays.fill(this.ranks, 1);
		this.previous = new Adjacency[listLength];
		this.specialTreat = new boolean[listLength];
		this.state = new SpecialState[listLength];
		if(specialTreatInit){
			Arrays.fill(this.specialTreat, true);
		}
		this.next = new Adjacency[listLength];

		/* Add the adjacency il all listW(s). */
//		listOfLists
//		.parallelStream()
//		.forEach(listW->{
//			this.scores[listW.index] = listW.metric.computeDistances(this.leftNode, this.rightNode);
//			listW.add(this);
//		});
		
		/* (!) the sequential way seems to be faster than the above commented parallel */
		for(int li = 0; li < listLength; ++li){
			
			ListW listW;
			listW = listOfLists.get(li);
			this.scores[li] = listW.metric.computeDistances(this.leftNode, this.rightNode);
			listW.add(this);
			
		}
		
	}

	/** 
	 * Constructor returning an adjacency that will be only used for a comparison.
	 * @param leftNode left node to use.
	 * @param rightNode right node to use.
	 */
	public Adjacency(Node leftNode, Node rightNode) {
		
		/**
		 * (!) The left node's index must be inferior to the right node's.
		 */
		if(leftNode.index < rightNode.index){
			this.leftNode = leftNode;
			this.rightNode = rightNode;
		}else{
			this.leftNode = rightNode;
			this.rightNode = leftNode;
		}
		
	}

	
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* 
* IMPLEMENTED METHOD FROM THE COMPARABLE INTERFACE
* 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	
	@Override
	public int compareTo(Adjacency a) {
		
		if(this.leftNode.compareTo(a.leftNode) == 0){
			return this.rightNode.compareTo(a.rightNode);
		}else{
			return this.leftNode.compareTo(a.leftNode);
		}
		
	}
	
	@Override
	public boolean equals(Object o){

		Adjacency adja = (Adjacency) o;
		if(this.leftNode.equals(adja.leftNode) && this.rightNode.equals(adja.rightNode)) return true;
		return false;
		
	}

}
