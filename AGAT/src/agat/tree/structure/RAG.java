package agat.tree.structure;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.Arrays;

import agat.metric.support.ToolsMetric;
import agat.tree.MBPT;
import agat.util.Log;

/**
 * 
 * REGION ADJACENCY GRAPH
 * 
 * @author T.J.F.R.
 *
 */
public class RAG {
	
	public enum Context{
		RAG
	}
	
	/**
	 * All attributes of the RAG.
	 */
	public int nbLeaves; // NUMBER OF LEAVES.
	public MBPT parentTree; // OWNER TREE OF THIS RAG.
	public Node[] listOfNodes; // ALL NODES OF THE OWNER TREE.
	public Adjacency[] listOfAdjacencies; // ACTIVE ADJACENCIES.

	/**
	 * Build the initial RAG.
	 * @param parentTree The tree containing the RAG.
	 */
	public RAG(MBPT parentTree) {

		this.parentTree = parentTree;
		Log.printLvl1(String.valueOf(Context.RAG), "Starting to build the initial RAG.");
		
		/* Look at the rag creation time. */
		ThreadMXBean ragCreationTimeThread = ManagementFactory.getThreadMXBean();
		long ragCreationStartingTime = ragCreationTimeThread.getCurrentThreadCpuTime();
		
		/**
		 * Define the leaves according to the labels stored in the indexation matrix.
		 * If the pre-segmentation was from the pixels: The number of leaves is the value of the last cell + 1;
		 * If the pre-segmentation was from a random color image: the number of the pixels is determined by the property 'nbRegion' of the pre-Segmented image.
		 */
		if(this.parentTree.preSegFile == null) this.nbLeaves = this.parentTree.indexationMatrix[this.parentTree.indexationMatrix.length-1][this.parentTree.indexationMatrix[0].length-1] + 1;
		else this.nbLeaves = this.parentTree.preSegFile.nbRegions;
		Log.printLvl1(String.valueOf(Context.RAG), "Number of estimated leaves: "+ this.nbLeaves);
		
		/*
		 * Start to save the tree in a file.
		 */
		this.parentTree.treeSave.init(this.nbLeaves);

		/**
		 * Store each point (~ pixel) in the corresponding leaf (~ initial region).
		 */
		Node leaf;
		int label;
		this.listOfNodes = new Node[(this.nbLeaves * 2)-1]; // SPACE FOR ALL NODES OF THE TREE.
		int countLeaf = 0;
		/*
		 * Start saving the list of leaves.
		 */
		this.parentTree.treeSave.write(",\"pixels\":[");
		for(int y = 0; y < this.parentTree.indexationMatrix[0].length; ++y){
			for(int x = 0; x < this.parentTree.indexationMatrix.length; ++x){
				
				label = this.parentTree.indexationMatrix[x][y];
				
				/**
				 * If the leaf is not created yet, build it.
				 */
				leaf = this.listOfNodes[label];
				if(leaf == null){
					leaf = new Node(label, this.parentTree.listOfLists);
					this.listOfNodes[label] = leaf;
					this.parentTree.activeNodes.add(leaf); // ADD THE LEAF IN THE LIST OF ACTIVE NODES.
					countLeaf++;
				}
				
				/**
				 * Add the point (~ pixel) in the leaf (~ region).
				 */
				leaf.points.add((y * this.parentTree.indexationMatrix.length) + x);
				leaf.size++;
				
				ToolsMetric.computeRegionBoundingBox(leaf, this.parentTree.indexationMatrix.length, this.parentTree.indexationMatrix[0].length);
				ToolsMetric.computeBorderPixels(leaf, this.parentTree.indexationMatrix.length, this.parentTree.indexationMatrix[0].length);
				
				/*
				 * Save leaf.
				 */
				this.parentTree.treeSave.write(new StringBuilder("{\"leaf\":").append(leaf.index).append(",\"x\":").append(x).append(",\"y\":").append(y).append("}").toString());
				if(x < this.parentTree.listOfImages.get(0).width - 1) this.parentTree.treeSave.write(",");
				
			}
		}
		Log.printLvl1(String.valueOf(Context.RAG), "Number of created leaves: "+ countLeaf);
		
		/**
		 * Update the value of the MF of the leaves.
		 * Done in a parallel fashion.
		 * (!) this can be done only after adding all points (~ pixels) in the leaf (~ region)
		 */
		Arrays.stream(this.listOfNodes, 0, countLeaf)
		.parallel()
		.forEach(lf->{
			this.parentTree.listOfLists
			.parallelStream()
			.forEach(lw->lw.metric.updateMF(lf));
		});
		
		Log.printLvl1(String.valueOf(Context.RAG), "Leaves' metric features updated!");
		
		/**
		 * - Allocate some spaces in advance for the list of adjacencies.
		 * - Define initial adjacencies.
		 * (!) for now, we are taking account 8 connexities.
		 * (!) cannot be done in a parallel fashion.
		 */
		int imgWidth = this.parentTree.listOfImages.get(0).width; // WIDTH OF THE FIRST IMAGE.
		int imgHeight = this.parentTree.listOfImages.get(0).height; // HEIGHT OF THE FIRST IMAGE.
		int estimatedInitialAdjaNumber = 4 * imgWidth * imgHeight - (3 * imgWidth) - (3 * imgHeight) + 2;// PREPARE THE LIST OF ADJACENCIES. (!) TAKING ACCOUNT 8 CONNEXITIES.
		this.listOfAdjacencies = new Adjacency[estimatedInitialAdjaNumber];
		Log.printLvl1(String.valueOf(Context.RAG), "Defining first adjacencies ...");
		
		Arrays.stream(this.listOfNodes, 0, Node.nodeCounter)
		.forEach(node->{
			
			node.getPoints()
			.stream()
			.forEach(point->{
				Node neighbor;
				int neighborLabel;
				Adjacency newAdja;
				int xp = point % imgWidth; // X POSITION OF THE POINT IN THE IMAGE.
				int yp = point / imgWidth; // Y POSITION OF THE POINT IN THE IMAGE.
				
				/**
				 * 8 connexities.
				 */
				for(int y = yp-1; y <= yp+1; ++y){
					for(int x = xp-1; x <= xp+1; ++x){
						/**
						 * - If the current point (~ pixel) does not have the same label with its neighbors, create the corresponding adjacency.
						 * - If the neighbor is not still recognized by the node, add it to its neighbors list.
						 */
						if((x != xp || y != yp) && this.isInStudiedAread(x, y)){
							neighborLabel = this.parentTree.indexationMatrix[x][y];
							if(neighborLabel != this.parentTree.indexationMatrix[xp][yp]){
								neighbor = this.listOfNodes[neighborLabel];
								if(!node.neighborsLink.contains(new Adjacency(node, neighbor))){
									newAdja = new Adjacency(node, neighbor, this.parentTree.listOfLists, false);
									this.addAdjacency(newAdja);// NEW ADJACENCIES ARE AUTOMATICALLY ADDED IN THE LISTW(S) BUT NOT IN THE RAG.
								}
							}
						}
						
					}
				}
			});
			
		});
	
		long endingTime = ragCreationTimeThread.getCurrentThreadCpuTime();
		Log.printLvl1(String.valueOf(Context.RAG), "RAG generated in : "+ ((endingTime - ragCreationStartingTime)/1000000) +" ms"); // TIME OF FUSION IN MS.
		Log.printLvl1(String.valueOf(Context.RAG), "Initial RAG builded successfully! Number of initial adjacencies: "+ Adjacency.adjacencyCounter);
		
	}

	/**
	 * Create an empty RAG.
	 * @param parentTree The tree owning the RAG.
	 * @param nbLeaves Number of leaves.
	 */
	public RAG(MBPT parentTree, int nbLeaves){
		
		/**
		 * Prepare the list of Nodes to receive pseudo nodes.
		 */
		this.nbLeaves = nbLeaves;
		this.listOfNodes = new Node[(this.nbLeaves * 2)-1]; // SPACE FOR ALL NODES OF THE TREE.
		
	}
	
	/**
	 * Look if a point (~ pixel) is in the scope of the studied zone (~ image).
	 * @param x location of the point (~ pixel) on the column.
	 * @param y location of the point (~ pixel) on the line.
	 * @return True if the point (~ pixel) is in the studied area, else False.
	 */
	public boolean isInStudiedAread(int x, int y) {
		
		/**
		 * IF WE ARE TAKING ACCOUNT THE WHOLE IMAGE.
		 */
		return (x >= 0 && y >=0 && x < this.parentTree.listOfImages.get(0).width && y < this.parentTree.listOfImages.get(0).height);
		
	}
	
	/**
	 * Add an adjacency in the RAG.
	 * @param adja Element to add.
	 */
	public void addAdjacency(Adjacency adja){
	
		adja.index = Adjacency.adjacencyCounter;
		Adjacency.adjacencyCounter++;
		this.listOfAdjacencies[adja.index] = adja;
		
	}
	
	/**
	 * Remove an adjacency from the RAG.
	 * @param adja Element to remove.
	 */
	public void removeAdjacency(Adjacency adja){
		
		int lastValideIndex = Adjacency.adjacencyCounter - 1;
		if(adja.index < lastValideIndex){
			this.listOfAdjacencies[adja.index] = this.listOfAdjacencies[lastValideIndex]; // REPLACE THE ADJACENCY TO REMOVE WITH THE LAST ELEMENT IN THE RAG.
			this.listOfAdjacencies[adja.index].index = adja.index;
		}
		
		this.listOfAdjacencies[lastValideIndex] = null; // FREE THE SPACE THAT WAS STORING THE LAST ADJACENCY IN THE RAG.
		Adjacency.adjacencyCounter--;
		
	}	
	
}
