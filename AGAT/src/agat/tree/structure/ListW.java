package agat.tree.structure;

import agat.metric.support.Metric;
import agat.metric.support.Metric.TypeOfMetric;
import agat.metric.support.MetricFactory;
import agat.tree.structure.Adjacency;
import agat.util.Log;
import agat.util.image.TiffImage;

/**
 * 
 * List ordering all adjacencies of a RAG according to their scores.
 * - For the creation of a standard BPT, there is only one list. The adjacency having the minimum score in this list is the best and allows to determine the two nodes (~ regions) to merge.
 * - In the case of the Multi-features BPT, several lists have to be maintained. The choice of the best adjacency will be managed by a consensual strategy that the user has to choose. 
 * 
 * This list is based on 'TreeMap' so it is maintained stored constantly.
 * Such maintenance is certainly high performance costing.
 * 
 * @author T.J.F.R.
 *
 */
public class ListW {
	
	public enum Context{
		LISTW
	}
	
	/**
	 * All attributes of the ListW.
	 */
	public int index; // IDENTIFICATION OF THE LISTW.
	public Metric metric; // METRIC ASSOCIATED TO THE LISTW.
	public Structure<Adjacency, Integer> structure; // STRUCTURE OF THE LIST. (!) MAINTAINED ORDERED (DESC).
	public int numberOfElements; // NUMBER OF CONTAINED ADJACENCIES.
	
	/**
	 * 
	 * Create a ListW and the corresponding metric.
	 * @param listIndex Index of the listW in the list of listw(s).
	 * @param tiffImg Index of the image to link with the metric.
	 * @param metricType Type of the metric to instantiate.
	 */
	public ListW(int listIndex, TiffImage tiffImg, TypeOfMetric metricType) {
		
		this.index = listIndex;
		this.structure = new Structure<Adjacency, Integer>(this.index, new AdjacencyScoreComparator(this.index));
		this.metric = MetricFactory.initMetric(metricType, tiffImg);
		Log.printLvl1(String.valueOf(Context.LISTW), "ListW linking <Image-"+ this.metric.img.index +", "+ this.metric.type +"> prepared!");
		
	}

	/**
	 * Add an adjacency in the listW.
	 * @param adjacency Element to add in the listW.
	 */
	public void add(Adjacency adjacency) {

		this.structure.put(adjacency, 0);
		this.numberOfElements++;

	}
	
	/**
	 * (!) The number of elements is not the same as the size of the structure of the listW.
	 * Indeed, the structure is ordering groups of adjacencies having the same scores.
	 * @return The number of adjacencies contained in the listW.
	 */
	public int getNumberOfContainedAdjacencies(){
		
		return numberOfElements;
		
	}
	
}
