package agat.consensus.support;

import agat.consensus.strategy.MeanOfRank;
import agat.consensus.strategy.MinOfMin;
import agat.consensus.strategy.MostFrequent;
import agat.consensus.support.Consensus.ConsensusStrategy;

/**
 * 
 * Factory building consensus strategies objects.
 * Each coded strategy class should figure in this class as a choice.
 * 
 * @author T.J.F.R.
 *
 */
public class ConsensusFactory {

	/**
	 *  Build the right consensus strategy and consider its parameters.
	 * @param consensusStrategy The consensus strategy to build.
	 * @param consensusRange The range to consider in the listW(s).
	 * @return A consensus strategy object to use during the creation of the tree.
	 */
	public static Consensus buildonsensusStrategy(ConsensusStrategy consensusStrategy, int consensusRange) {

		switch(consensusStrategy){
		
			case MEAN_OF_RANK:
				return new MeanOfRank(consensusRange);
			case MIN_OF_MIN:
				return new MinOfMin();
			case MOST_FREQUENT:
				return new MostFrequent(consensusRange);
			default:
				return new MeanOfRank(consensusRange);
				
		}

	}

}
