package agat.consensus.support;

import java.util.List;

import agat.tree.structure.Adjacency;
import agat.tree.structure.ListW;

/**
 * 
 * Interface containing all methods that all consensus strategy class should implement.
 * 
 * @author T.J.F.R.
 *
 */
public interface ConsensusInterface {
	
	/**
	 * Application of the consensus strategy.
	 * @param listOfLists List of listW(s) containing various information that will be used by the consensus strategy.
	 * @return The chosen adjacency determining the two nodes (~ regions) to merge.
	 */
	public Adjacency apply(List<ListW> listOfLists);
	
}
