package agat.consensus.support;

import java.util.List;

import agat.tree.structure.Adjacency;
import agat.tree.structure.ListW;

/**
 * 
 * Parent of all consensus strategies.
 * (!) All consensus strategies classes must implement the interface 'agat.consensus.support.ConsensusInterface' and override all its methods.
 * 
 * @author T.J.F.R.
 *
 */
public class Consensus implements ConsensusInterface{

	public enum ConsensusStrategy{
		MEAN_OF_RANK,
		MIN_OF_MIN,
		MOST_FREQUENT
	}

	/**
	 * Attributes of the consensus object.
	 */
	public ConsensusStrategy type;
	public int consensusRange = 100; // 1ST % RANGE TO TAKE ACCOUNT IN EACH LIST. BY DEFAULT, THE WHOLE CONTENT OF EACH LIST IS TAKEN INTO ACCOUNT.
	
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* 
* IMPLEMENTED METHODS FROM THE CONSENSUS INTERFACE
* 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	@Override
	public Adjacency apply(List<ListW> listOfLists) {

		System.err.println(String.valueOf(this.type) +"[WARNING] the method 'agat.consensus.support.ConsensusInterface.apply()' is not implemented!");
		System.exit(0);

		return null;
	}
	
}
