package agat.consensus.strategy;

import java.util.List;
import java.util.Map.Entry;
import agat.consensus.support.Consensus;
import agat.consensus.support.ConsensusInterface;
import agat.tree.structure.Adjacency;
import agat.tree.structure.ListW;

/**
 * 
 * The MEAN_OF_RANK is a consensus strategy of the relative local/global information.
 * All adjacencies are ranked differently in each list. The adjacency having the minimum mean of rank among all lists will be the chosen one. 
 * In order to reduce the computation cost, we only do the sum of the ranks and the choice will be determined by the minimum value of this result.
 * </br>
 * (!) In the case of a relative local information strategy, an interval has to be specified so only a specified range in the listW(s) will be considered.
 * In some cases where the studies adjacency is not present in all listW(s) between this interval, a penalty strategy (has to) may be applied. </br>
 *</br>
 * In the case of the relative global approach, all of the listW(s)' content have to be considered. It is obvious that this kind of strategy will show some performance cost problematic.
 * 
 * @author T.J.F.R.
 *
 */
public class MeanOfRank extends Consensus implements ConsensusInterface{
	
	/**
	 * Relative local/global information consensus strategy based on the mean of the ranks of the adjacencies stored in all listW(s). 
	 * The consensus strategy helps the choice of the adjacency that will determine the two nodes (~ regions) to merge.
	 * @param consensusRange Interval of the 1st ranks of ajdacencies to consider.
	 */
	public MeanOfRank(int consensusRange) {

		this.type = ConsensusStrategy.MEAN_OF_RANK;
		this.consensusRange = consensusRange;

	}
	
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* 
* IMPLEMENTED METHODS FROM THE METRIC INTERFACE
* 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	@Override
	public Adjacency apply(List<ListW> listOfLists) {
		
		Adjacency chosenAdjacency = null;
		double chosenMeanRank = Double.MIN_VALUE;
		
		/**
		 * Set the ranks of the considered adjacencies.
		 */
		int numberOfLists = listOfLists.size();

		/**
		 * If the consensus range is less than 100%:
		 * - determine the number of elements to treat,
		 * - for each list, treat determined adjacencies,
		 * (!) With the new structure that can define for each turn the rank of the adjacency in each list, the strategy penalty is no longer needed.
		 * 
		 * Else (~ the consensus range is 100%):
		 * - browse all adjacencies and compute the sum (or mean) of the ranks.
		 */
		double meanRank;
		if(this.consensusRange < 100){

			/*
			 * Determine the number of elements to treat.
			 */
			int nbElements = listOfLists.get(0).structure.size();
			int nbElementsToTreat = (nbElements * this.consensusRange) / 100;			
			
			for(int il = 0; il < listOfLists.size(); ++il){
				
				int cursor = 0;
				Adjacency adjacency = listOfLists.get(il).structure.lastKey();
				while(cursor <= nbElementsToTreat){
					
					meanRank = 0;
					for(int iil = 0; iil < listOfLists.size(); ++iil){
						meanRank += adjacency.ranks[iil];
					}

					/*
					 * If the computed mean of rank is less than the specified chosen mean of rank or if they are equals and the adjacency's index is less than the chosen one, 
					 * put the adjacency as the new chosen one.
					 */
					if(meanRank > chosenMeanRank || (meanRank == chosenMeanRank && adjacency.compareTo(chosenAdjacency)==-1)){
						chosenAdjacency = adjacency;
						chosenMeanRank = meanRank;
					}

					adjacency = adjacency.previous[il];
					cursor++;
				}

			}
		
		}else{
		
			/**
			 * Compute the mean of ranks.
			 * Done in a sequential way.
			 */
			for(Entry<Adjacency, Integer> entry: listOfLists.get(0).structure.entrySet()){
				Adjacency adjacency = entry.getKey();
				meanRank = 0;
				
				/* sum computation */
				for(int il = 0; il < numberOfLists; ++il){
					meanRank += adjacency.ranks[il];			
				}

				// meanRank /= numberOfLists;

				/*
				 * If the computed mean of rank is less than the specified chosen mean of rank or if they are equals and the adjacency's index is less than the chosen one, 
				 * put the adjacency as the new chosen one.
				 */
				if(meanRank > chosenMeanRank || (meanRank == chosenMeanRank && adjacency.compareTo(chosenAdjacency)==-1)){
					chosenAdjacency = adjacency;
					chosenMeanRank = meanRank;
				}
			}
			
		}
				
        chosenAdjacency.consensusScore = chosenMeanRank;
		return chosenAdjacency;
		
	}
		
	
}
