package agat.consensus.strategy;

import java.util.HashMap;
import java.util.List;

import agat.consensus.support.Consensus;
import agat.consensus.support.ConsensusInterface;
import agat.tree.structure.Adjacency;
import agat.tree.structure.ListW;

/**
 * 
 * The MOST_FREQUENT is a consensus strategy of the relative local information.
 * This strategy focuses only on the first adjacencies included in a specified interval 'r'.
 * The choice of the adjacency will be determined by the adjacency having the most occurrence in the first 'r%' of the lists.
 * 
 * @author T.J.F.R.
 *
 */
public class MostFrequent extends Consensus implements ConsensusInterface{

	/**
	 * Relative local information consensus strategy based on the most frequent adjacencies among a specific interval 'r' firsts adjacencies having low scores. 
	 * The consensus strategy helps the choice of the adjacency that will determine the two nodes (~ regions) to merge.
	 * @param consensusRange Interval of the 1st ranks of ajdacencies to consider.
	 */
	public MostFrequent(int consensusRange) {

		this.type = ConsensusStrategy.MOST_FREQUENT;
		this.consensusRange = consensusRange;
		
	}	

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* 
* IMPLEMENTED METHODS FROM THE CONSENSUS INTERFACE
* 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	@Override
	public Adjacency apply(List<ListW> listOfLists) {
		
		Adjacency chosenAdjacency = null;

		/*
		 * Determine the number of elements to treat.
		 */
		ListW list0 = listOfLists.get(0);
		int nbElements = list0.structure.size();
		int nbElementsToTreat = (nbElements * this.consensusRange) / 100; /* (!) the 'consensusRange' must be specified. */
		
		/* List of all studied adjacencies */
		HashMap<Adjacency, Integer> listOfOccurrencies = new HashMap<Adjacency, Integer>(nbElementsToTreat*listOfLists.size());
		
		/* For each list, browse the specified interval and count the occurences of the adjacencies 
		 * (!) since the listW(s) are ordered by a descendant manner, the last adjacency has the lowest score.
		 */
		for(int il = 0; il < listOfLists.size(); ++il){
			
			ListW listw = listOfLists.get(il);
			int cursor = 0;
			Adjacency adjacency = listw.structure.lastKey();
			
			while(cursor <= nbElementsToTreat){

				if(listOfOccurrencies.get(adjacency) != null) listOfOccurrencies.put(adjacency, listOfOccurrencies.get(adjacency) + 1);
				else listOfOccurrencies.put(adjacency, 1);
				if(chosenAdjacency == null || 
						(listOfOccurrencies.get(adjacency) > listOfOccurrencies.get(chosenAdjacency)
								&& adjacency.compareTo(chosenAdjacency) == -1)){
					chosenAdjacency = adjacency;
				}
				adjacency = adjacency.previous[il];
				cursor++;
			}
			
		}
		
		/*
		 * For tracking the conflict, lets consider the number of occurrences of the chosen adjacency.
		 * TODO - think about it again deeply.
		 */
		chosenAdjacency.consensusScore = listOfLists.size() - listOfOccurrencies.get(chosenAdjacency);
		
		return chosenAdjacency;
	}

}
