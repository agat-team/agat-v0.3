package agat.consensus.strategy;

import java.util.List;
import java.util.Set;
import agat.consensus.support.Consensus;
import agat.consensus.support.ConsensusInterface;
import agat.tree.structure.Adjacency;
import agat.tree.structure.ListW;
import agat.util.TreeSet;

/**
 * 
 * The MIN_OF_MIN is a consensus strategy of the absolute information.
 * This strategy focuses only on the adjacencies having the smallest score in each ordered list.
 * The choice of the adjacency will be determined by the one that have the minimum score value among this focused set of adjacencies.
 * 
 * @author T.J.F.R.
 *
 */
public class MinOfMin extends Consensus implements ConsensusInterface{

	/**
	 * Absolute information consensus strategy based on the minimum score of the mimimum scores of of the adjacencies stored in all listW(s). 
	 * The consensus strategy helps the choice of the adjacency that will determine the two nodes (~ regions) to merge.
	 */
	public MinOfMin() {

		this.type = ConsensusStrategy.MIN_OF_MIN;
		
	}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* 
* IMPLEMENTED METHODS FROM THE CONSENSUS INTERFACE
* 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	/**
	 * Applying the consensus strategy.
	 * @param listOfLists List of list <img, metric> contained in the tree owning this adjacency.
	 * @return The adjacency having the minimum score among all lists.
	 */
	@Override
	public Adjacency apply(List<ListW> listOfLists) {

		Adjacency chosenAdjacency = null;
		
		/* Used for storing the adjacencies having the minimum value of score. There is no redundant element. */
		Set<Adjacency> minAdjacencySet = new TreeSet<Adjacency>();
		
		/* For each listW, take the adjacency having the minimum value and consider it as the chosen one if the its score is lower than the others. 
		 * (!) As the order of our list is descendant, we will pick up the last adjacency.
		 */
		for(int il = 0; il < listOfLists.size(); ++il){
			
			ListW listw = listOfLists.get(il);
			Adjacency adjacency = listw.structure.lastKey();
			minAdjacencySet.add(adjacency);
			
			if(chosenAdjacency == null || adjacency.scores[il] < chosenAdjacency.scores[il]){
				chosenAdjacency = adjacency;
			}
			
		}
		
		/*
		 * For tracking the conflict, lets consider the number of adjacencies having the same score.
		 * TODO - think about it again deeply.
		 */
		chosenAdjacency.consensusScore = listOfLists.get(0).structure.size() - minAdjacencySet.size();

		return chosenAdjacency;
	}

}
