package agat.batch.pr_xp;

import agat.util.Log;
import agat.tree.MBPT;
import agat.tree.MBPT.Context;

/**
 * For cutting MBPT.
 * 
 * @author T.J.F.R
 *
 */
public class MbptXPCut {

	public static void main(String[] args) {
		
		String path = args[0];
		String bptName = args[1];
		int regStart = Integer.valueOf(args[2]);
		int regInter = Integer.valueOf(args[3]);
		
		/**
		 * Show lvl1 messages.
		 */
		Log.showLvl1 = true;
//		Log.setOut(path +"/log.txt");
		
		/**
		 * Print the version of the Agat application.
		 */
		Log.printLvl1(String.valueOf(Context.AGAT), Log.VERSION);

		/**
		 * Create an empty tree.
		 */
		String bptPath = path +"/"+ bptName; 
		MBPT tree = new MBPT(bptPath, false);
		tree.reGrow();
		tree.cut(null, true, regStart, regInter, 10000000);
		
	}

}
