package agat.batch.pr_xp;

import java.io.File;
import java.io.IOException;

import agat.util.Log;
import agat.util.image.CropImage;
import agat.util.image.TiffImage;

/**
 * This class is a batch used for cropping a big image.
 * @author T.J.F.R.
 *
 */
public class CroppingImageForPR {

	
	public static void main(String [] args){

		String dir = new String("experiments//pr_xp");
		String fileName = new String("pleiade_stbg");
		File f = new File(dir +"//"+fileName+".tif");
		
		Log.showLvl1 = true;
		Log.printLvl1("Cropping image for the PR experiences",f.getName());
		TiffImage big =  new TiffImage(f.getAbsolutePath());

		/*
		 * 1st crop.
		 */
		int xPos1 = 4519;
		int yPos1 = 1219;
		String xp1 = "xp1";
		for(int imgSize = 200; imgSize <= 2000; imgSize += 200){
			
			TiffImage small = CropImage.crop(big, imgSize, imgSize, xPos1, yPos1);

			// Saving the crop.
			try {
				
				new File(dir +"//"+ xp1).mkdir();
				String folderPath = new String(dir +"//"+ xp1 +"//"+ imgSize +"x"+ imgSize);
				new File(folderPath).mkdir();
				String newFilePath = folderPath +"//"+ f.getName();
				Log.printLvl1("Saving crop", newFilePath +" saved!");
				TiffImage.save(small, newFilePath);

			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		/*
		 * 2nd crop.
		 */
		int xPos2 = 339; //4526; 
		int yPos2 = 2827; //4306; 
		String xp2 = "xp";
		for(int imgSize = 200; imgSize <= 2000; imgSize += 200){
			
			TiffImage small = CropImage.crop(big, imgSize, imgSize, xPos2, yPos2);

			// Saving the crop.
			try {
				
				new File(dir +"//"+ xp2).mkdir();
				String folderPath = new String(dir +"//"+ xp2 +"//"+ imgSize +"x"+ imgSize);
				new File(folderPath).mkdir();
				String newFilePath = folderPath +"//"+ f.getName();
				Log.printLvl1("Saving crop", newFilePath +" saved!");
				TiffImage.save(small, newFilePath);

			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		Log.printLvl1("Cropping image", "Finished");
	}
	
}
