package agat.batch.pr_xp;

import agat.util.Log;
import agat.util.image.TiffImage;

import agat.metric.support.Metric.TypeOfMetric;
import agat.tree.MBPT;
import agat.tree.MBPT.Context;

/**
 * - Create a standard BPT from one image and using one metric (RADIOMETRIC MIN MAX).</br>
 * - Save the tree in a '.mbpt' file.
 * 
 * @author T.J.F.R
 *
 */
public class Urbain_pleiade_bpt_std {

	public static void main(String[] args) {
		
		/**
		 * Show lvl1 messages.
		 */
		Log.showLvl1 = true;
		
		/**
		 * Print the version of the Agat application.
		 */
		Log.printLvl1(String.valueOf(Context.AGAT), Log.VERSION);
		
		String dir = new String("experiments//pr_xp");
		String fileName = new String("pleiade_stbg");

		/**
		 * Create an empty tree.
		 */
		String xp = "urban";
		int imgSize = 2000;
		String xpDir = new String(dir +"//"+ xp +"//"+ imgSize +"x"+ imgSize +""); 
		String mbptFileName = new String(fileName +"_bpt_std");
		MBPT tree = new MBPT(xpDir +"//"+ mbptFileName +".mbpt", true);
		
		/**
		 * - Register an image by linking it with the tree.
		 * - Associate a metric with the image.
		 */
		TiffImage tiffImg = new TiffImage(xpDir +"//"+ fileName +".tif");
		tree.registerImage(tiffImg);
		tree.linkMetricToAnImage(tiffImg, TypeOfMetric.RADIOMETRIC_MIN_MAX);
		
		/**
		 * Specify the pre-segmented (random color image) file to use.
		 */
		tree.preSegFile = new TiffImage(xpDir +"//preseg_"+ fileName +".tif");
		
		/**
		 * Start to build a standard BPT.
		 */
		tree.grow();
		
	}

}
