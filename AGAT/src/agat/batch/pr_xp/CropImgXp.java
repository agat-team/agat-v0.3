package agat.batch.pr_xp;

import java.io.File;
import java.io.IOException;

import agat.util.Log;
import agat.util.image.CropImage;
import agat.util.image.TiffImage;
import fr.unistra.pelican.ByteImage;
import fr.unistra.pelican.Image;

public class CropImgXp {

	public static void main(String[] args) {
		
		String path = args[0];
		String imgName = args[1];
		int newWidth = Integer.valueOf(args[2]); 
		int newHeight = Integer.valueOf(args[3]);
		int newX = Integer.valueOf(args[4]);
		int newY = Integer.valueOf(args[5]);
		String cropFolderName = args[6];
		
		File f = new File(path +"//"+ imgName);
		
		Log.showLvl1 = true;
		Log.printLvl1("Cropping image for the PR experiences",f.getName());
		
		TiffImage big =  new TiffImage(f.getAbsolutePath());
		TiffImage small = CropImage.crop(big, newWidth, newHeight, newX, newY);

		// Saving the crop.
		try {
			
			String folderPath = new String(path +"//"+ cropFolderName);
			new File(folderPath).mkdir();
			String newFilePath = folderPath +"//"+ f.getName();
			Log.printLvl1("Saving crop", newFilePath +" saved!");
			TiffImage.save(small, newFilePath);

//			BufferedImage image1 = new BufferedImage(small.width, small.height, BufferedImage.TYPE_INT_RGB);
//			Image imageVisu = CropImgXp.constructVisuImage(small.getPiafImage(), true);
//			int rcol , gcol, bcol;
//			int col;
//			for(int xv=0; xv<imageVisu.xdim; ++xv){
//				for(int yv=0; yv<imageVisu.ydim; ++yv){
//					rcol = imageVisu.getPixelXYBByte(xv, yv, 0);
//					gcol = imageVisu.getPixelXYBByte(xv, yv, 1);
//					bcol = imageVisu.getPixelXYBByte(xv, yv, 2);
//					col = (rcol << 16) | (gcol << 8) | bcol;
//					image1.setRGB(xv, yv, col);
//				}
//			}
			
			// Saving to png.
//			String newTiffName[] = f.getName().split(".tif");
//			String newTiffNoExt = newTiffName[0];
//			String newTiffPath = folderPath +"//"+ newTiffNoExt +".png";
//			System.out.println(newTiffPath);
////			BufferedImage tif = ImageIO.read(new File(newFilePath));
//		    ImageIO.write(image1, "png", new File(newTiffPath));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Prepare a visualization image.
	 * @param image
	 * @param readingMode
	 * @return a visualization image.
	 * @author C. Kurtz
	 */
	public static Image constructVisuImage(Image image, Boolean readingMode) {

		int xDim = image.getXDim();
		int yDim = image.getYDim();
		int bDim = image.getBDim();
		int x, y;

		Image res = null;

		switch(bDim){
		case 1: 
			res = new ByteImage(xDim, yDim, 1,1, 3);
			for (y = 0; y < yDim; ++y){
				for (x = 0; x < xDim; ++x){
					res.setPixelByte( x,y,0,0,0, 
							image.getPixelByte( x,y,0,0,0) );

					res.setPixelByte( x,y,0,0,1, 
							image.getPixelByte( x,y,0,0,0) );

					res.setPixelByte( x,y,0,0,2, 
							image.getPixelByte( x,y,0,0,0) );
				}
			}
			break;
		case 2: 
			res = new ByteImage(xDim, yDim, 1,1, 3);
			for (y = 0; y < yDim; ++y){
				for (x = 0; x < xDim; ++y){
					res.setPixelByte( x,y,0,0,0, 
							image.getPixelByte( x,y,0,0,0) );

					res.setPixelByte( x,y,0,0,1, 
							image.getPixelByte( x,y,0,0,0) );

					res.setPixelByte( x,y,0,0,2, 
							image.getPixelByte( x,y,0,0,0) );
				}
			}
			break;	

		case 3:	
			res = image;
			break;
		case 4:	
			res = new ByteImage(xDim, yDim, 1,1, 3);
			for (y = 0; y < yDim; ++y){
				for (x = 0; x < xDim; ++x){

					if(readingMode){
						/*
						 * If the whole image has to be read, there is a chance that this is a MSR ...
						 */
						res.setPixelByte( x,y,0,0,0, 
								image.getPixelByte( x,y,0,0,0) );

						res.setPixelByte( x,y,0,0,1, 
								image.getPixelByte( x,y,0,0,1) );

						res.setPixelByte( x,y,0,0,2, 
								image.getPixelByte( x,y,0,0,2) );
					}else{
						/*
						 * Else, there is a chance that this is a HSR ...
						 */
						res.setPixelByte( x,y,0,0,0, 
								image.getPixelByte( x,y,0,0,2) );

						res.setPixelByte( x,y,0,0,1, 
								image.getPixelByte( x,y,0,0,1) );

						res.setPixelByte( x,y,0,0,2, 
								image.getPixelByte( x,y,0,0,0) );

					}	
				}
			}	
			break;

		case 5:	
			res = new ByteImage(xDim, yDim, 1,1, 3);
			for (y = 0; y < yDim; ++y){
				for (x = 0; x < xDim; ++x){

					if(readingMode){
						/*
						 * If the whole image has to be read, there is a chance that this is a MSR ...
						 */
						res.setPixelByte( x,y,0,0,0, 
								image.getPixelByte( x,y,0,0,0) );

						res.setPixelByte( x,y,0,0,1, 
								image.getPixelByte( x,y,0,0,1) );

						res.setPixelByte( x,y,0,0,2, 
								image.getPixelByte( x,y,0,0,2) );
					}else{
						/*
						 * Else, there is a chance that this is a HSR ...
						 */
						res.setPixelByte( x,y,0,0,0, 
								image.getPixelByte( x,y,0,0,2) );

						res.setPixelByte( x,y,0,0,1, 
								image.getPixelByte( x,y,0,0,1) );

						res.setPixelByte( x,y,0,0,2, 
								image.getPixelByte( x,y,0,0,0) );

					}	
				}
			}	
			break;


		case 6:	
			res = new ByteImage(xDim, yDim, 1,1, 3);
			for (y = 0; y < yDim; ++y){
				for (x = 0; x < xDim; ++x){

					if(readingMode){
						/*
						 * If the whole image has to be read, there is a chance that this is a MSR ...
						 */
						res.setPixelByte( x,y,0,0,0, 
								image.getPixelByte( x,y,0,0,0) );

						res.setPixelByte( x,y,0,0,1, 
								image.getPixelByte( x,y,0,0,1) );

						res.setPixelByte( x,y,0,0,2, 
								image.getPixelByte( x,y,0,0,2) );
					}else{
						/*
						 * Else, there is a chance that this is a HSR ...
						 */
						res.setPixelByte( x,y,0,0,0, 
								image.getPixelByte( x,y,0,0,2) );

						res.setPixelByte( x,y,0,0,1, 
								image.getPixelByte( x,y,0,0,1) );

						res.setPixelByte( x,y,0,0,2, 
								image.getPixelByte( x,y,0,0,0) );

					}	
				}
			}	
			break;

		}

		/**
		 * Using equalization changes the picture's visualization color.
		 */
//		res = Equalization.exec(res);

		return res;	

	}
	
}
