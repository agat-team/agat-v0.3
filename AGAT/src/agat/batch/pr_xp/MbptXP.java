package agat.batch.pr_xp;

import agat.util.Log;
import agat.util.image.TiffImage;
import agat.consensus.support.Consensus.ConsensusStrategy;
import agat.metric.support.Metric.TypeOfMetric;
import agat.tree.MBPT;
import agat.tree.MBPT.Context;

/**
 * For experiments purpose.
 * 
 * @author T.J.F.R
 *
 */
public class MbptXP {

	public static void main(String[] args) {
		
		String path = args[0];
		String imgNames = args[1];
		String img[];
		try{
			img = imgNames.split("-");
		}catch(Exception e){
			img = new String[1];
			img[0] = imgNames;
		}
		String bptName = args[2];
		String preSegName = args[3];
		String features = args[4];
		String[] ft = features.split("-");
		String consensus = args[5];
		int consensusParam = Integer.valueOf(args[6]);
		
		String getOnlyTime = args[7];
		String timeFileName = args[8];
		
		/**
		 * Show lvl1 messages.
		 */
		Log.showLvl1 = true;
		
		if(getOnlyTime.compareTo("getOnlyTime") == 0){
			Log.showLvl1 = false;
			Log.setOut(path +"//"+ timeFileName);
		}
		
		/**
		 * Print the version of the Agat application.
		 */
		Log.printLvl1(String.valueOf(Context.AGAT), Log.VERSION);

		/**
		 * Create an empty tree.
		 */
		String bptPath = path +"/"+ bptName; 
		MBPT tree = new MBPT(bptPath, true);
		
		/**
		 * - Register an image by linking it with the tree.
		 * - Associate a metric with the image.
		 */
		for(int i=0; i<img.length; ++i){
			
			String imgPath = path +"/"+ img[i]; 
			TiffImage tiffImg = new TiffImage(imgPath);
			tree.registerImage(tiffImg);
			
			for(int j=0; j<ft.length; ++j){
				
				if(ft[j].compareTo(TypeOfMetric.RADIOMETRIC_MIN_MAX.toString())==0)
					tree.linkMetricToAnImage(tiffImg, TypeOfMetric.RADIOMETRIC_MIN_MAX);
				if(ft[j].compareTo(TypeOfMetric.RADIOMETRIC_AVERAGE.toString())==0)
					tree.linkMetricToAnImage(tiffImg, TypeOfMetric.RADIOMETRIC_AVERAGE);
				if(ft[j].compareTo(TypeOfMetric.PRECISED_ELONGATION.toString())==0)
					tree.linkMetricToAnImage(tiffImg, TypeOfMetric.PRECISED_ELONGATION);
				if(ft[j].compareTo(TypeOfMetric.SIMPLE_ELONGATION.toString())==0)
					tree.linkMetricToAnImage(tiffImg, TypeOfMetric.SIMPLE_ELONGATION);
				if(ft[j].compareTo(TypeOfMetric.FAST_ELONGATION.toString())==0)
					tree.linkMetricToAnImage(tiffImg, TypeOfMetric.FAST_ELONGATION);
				if(ft[j].compareTo(TypeOfMetric.SMOOTHNESS.toString())==0)
					tree.linkMetricToAnImage(tiffImg, TypeOfMetric.SMOOTHNESS);
				if(ft[j].compareTo(TypeOfMetric.FAST_SMOOTHNESS.toString())==0)
					tree.linkMetricToAnImage(tiffImg, TypeOfMetric.FAST_SMOOTHNESS);
				if(ft[j].compareTo(TypeOfMetric.FAST_COMPACTNESS.toString())==0)
					tree.linkMetricToAnImage(tiffImg, TypeOfMetric.FAST_COMPACTNESS);
				if(ft[j].compareTo(TypeOfMetric.NDVI.toString())==0)
					tree.linkMetricToAnImage(tiffImg, TypeOfMetric.NDVI);
				if(ft[j].compareTo(TypeOfMetric.NDWI.toString())==0)
					tree.linkMetricToAnImage(tiffImg, TypeOfMetric.NDWI);
				
			}
			
		}
		
		/**
		 * Specify the pre-segmented (random color image) file to use.
		 */
		if(preSegName.compareTo("null") != 0){
			String preSegPath = path +"/"+ preSegName;
			tree.preSegFile = new TiffImage(preSegPath);
		}
		
		/**
		 * Specify the consensus strategy.
		 */
		if(consensus.compareTo("null") != 0){
			tree.setConsensusStrategy(ConsensusStrategy.MEAN_OF_RANK, consensusParam);
		}
		
		/**
		 * Start to build a standard BPT.
		 */
		tree.grow();
		
	}

}
