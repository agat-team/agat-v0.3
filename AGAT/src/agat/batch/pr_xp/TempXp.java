package agat.batch.pr_xp;

import agat.util.Log;
import agat.util.image.TiffImage;
import agat.consensus.support.Consensus.ConsensusStrategy;
import agat.metric.support.Metric.TypeOfMetric;
import agat.tree.MBPT;
import agat.tree.MBPT.Context;

/**
 * Class dedicated to temporary experiences.
 * 
 * @author T.J.F.R
 *
 */
public class TempXp {

	public static void main(String[] args) {
		
		/**
		 * Show lvl1 messages.
		 */
		Log.showLvl1 = true;
		
		/**
		 * Print the version of the Agat application.
		 */
		Log.printLvl1(String.valueOf(Context.AGAT), Log.VERSION);

		/**
		 * Create an empty tree.
		 */
		MBPT tree = new MBPT("C://Users//Moi//Documents//development//neon-git-repository//agat-v0.3//AGAT//experiments//pr_xp//agri//400x400//pleiade_stbg.mbpt", true);
		
		/**
		 * - Register an image by linking it with the tree.
		 * - Associate a metric with the image.
		 */
		TiffImage tiffImg = new TiffImage("C://Users//Moi//Documents//development//neon-git-repository//agat-v0.3//AGAT//experiments//pr_xp//agri//400x400//pleiade_stbg.tif");
		tree.registerImage(tiffImg);
		tree.linkMetricToAnImage(tiffImg, TypeOfMetric.RADIOMETRIC_MIN_MAX);
		tree.linkMetricToAnImage(tiffImg, TypeOfMetric.FAST_ELONGATION);
		tree.linkMetricToAnImage(tiffImg, TypeOfMetric.SIMPLE_ELONGATION);
		tree.linkMetricToAnImage(tiffImg, TypeOfMetric.PRECISED_ELONGATION);
		tree.linkMetricToAnImage(tiffImg, TypeOfMetric.NDVI);
		tree.linkMetricToAnImage(tiffImg, TypeOfMetric.NDWI);
	
		/**
		 * Define the consensus strategy to use.
		 * - consider only the first 100% ranks in the lists.
		 */
		tree.setConsensusStrategy(ConsensusStrategy.MEAN_OF_RANK, 15);
		
		/**
		 * Specify the pre-segmented (random color image) file to use.
		 */
		tree.preSegFile = new TiffImage("C://Users//Moi//Documents//development//neon-git-repository//agat-v0.3//AGAT//experiments//pr_xp//agri//400x400//RC-20000.tif");
		
		/**
		 * Start to build a standard BPT.
		 */
		tree.grow();
		
		tree.cut(null, true, 20000, 500, 10000000);

//		MBPT tree = new MBPT("experiments//pr_xp//xp1//1000x1000//pleiade_stbg.mbpt", false);
//		tree.reGrow();
//		tree.cut(null, 100000);
	}

}
