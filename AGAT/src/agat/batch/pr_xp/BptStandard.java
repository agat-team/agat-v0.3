package agat.batch.pr_xp;

import agat.util.Log;
import agat.util.image.TiffImage;
import agat.metric.support.Metric.TypeOfMetric;
import agat.tree.MBPT;
import agat.tree.MBPT.Context;

/**
 * Class dedicated to standard BPT.
 * 
 * @author T.J.F.R
 *
 */
public class BptStandard {

	public static void main(String[] args) {
		
		String path = args[0];
		String imgName = args[1];
		String bptName = args[2];
		String usePreseg = args[3];
		String preSegName = null;
		if(usePreseg.compareTo("yes") == 0)
			preSegName = args[4];
		
		/**
		 * Show lvl1 messages.
		 */
		Log.showLvl1 = true;
		Log.setOut(path +"/log.txt");
		
		/**
		 * Print the version of the Agat application.
		 */
		Log.printLvl1(String.valueOf(Context.AGAT), Log.VERSION);

		/**
		 * Create an empty tree.
		 */
		String bptPath = path +"/"+ bptName; 
		MBPT tree = new MBPT(bptPath, true);
		
		/**
		 * - Register an image by linking it with the tree.
		 * - Associate a metric with the image.
		 */
		String imgPath = path +"/"+ imgName; 
		TiffImage tiffImg = new TiffImage(imgPath);
		tree.registerImage(tiffImg);
		tree.linkMetricToAnImage(tiffImg, TypeOfMetric.RADIOMETRIC_MIN_MAX);
		
		/**
		 * Specify the pre-segmented (random color image) file to use.
		 */
		if(usePreseg.compareTo("yes") == 0){
			String preSegPath = path +"/"+ preSegName;
			tree.preSegFile = new TiffImage(preSegPath);
		}
		
		/**
		 * Start to build a standard BPT.
		 */
		tree.grow();
		
	}

}
