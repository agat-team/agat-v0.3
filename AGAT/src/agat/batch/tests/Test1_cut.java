package agat.batch.tests;

import agat.util.Log;
import agat.tree.MBPT;
import agat.tree.MBPT.Context;

/**
 * - Rebuild a MBPT from a standard '.mbpt' file created from one image and one metric (RADIOMETRIC MIN MAX).</br>
 * - Perform a simple cut (result of 'Test1_build.java') according to a specific number of region.
 * 
 * @author T.J.F.R
 *
 */
public class Test1_cut {

	public static void main(String[] args) {
		
		/**
		 * Show lvl1 messages.
		 */
		Log.showLvl1 = true;
		
		/**
		 * Print the version of the Agat application.
		 */
		Log.printLvl1(String.valueOf(Context.AGAT), Log.VERSION +" - Test1_cut");

		/**
		 * Create an empty tree.
		 */
		int size = 1000;
		String mbptFileName = "Test1("+ size +"x"+ size +")";
		MBPT tree = new MBPT("experiments//toulouse07//"+ size +"x"+ size +"//"+ mbptFileName +".mbpt", false);
		
		/**
		 * Start to rebuild a Multi-features BPT.
		 */
		tree.reGrow();
		
		/**
		 * Perform a cut and generate image results.
		 * - auto-generated output folder,
		 * - start the cutting at 1500 regions,
		 * - for each 100 fusions, perform a cut,
		 * - for each 20000 fusions, store conflict tracking values for the chart.
		 */
		tree.cut(null, true, 1500, 100, 20000);
		
	}

}
