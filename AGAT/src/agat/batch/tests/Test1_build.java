package agat.batch.tests;

import agat.util.Log;
import agat.util.image.TiffImage;
import agat.metric.support.Metric.TypeOfMetric;
import agat.tree.MBPT;
import agat.tree.MBPT.Context;

/**
 * - Create a MBPT from one image and using one metric (RADIOMETRIC MIN MAX).</br>
 * - Save the tree in a '.mbpt' file.
 * 
 * @author T.J.F.R
 *
 */
public class Test1_build {

	public static void main(String[] args) {
		
		/**
		 * Show lvl1 messages.
		 */
		Log.showLvl1 = true;
		
		/**
		 * Print the version of the Agat application.
		 */
		Log.printLvl1(String.valueOf(Context.AGAT), Log.VERSION +" - Test1_build");

		/**
		 * Create an empty tree.
		 */
		int size = 1000;
		String mbptFileName = "Test1("+ size +"x"+ size +")";
		MBPT tree = new MBPT("experiments//toulouse07//"+ size +"x"+ size +"//"+ mbptFileName +".mbpt", true);
		
		/**
		 * - Register an image by linking it with the tree.
		 * - Associate a metric with the image.
		 */
		TiffImage tiffImg = new TiffImage("experiments//toulouse07//"+ size +"x"+ size +"//20071027.tif");
		tree.registerImage(tiffImg);
		tree.linkMetricToAnImage(tiffImg, TypeOfMetric.RADIOMETRIC_MIN_MAX);
		
		/**
		 * Start to build a Multi-features BPT.
		 */
		tree.grow();
		
	}

}
