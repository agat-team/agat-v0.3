package agat.batch.tests;

import agat.util.Log;
import agat.tree.MBPT;
import agat.tree.MBPT.Context;

/**
 * - Create a MBPT from one image and using one color metric (RADIOMETRIC MIN MAX) and one shape metric (SMOOTHNESS).</br>
 * - Perform a simple cut (result of 'Test9_build.java') according to a specific number of region.
 * 
 * @author T.J.F.R
 *
 */
public class Test9_cut {

	public static void main(String[] args) {
		
		/**
		 * Show lvl1 messages.
		 */
		Log.showLvl1 = true;
		
		/**
		 * Print the version of the Agat application.
		 */
		Log.printLvl1(String.valueOf(Context.AGAT), Log.VERSION +" - Test9_cut");

		/**
		 * Create an empty tree.
		 */
		int size = 100;
		String mbptFileName = "Test9("+ size +"x"+ size +")";
		MBPT tree = new MBPT("experiments//toulouse07//"+ size +"x"+ size +"//"+ mbptFileName +".mbpt", false);
		
		/**
		 * Start to rebuild a Multi-features BPT.
		 */
		tree.reGrow();
		
		/**
		 * Perform a cut and generate image results.
		 * - auto-generated output folder,
		 * - cut leading to 150 regions,
		 * - for each 200 fusions, store conflict tracking values for the chart.
		 */
		tree.cut(null, true, 150, 200);
		
	}

}
