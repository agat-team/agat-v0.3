package agat.batch.tests;

import agat.util.Log;
import agat.util.image.TiffImage;
import agat.consensus.support.Consensus.ConsensusStrategy;
import agat.metric.support.Metric.TypeOfMetric;
import agat.tree.MBPT;
import agat.tree.MBPT.Context;

/**
 * - Create a MBPT from one image and using two color metrics (RADIOMETRIC MIN MAX - NDVI).</br>
 * - Consensus strategy used 'MEAN_OF_RANK' and consider 100% of all lists contents. </br>
 * - Save the tree in a '.mbpt' file.</br>
 * 
 * @author T.J.F.R
 *
 */
public class Test13_build {

	public static void main(String[] args) {
		
		int testNum = 13;
		int size = 200;
//		String dir = "experiments//toulouse07//"+ size +"x"+ size;
//		String fileName = "20071027";
		String xp = "xp1";
		String dir = "experiments//pr_xp//"+ xp +"//("+ size +"x"+ size +")";
		String fileName = "pleiade_stbg";

		/**
		 * Show lvl1 messages.
		 */
		Log.showLvl1 = true;
		
		/**
		 * Print the version of the Agat application.
		 */
		Log.printLvl1(String.valueOf(Context.AGAT), Log.VERSION +" - Test"+ testNum +"_build");

		/**
		 * Create an empty tree.
		 */
		String mbptFileName = "Test"+ testNum +"("+ size +"x"+ size +")";
		MBPT tree = new MBPT(dir +"//"+ mbptFileName +".mbpt", true);
		
		/**
		 * - Register the image by linking it with the tree.
		 * - Associate the metrics with the image.
		 */
		TiffImage tiffImg = new TiffImage(dir +"//"+ fileName +".tif");
		tree.registerImage(tiffImg);
		tree.linkMetricToAnImage(tiffImg, TypeOfMetric.RADIOMETRIC_MIN_MAX);
		tree.linkMetricToAnImage(tiffImg, TypeOfMetric.NDVI);
		
		/**
		 * Define the consensus strategy to use.
		 * - consider only the first 100% ranks in the lists.
		 */
		tree.setConsensusStrategy(ConsensusStrategy.MEAN_OF_RANK);
		
		/**
		 * Start to build a Multi-features BPT.
		 */
		tree.grow();
				
	}

}
