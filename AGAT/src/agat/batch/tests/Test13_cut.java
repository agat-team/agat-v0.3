package agat.batch.tests;

import agat.util.Log;
import agat.tree.MBPT;
import agat.tree.MBPT.Context;

/**
 * - Rebuild a MBPT from a standard '.mbpt' file created from one image and two metrics (RADIOMETRIC MIN MAX - NDVI).</br>
 * - Perform a simple cut (result of 'Test13_build.java') according to a specific number of region.
 * 
 * @author T.J.F.R
 *
 */
public class Test13_cut {

	public static void main(String[] args) {
		
		int testNum = 13;
		int size = 200;
//		String dir = "experiments//toulouse07//"+ size +"x"+ size;
		String xp = "xp1";
		String dir = "experiments//pr_xp//"+ xp +"//("+ size +"x"+ size +")";
		
		/**
		 * Show lvl1 messages.
		 */
		Log.showLvl1 = true;
		
		/**
		 * Print the version of the Agat application.
		 */
		Log.printLvl1(String.valueOf(Context.AGAT), Log.VERSION +" - Test"+ testNum +"_cut");

		/**
		 * Create an empty tree.
		 */
		String mbptFileName = "Test"+ testNum +"("+ size +"x"+ size +")";
		MBPT tree = new MBPT(dir +"//"+ mbptFileName +".mbpt", false);
		
		/**
		 * Start to rebuild a Multi-features BPT.
		 */
		tree.reGrow();
		
		/**
		 * Perform a cut and generate image results.
		 * - auto-generated output folder,
		 * - cut leading to 150 regions,
		 * - for each 200 fusions, store conflict tracking values for the chart.
		 */
		tree.cut(null, true, 150, 200);
		
	}

}
