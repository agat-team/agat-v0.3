package agat.batch.tests;

import agat.util.Log;
import agat.util.image.TiffImage;
import agat.consensus.support.Consensus.ConsensusStrategy;
import agat.metric.support.Metric.TypeOfMetric;
import agat.tree.MBPT;
import agat.tree.MBPT.Context;

/**
 * - Create a MBPT from one image and using one color metric (RADIOMETRIC MIN MAX) and one shape metric (SMOOTHNESS).</br>
 * - Consensus strategy used 'MEAN_OF_RANK' and consider 100% of all lists contents.
 * - Save the tree in a '.mbpt' file.</br>
 * 
 * @author T.J.F.R
 *
 */
public class Test9_build {

	public static void main(String[] args) {
		
		/**
		 * Show lvl1 messages.
		 */
		Log.showLvl1 = true;
		
		/**
		 * Print the version of the Agat application.
		 */
		Log.printLvl1(String.valueOf(Context.AGAT), Log.VERSION +" - Test9_build");

		/**
		 * Create an empty tree.
		 */
		int size = 100;
		String mbptFileName = "Test9("+ size +"x"+ size +")";
		MBPT tree = new MBPT("experiments//toulouse07//"+ size +"x"+ size +"//"+ mbptFileName +".mbpt", true);
		
		/**
		 * - Register the image by linking it with the tree.
		 * - Associate the metrics with the image.
		 */
		TiffImage tiffImg = new TiffImage("experiments//toulouse07//"+ size +"x"+ size +"//20071027.tif");
		tree.registerImage(tiffImg);
		tree.linkMetricToAnImage(tiffImg, TypeOfMetric.RADIOMETRIC_MIN_MAX);
		tree.linkMetricToAnImage(tiffImg, TypeOfMetric.SMOOTHNESS);
		
		/**
		 * Define the consensus strategy to use.
		 * - consider only the first 100% ranks in the lists.
		 */
		tree.setConsensusStrategy(ConsensusStrategy.MEAN_OF_RANK);
		
		/**
		 * Start to build a Multi-features BPT.
		 */
		tree.grow();
				
	}

}
