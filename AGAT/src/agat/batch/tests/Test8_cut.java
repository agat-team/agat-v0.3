package agat.batch.tests;

import agat.util.Log;
import agat.tree.MBPT;
import agat.tree.MBPT.Context;

/**
 * - Rebuild a MBPT from a standard '.mbpt' file created from two images and one metric (MOST FREQUENT).</br>
 * - Consensus strategy used 'MIN_OF_MIN' and consider 20% of all lists contents. </br>
 * - Perform a simple cut (result of 'Test7_build.java') according to a specific number of region.
 * 
 * @author T.J.F.R
 *
 */
public class Test8_cut {

	public static void main(String[] args) {
		
		/**
		 * Show lvl1 messages.
		 */
		Log.showLvl1 = true;
		
		/**
		 * Print the version of the Agat application.
		 */
		Log.printLvl1(String.valueOf(Context.AGAT), Log.VERSION +" - Test8_cut");

		/**
		 * Create an empty tree.
		 */
		int size = 100;
		String mbptFileName = "Test8("+ size +"x"+ size +")";
		MBPT tree = new MBPT("experiments//toulouse07//"+ size +"x"+ size +"//"+ mbptFileName +".mbpt", false);
		
		/**
		 * Start to rebuild a Multi-features BPT.
		 */
		tree.reGrow();
		
		/**
		 * Perform a cut and generate image results.
		 * - auto-generated output folder,
		 * - cut leading to 150 regions,
		 */
		tree.cut(null, true, 150);
		
	}

}
