package agat.batch.tests;

import agat.util.Log;
import agat.util.image.TiffImage;
import agat.consensus.support.Consensus.ConsensusStrategy;
import agat.metric.support.Metric.TypeOfMetric;
import agat.tree.MBPT;
import agat.tree.MBPT.Context;

/**
 * - Create a MBPT from two images and using one metric (RADIOMETRIC MIN MAX).</br>
 * - Consensus strategy used 'MEAN_OF_RANK' and consider 11% of all lists contents.
 * - Save the tree in a '.mbpt' file.</br>
 * 
 * @author T.J.F.R
 *
 */
public class Test4_build {

	public static void main(String[] args) {
		
		/**
		 * Show lvl1 messages.
		 */
		Log.showLvl1 = true;
		
		/**
		 * Print the version of the Agat application.
		 */
		Log.printLvl1(String.valueOf(Context.AGAT), Log.VERSION +" - Test4_build");

		/**
		 * Create an empty tree.
		 */
		int size = 100;
		int consensusRange = 11;
		String mbptFileName = "Test4("+ size +"x"+ size +","+ consensusRange +")";
		MBPT tree = new MBPT("experiments//toulouse07//"+ size +"x"+ size +"//"+ mbptFileName +".mbpt", true);
		
		/**
		 * - Register the 1st image by linking it with the tree.
		 * - Associate a metric with the image.
		 */
		TiffImage tiffImg = new TiffImage("experiments//toulouse07//"+ size +"x"+ size +"//20071027.tif");
		tree.registerImage(tiffImg);
		tree.linkMetricToAnImage(tiffImg, TypeOfMetric.RADIOMETRIC_MIN_MAX);
		
		/**
		 * - Register the 2nd image by linking it with the tree.
		 * - Associate a metric with the image.
		 */
		TiffImage tiffImg2 = new TiffImage("experiments//toulouse07//"+ size +"x"+ size +"//20071214.tif");
		tree.registerImage(tiffImg2);
		tree.linkMetricToAnImage(tiffImg2, TypeOfMetric.RADIOMETRIC_MIN_MAX);
		
		/**
		 * Define the consensus strategy to use.
		 * - consider only the first 11% ranks in the lists.
		 * (!) After a first generation of the result by considering 100% of all the lists contents, we noticed that considering 12% also provide the same results with the same accuracies.
		 */
		tree.setConsensusStrategy(ConsensusStrategy.MEAN_OF_RANK, consensusRange);
		
		/**
		 * Start to build a Multi-features BPT.
		 */
		tree.grow();
				
	}

}
