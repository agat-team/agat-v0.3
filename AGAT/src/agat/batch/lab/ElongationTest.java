package agat.batch.lab;

/**
 * Test showing the elongation values of different objects contained in some toy images.
 * @author C. Kurtz
 *
 */
public class ElongationTest {
	
//	public static void computeMetricsOfAnImage(File im){
//		
//		/*
//		 * Load a test image (background in white, object in black).
//		 */
//		TiffImage img = new TiffImage(im.getAbsolutePath());
//		int widthImage= img.width;
//		int heightImage= img.height;
//
//		/*
//		 * Get the list of integer points.
//		 */
//		TreeSet<Integer> pts = new TreeSet<Integer>();
//		for(int x=0;x<widthImage;x++)
//			for(int y=0;y<heightImage;y++)
//				if(img.getPixel(x, y, 0)!=255)
//					pts.add((y * widthImage)+x);
//		
//		/*
//		 * Compute elongation without bounding box of the region
//		 */
//		double elong = Elongation.computeElongation(pts, widthImage, heightImage);
//		
//		System.out.println("Img = "+ im.getName() +" Region size = "+ pts.size() +"  elongation (without bounding boxes) = "+ elong);
//	}
//	
//	
//	/**
//	 * @param args
//	 */
//	public static void main(String[] args) {
//		// Directory where the files are located
//		File dir = new File("experiments//metric_test//elongation");
//
//		// Create a FileFilter that matches ".xml" files
//		FileFilter filter = new FileFilter() {
//		    @Override
//		    public boolean accept(File file) {
//		       return file.isFile() && file.getName().endsWith(".tif");
//		    }
//		};
//
//		// Get pathnames of matching files.
//		File[] paths = dir.listFiles(filter);
//		Arrays.sort(paths);
//		
//		for(File im: paths){
//			computeMetricsOfAnImage(im);
//		}
//		
//	}

}
