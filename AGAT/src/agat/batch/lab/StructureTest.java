package agat.batch.lab;

import java.util.Arrays;
import java.util.Map.Entry;

import agat.tree.structure.Adjacency;
import agat.tree.structure.Adjacency.SpecialState;
import agat.tree.structure.Node;
import agat.tree.structure.Structure;
import agat.tree.structure.AdjacencyScoreComparator;

/**
 * Testing the adapted collection used for the listW that allows the adjacency to know its positions in each list.
 * @author T.J.F.R.
 *
 */
public class StructureTest {
	public static void main(String[] args) {
		
		Structure<Adjacency, Integer> struct =  new Structure<Adjacency, Integer>(0, new AdjacencyScoreComparator(0));
		
		Node n1 = new Node();
		n1.index = 0;
		Node n2 = new Node();
		n2.index = 1;
		Adjacency a0 = new Adjacency(n1, n2);
		a0.index = 0;
		a0.scores = new double[1];
		a0.scores[0] = 0.1;
		a0.ranks = new int[1];
		Arrays.fill(a0.ranks, 1);
		a0.specialTreat = new boolean[1];
		a0.specialTreat[0] = true;
		a0.state = new SpecialState[1];
		a0.previous = new Adjacency[1];
		a0.next = new Adjacency[1];
		
		Node n3 = new Node();
		n3.index = 2;
		Node n4 = new Node();
		n4.index = 3;
		Adjacency a1 = new Adjacency(n3, n4);
		a1.index = 1;
		a1.scores = new double[1];
		a1.scores[0] = 0.15;
		a1.ranks = new int[1];
		Arrays.fill(a1.ranks, 1);
		a1.specialTreat = new boolean[1];
		a1.specialTreat[0] = true;
		a1.state = new SpecialState[1];
		a1.previous = new Adjacency[1];
		a1.next = new Adjacency[1];
		
		Node n5 = new Node();
		n5.index = 4;
		Node n6 = new Node();
		n6.index = 5;
		Adjacency a2 = new Adjacency(n5, n6);
		a2.index = 2;
		a2.scores = new double[1];
		a2.scores[0] = 0.15;
		a2.ranks = new int[1];
		Arrays.fill(a2.ranks, 1);
		a2.specialTreat = new boolean[1];
		a2.specialTreat[0] = true;
		a2.state = new SpecialState[1];
		a2.previous = new Adjacency[1];
		a2.next = new Adjacency[1];
		
		Node n7 = new Node();
		n7.index = 6;
		Node n8 = new Node();
		n8.index = 7;
		Adjacency a3 = new Adjacency(n7, n8);
		a3.index = 3;
		a3.scores = new double[1];
		a3.scores[0] = 1;
		a3.ranks = new int[1];
		Arrays.fill(a3.ranks, 1);
		a3.specialTreat = new boolean[1];
		a3.specialTreat[0] = true;
		a3.state = new SpecialState[1];
		a3.previous = new Adjacency[1];
		a3.next = new Adjacency[1];
		
		Node n9 = new Node();
		n9.index = 8;
		Node n10 = new Node();
		n10.index = 9;
		Adjacency a4 = new Adjacency(n9, n10);
		a4.index = 4;
		a4.scores = new double[1];
		a4.scores[0] = 0.15;
		a4.ranks = new int[1];
		Arrays.fill(a4.ranks, 1);
		a4.specialTreat = new boolean[1];
		a4.specialTreat[0] = true;
		a4.state = new SpecialState[1];
		a4.previous = new Adjacency[1];
		a4.next = new Adjacency[1];
		
		Node n11 = new Node();
		n11.index = 10;
		Node n12 = new Node();
		n12.index = 11;
		Adjacency a5 = new Adjacency(n11, n12);
		a5.index = 5;
		a5.scores = new double[1];
		a5.scores[0] = 0.5;
		a5.ranks = new int[1];
		Arrays.fill(a5.ranks, 1);
		a5.specialTreat = new boolean[1];
		a5.specialTreat[0] = true;
		a5.state = new SpecialState[1];
		a5.previous = new Adjacency[1];
		a5.next = new Adjacency[1];

		Adjacency a6 = new Adjacency(n10, n12);
		a6.index = 6;
		a6.scores = new double[1];
		a6.scores[0] = 2.5;
		a6.ranks = new int[1];
		Arrays.fill(a6.ranks, 1);
		a6.specialTreat = new boolean[1];
		a6.specialTreat[0] = true;
		a6.state = new SpecialState[1];
		a6.previous = new Adjacency[1];
		a6.next = new Adjacency[1];
		
		struct.put(a0, 1);
		struct.put(a1, 1);
		struct.put(a2, 1);
		struct.put(a3, 1);
		struct.put(a5, 1);
		struct.put(a6, 1);
//		struct.updateRanks();
		struct.put(a4, 1);
		
		
//		if(struct.containsKey(a0)) System.out.println("ok");
//		a4.scores[0] = 0.1;
//		a0.normalTreat[0] = true;
//		struct.remove(a2);
//		struct.put(a0, 1);
		
		struct.remove(a4);
//		struct.remove(a3);
		
//		struct.remove(a0);
		struct.remove(a1);
//		System.out.println("update start: "+ struct.updateStart.index);
//		struct.updateRanks();
		
//		struct.remove(a4);
//		struct.remove(a2);
//		struct.remove(a5);
		struct.remove(a6);
//
//		struct.put(a0, 1);
//		struct.put(a1, 1);

//		a2 = new Adjacency(n1, n3);
//		a2.index = 22;
//		a2.scores = new double[1];
//		a2.scores[0] = 0.01;
//		a2.ranks = new int[1];
//		a2.normalTreat = new boolean[1];
//		a2.previous = new Adjacency[1];
//		a2.next = new Adjacency[1];
		
//		a2.specialTreat[0] = true;
//		struct.put(a2, 1);
		
//		struct.put(a3, 1);
//		struct.put(a4, 1);
//		struct.put(a5, 1);
//		struct.put(a6, 1);
//		struct.put(a0, 1);
		
//		struct.remove(a4);
//		struct.remove(a3);
		struct.remove(a0);
//		struct.remove(a4);
//		struct.remove(a1);
//		struct.remove(a2);

//		struct.remove(a2);
//		System.out.println("update start: "+ struct.updateStart.index);
//		struct.updateRanks();
		
		for(Entry<Adjacency, Integer> entry: struct.entrySet()){
			Adjacency a = entry.getKey();
			System.out.print("Adjacency: "+ a.index +"  score: "+ a.scores[0]);
			if(a.previous[0] != null) System.out.print("  prev: "+ a.previous[0].index);
			else System.out.print("  prev: null");
			if(a.next[0] != null) System.out.print("  next: "+ a.next[0].index);
			else System.out.print("  next: null");
			System.out.println("  rank: "+ a.ranks[0]);
		}
	}
}
