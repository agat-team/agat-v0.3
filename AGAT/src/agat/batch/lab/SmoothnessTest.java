package agat.batch.lab;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.TreeSet;

import agat.metric.shape.FastSmoothness;
import agat.metric.shape.Smoothness;
import agat.metric.support.ToolsMetric;
import agat.tree.structure.Node;
import agat.tree.structure.Node.TypeOfNode;
import agat.util.image.TiffImage;

/**
 * Test showing the smoothness values of different objects contained in some toy images.
 * @author C. Kurtz
 *
 */
public class SmoothnessTest {
	
	public static void computeMetricsOfAnImage(File im){
		
		/*
		 * Load a test image (background in white, object in black).
		 */
		TiffImage img = new TiffImage(im.getAbsolutePath());
		int widthImage= img.width;
		int heightImage= img.height;

		/*
		 * Get the list of integer points.
		 */
		TreeSet<Integer> pts = new TreeSet<Integer>();
		for(int x=0;x<widthImage;x++)
			for(int y=0;y<heightImage;y++)
				if(img.getPixel(x, y, 0)!=255)
					pts.add((y * widthImage)+x);
		
		/*
		 * Compute smoothness
		 */
		double smoothness = Smoothness.computeSmoothnessWithMorpho(pts, widthImage, heightImage);
		
		Node node = new Node();
		node.type = TypeOfNode.LEAF;
		node.points = new TreeSet<Integer>();
		node.points.addAll(pts);
		ToolsMetric.computeRegionBoundingBox(node, widthImage, heightImage);
		ToolsMetric.computeBorderPixels(node, widthImage, heightImage);
		
		// Boundig box
		int minX = node.boundingBox[0];
		int maxX = node.boundingBox[1];
		int minY = node.boundingBox[2];
		int maxY = node.boundingBox[3];

		int boundingBoxEdgeLength = 2*((maxX - minX)+(maxY - minY));
		if (boundingBoxEdgeLength == 0)
			boundingBoxEdgeLength=1;
		
		System.out.println("node.borderPoints.size() = "+ node.borderPoints.size() +"  boundingBoxEdgeLength = "+ boundingBoxEdgeLength);
		double fastSmoothness = FastSmoothness.computeSmoothness(node.borderPoints.size(), boundingBoxEdgeLength);
		
		System.out.println("Img = "+ im.getName() +" Region size = "+ pts.size() +"  smoothness = "+ smoothness +"  fast_smoothness = "+ fastSmoothness);
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Directory where the files are located
		File dir = new File("experiments//metric_test//elongation");

		// Create a FileFilter that matches ".xml" files
		FileFilter filter = new FileFilter() {
		    @Override
		    public boolean accept(File file) {
		       return file.isFile() && file.getName().endsWith(".tif");
		    }
		};

		// Get pathnames of matching files.
		File[] paths = dir.listFiles(filter);
		Arrays.sort(paths);
		
		for(File im: paths){
			computeMetricsOfAnImage(im);
		}
		
	}

}
