package agat.batch.lab;

import java.util.ArrayList;
import java.util.List;

final class StreamTest {

    private final String name;
    private final int age;

    public StreamTest(final String name, final int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }



    public static void main(final String[] args) {
        final List<StreamTest> persons = new ArrayList<>();
        persons.add(new StreamTest("Name1", 1));
        persons.add(new StreamTest("Name2", 2));
        persons.add(new StreamTest("Name3", 3));
        persons
            .stream()
            .parallel()
            .forEach(e -> System.out.println(e.getName()));
    }
}
