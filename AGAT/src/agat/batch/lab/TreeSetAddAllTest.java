package agat.batch.lab;

import agat.util.TreeSet;

/**
 * Test the method TreeSet.addAll
 * @author T.J.F.R.
 *
 */
public class TreeSetAddAllTest {

	public static void main(String[] args) {
		
		TreeSet<Integer> collection1 = new TreeSet<Integer>();
		TreeSet<Integer> collection2 = new TreeSet<Integer>();
		TreeSet<Integer> collection3 = new TreeSet<Integer>();

		for(int i=0; i < 10; ++i){
			collection1.add(i);
		}
		
		for(int j=20; j < 30; ++j){
			collection2.add(j);
		}
		
		collection3.addAll(collection1);
		collection3.addAll(collection2);
		
		System.out.println(collection3);
	}

}
