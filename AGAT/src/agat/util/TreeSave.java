package agat.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import agat.consensus.support.Consensus;
import agat.metric.support.Metric.TypeOfMetric;
import agat.tree.MBPT;
import agat.util.image.TiffImage;

/**
 * 
 * Class managing the safety of the tree by writing a file.
 * 
 * @author T.J.F.R.
 *
 */
public class TreeSave {
	
	public enum Context{
		SAVE
	}
	
	/**
	 * All attributes of the class TreeSave.
	 */
	public MBPT parentTree;// TREE TO STORE.
	public File mbptFile; // FILE STORING THE TREE.
	public PrintWriter writer; // BUFFERED WRITER TO USE.
	public boolean saving; // SAVING THE TREE OR NOT.
	
	/**
	 * Prepare the object that will manage the tree saving.
	 * @param parentTree The tree to save.
	 * @param saving <b> True </b> if the saving process has to be done. Else <b> false </b>.
	 */
	public TreeSave(MBPT parentTree, boolean saving) {
		this.parentTree = parentTree;
		this.saving = saving;
	}


	/**
	 * Initialize the saving procedure of the tree in a file.
	 */
	public void init(int nbLeaves) {
		
		/**
		 * Only process when wanted.
		 */
		if(saving){

			/**
			 * Precise the size of the images.
			 * (!) Assuming that the images have the same size, the information is pulled from the first image.
			 */
			int imgWidth = this.parentTree.listOfImages.get(0).width;
			int imgHeight = this.parentTree.listOfImages.get(0).height;
			StringBuilder treeContent = new StringBuilder("{\"w\":").append(imgWidth).append(",\"h\":").append(imgHeight);

			/**
			 * Precise the number of used images.
			 */
			treeContent.append(",\"nbImgs\":").append(this.parentTree.listOfImages.size());

			/**
			 * Precise the information about the pre-segmentation.
			 */
			String preSegName;
			if(this.parentTree.preSegFile != null){

				preSegName = this.parentTree.preSegFile.tiffPath;

			}else preSegName = "none";
			treeContent.append(",\"preSeg\":").append("\"").append(preSegName).append("\"");

			/**
			 * Precise information about the used consensus strategies.
			 */
			treeContent.append(",\"consensus\":[");
			Consensus consensus;
			for(int ic = 0; ic < this.parentTree.consensusStrategies.size(); ++ic){
				
				consensus = this.parentTree.consensusStrategies.get(ic);
				treeContent.append("{\"c\":\"").append(consensus.type).append("\"").append(",\"p\":").append(consensus.consensusRange).append("}");
				if(ic < this.parentTree.consensusStrategies.size() - 1) treeContent.append(",");
				
			}
			if(this.parentTree.consensusStrategies.size() == 0) treeContent.append("{\"c\":\"none\"").append(",\"p\":100}");
			treeContent.append("]");

			/**
			 *  Precise the number of leaves.
			 */
			treeContent.append(",\"nbLeaves\":").append(nbLeaves);

			/**
			 * Precise the images and the used metrics.
			 */
			treeContent.append(",\"data\":[");
			int metricCount = 0;
			TiffImage img;
			for(int i = 0; i < this.parentTree.listOfImages.size(); ++i){

				img = this.parentTree.listOfImages.get(i);
				treeContent.append("{\"img\":\"").append(img.tiffPath).append("\",\"metrics\":[");

				for(TypeOfMetric metric: img.metrics){
					treeContent.append("\"").append(metric).append("\"");
					if(metricCount++ < img.metrics.size() - 1) treeContent.append(",");
				}
				treeContent.append("]}");

				if(i < this.parentTree.listOfImages.size() - 1) treeContent.append(",");

			}
			treeContent.append("]");

			/**
			 * Create the file and start to write in it.
			 */
			try{

				this.writer = new PrintWriter(this.parentTree.mBptFileNamePath, "UTF-8");
				this.writer.print(treeContent.toString());

			}catch(IOException e){
				e.printStackTrace();
			}		
		}
	}
	
	/**
	 * Close the used buffered Writer.
	 */
	public void closeBufferedWriter(){
		
		if(saving) this.writer.close();
		Log.printLvl1(String.valueOf(Context.SAVE), "Tree saved in: \""+ this.parentTree.mBptFileNamePath +"\"");
		
	}

	/**
	 * Write some information about the tree in a file.
	 * @param content Information about the tree.
	 */
	public void write(String content){
		
		if(saving) this.writer.print(content);
		
	}

}
