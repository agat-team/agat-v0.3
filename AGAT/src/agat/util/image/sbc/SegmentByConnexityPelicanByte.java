package agat.util.image.sbc;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import fr.unistra.pelican.Image;
import fr.unistra.pelican.IntegerImage;

/**
 * Create a segment for each "composantes connexe". Use 8-connexity. Use
 * equality for pixels on integer level.
 */
public class SegmentByConnexityPelicanByte {

	// Inputs parameters
	public Image inputImagePelican;
	public HashMap<Integer,ArrayList<Double>> inputRegion;  
	
	public int xDim;
	public int yDim;
	public int bDim;

	// Outputs parameters
	public IntegerImage outputImage;
	HashMap<Integer, Integer> output = new HashMap<Integer, Integer>();
	
	/**
	 * Constructor
	 * 
	 */
	public SegmentByConnexityPelicanByte(Image src) {
		this.inputImagePelican = src;	
		this.xDim = this.inputImagePelican.xdim;
		this.yDim = this.inputImagePelican.ydim;
		this.bDim = this.inputImagePelican.bdim;
		
	}

	public SegmentByConnexityPelicanByte(HashMap<Integer,ArrayList<Double>> regionToSegment, int XDim, int YDim, int BDim) {
		this.inputRegion=regionToSegment;
		xDim=XDim;
		yDim=YDim;
		bDim=BDim;	
	}
	
	
	/**
	 * Assign a label to each region
	 * @return <b>Image</b> that correspond to an <b>IntegerImage</b> containing each label of each region. Each pixel is labeled.
	 */
	public Image runForFullImage(){
		outputImage = new IntegerImage(inputImagePelican.xdim, inputImagePelican
				.ydim, 1, 1, 1);
		
		outputImage.fill(-1);
	
		int label = 0;
		int x, y;
		for (y = 0; y < xDim; ++y)
			for (x = 0; x < yDim; ++x)
				if (outputImage.getPixelXYInt(x, y) == -1)
					newSegmentForFullImage(x, y, label++);
		
		outputImage.setProperty("nbRegions", label);	
		return outputImage;
	}
	
	
	
	public HashMap<Integer,Integer> runForRegions(){
		
		for(int p:inputRegion.keySet()){
			output.put(p,-1);
		}
		
		int label = 0;
		int x, y;
		for(int p:inputRegion.keySet()){
			x=p % xDim;
			y=p / xDim;
			
			if (output.get(p) == -1){
				newSegmentForRegions(x, y, label++);
			}
		}	
		return output;
	}
	
	
	private void newSegmentForFullImage(int x, int y, int label) {
		outputImage.setPixelXYInt(x, y, label);

		Stack<Point> fifo = new Stack<Point>();
		fifo.push(new Point(x, y));

		Point p;
		int l, k;
		while (!fifo.empty()) {
			p = fifo.pop();
			outputImage.setPixelXYInt(p.x, p.y, label);

			// For every pixel in the 8-neighbourhood of the pixel
			for (l = p.y - 1; l <= p.y + 1; l++) {
				for (k = p.x - 1; k <= p.x + 1; k++) {
					if (k < 0 || k >= xDim || l < 0 || l >= yDim)
						continue;
					
					if (!(k == p.x && l == p.y)) {
						
						if (outputImage.getPixelXYInt(k, l) == -1
							&& areEqualsForFullImage(p.x, p.y, k, l)){				
							fifo.push(new Point(k, l));
						}
					}
				}
			}
		}
	}
	
	
	private void newSegmentForRegions(int x, int y, int label) {
		output.put( (y*xDim) + x ,label);

		Stack<Point> fifo = new Stack<Point>();
		fifo.push(new Point(x, y));

		Point p;
		int l, k;
		while (!fifo.empty()) {
			p = fifo.pop();
			output.put( p.y*xDim + p.x ,label);

			// For every pixel in the 8-neighbourhood of the pixel
			for (l = p.y - 1; l <= p.y + 1; ++l) {
				for (k = p.x - 1; k <= p.x + 1; ++k) {
					if (k < 0 || k >= xDim || l < 0|| l >= yDim)
						continue;
					
					if (!(k == p.x && l == p.y) && inputRegion.containsKey(l*(xDim)+k)) {
						if (output.get(l*(xDim)+k) == -1 
							&& areEqualsRegion(p.x, p.y, k, l)){
							fifo.push(new Point(k, l));
						}
					}
				}
			}
		}
	}
	

	private boolean areEqualsForFullImage(int x1, int y1, int x2, int y2) {
		for (int b = 0; b < bDim; b++)
			if (inputImagePelican.getPixelXYBByte(x1, y1, b) 
				!= inputImagePelican.getPixelXYBByte(x2, y2, b))
				return false;
		
		return true;
	}
	
	private boolean areEqualsRegion(int x1, int y1, int x2, int y2) {
		int p1= y1*xDim + x1;	
		int p2= y2*xDim + x2;
		
		for (int b = 0; b < inputRegion.get(p1).size(); ++b){
			if (inputRegion.get(p1).get(b).doubleValue() != inputRegion.get(p2).get(b).doubleValue()){	
				return false;
			}
		}
		return true;
	}
	
}
