package agat.util.image;

/**
 * Cropping image method adapted from the code developed by C. Kurtz.
 * @author T.J.F.R.
 *
 */

public class CropImage {
	
	/**
	 * Specify the coordinates and the size of the crop.
	 * 
	 * @param input
	 * @param newDimX
	 * @param newDimY
	 * @param xDepart
	 * @param yDepart
	 * @return
	 */
	public static TiffImage crop(TiffImage input, int newDimX,int newDimY,int xDepart,int yDepart){
		
		//Building a smaller image.
		TiffImage imgPetite= new TiffImage(newDimX,newDimY,input.nbBands);
		for(int x=xDepart;x<(imgPetite.width+xDepart);x++){
			for(int y = yDepart;y<(imgPetite.height+yDepart);y++){
				for(int b = 0;b<imgPetite.nbBands;b++){
//					System.out.println(input.getPixel(x, y, b));
					imgPetite.setPixel((x-xDepart), (y-yDepart), b, input.getPixel(x, y, b));
				}			
			}
		}
		return imgPetite;
		
	}

}
