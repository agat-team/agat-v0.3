package agat.util.image;

import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.SampleModel;
import java.io.IOException;
import java.util.TreeSet;

import javax.media.jai.JAI;
import javax.media.jai.ParameterBlockJAI;
import javax.media.jai.PlanarImage;
import javax.media.jai.RasterFactory;
import javax.media.jai.TiledImage;

import com.sun.media.jai.codec.TIFFEncodeParam;

import fr.unistra.pelican.DoubleImage;
import fr.unistra.pelican.Image;
import mustic.io.PiLLiTools;
import mustic.io.geotiff.GEOTiffImage;
import agat.metric.support.Metric.TypeOfMetric;
import agat.util.Log;

/**
 * 
 * Reading, storing and managing a TIFF image.
 * This class is based on the class 'TiffImage' written by C. Kurtz.
 * This last requires a another class named 'GEOTiffImage' and owned by Mustic.
 * 
 * @author T.J.F.R.
 *
 */
public class TiffImage {

	public enum Context{
		TIFF_IMG
	}
	
	/**
	 * All attributes of the image
	 */
	public int index;
	public String tiffPath;
	public int width;
	public int height;
	public int nbBands;
	public double dataMatrix[][][]; // MATRIX STORING ALL THE PIXEL VALUES.
	public TreeSet<TypeOfMetric> metrics = new TreeSet<TypeOfMetric>(); // LINKED METRICS.
	public double min;
	public double max;
	public int nbRegions;
	
	/**
	 * Read the tiff image and store the pixels values in the data matrix.
	 * @param tiffPath
	 */
	public TiffImage(String tiffPath){
		
		this.tiffPath = tiffPath;
		Log.printLvl1(String.valueOf(Context.TIFF_IMG), "Reading the content of \""+ this.tiffPath +"\"");
		
		/**
		 * - Initializing the attributes.
		 * - Copying the pixel values in the data Matrix.
		 */
		GEOTiffImage geoImg = new GEOTiffImage(tiffPath);
		this.width = geoImg.width;
		this.height = geoImg.height;
		this.nbBands = geoImg.getNbBands();
		this.dataMatrix = geoImg.load(0, 0, this.width, this.height);
		
		/**
		 * Set the minimum and the maximum pixel values.
		 */
		this.setMinMax();
		
	}
	
	/**
	 * Create an empty tiff image.
	 * @param newDimX
	 * @param newDimY
	 * @param nbBands
	 */
	public TiffImage(int newDimX, int newDimY, int nbBands) {

		Log.printLvl1(String.valueOf(Context.TIFF_IMG), "New Raw Image Tiff");

		this.height = newDimY;
		this.width = newDimX;
		this.nbBands = nbBands;
		this.dataMatrix = new double[nbBands][newDimX][newDimY];

	}

	/**
	 * @param x Position in the column.
	 * @param y Position in the line.
	 * @param b Corresponding channel (~ band).
	 * @return the pixel value at the specified position and corresponding to the right channel.
	 */
	public double getPixel(int x, int y, int b){
		
		return this.dataMatrix[b][x][y];
		
	}
	
	/**
	 * Set the minimum and the maximum pixel values.
	 */
	public void setMinMax(){
		
		this.min = Double.MAX_VALUE;
		this.max = Double.MIN_VALUE;
		
		for (int x = 0; x < this.width; x++) {
			for (int y = 0; y < this.height; y++) {
				for (int b = 0; b < this.nbBands; b++) {
					double c = this.getPixel(x, y, b);

					if (c < this.min)
						this.min = c;
					if (c > max)
						this.max = c;

				}
			}
		}
	}
	
	/**
	 * Generate a visualization image with normalized values of the pixels.
	 * @return a visualization image based on an initial tiff image.
	 * @author C. Kurtz
	 */
	public Image getPiafImage() {
		Image image = new DoubleImage(
				this.width, this.width, 1, 1, this.nbBands);
		
		double factor = 1.0 / (this.max - this.min);

		/**
		 * Set new values to pixels.
		 */
		for (int x = 0; x < this.width; x++) {
			for (int y = 0; y < this.height; y++) {
				for (int b = 0; b < this.nbBands; b++) {
					double pixelValue = this.getPixel(x, y, b);
					image.setPixelXYBDouble(x, y, b, (pixelValue - this.min) * factor);
				}
			}
		}

		return image;
	}

	/**
	 * Set a new pixel value.
	 * @param x
	 * @param y
	 * @param b
	 * @param value
	 */
	public void setPixel(int x, int y, int b, double value) {
		this.dataMatrix[b][x][y] = value;
	}

	/**
	 * Save the image in a file.
	 * @param img
	 * @param path
	 * @throws IOException
	 */
	static public void save(TiffImage img, String path) throws IOException {
		int x = 0;
		int y = 0;
		
		SampleModel sampleModel = null;
		sampleModel = RasterFactory.createBandedSampleModel(
				DataBuffer.TYPE_FLOAT, img.getWidth(), img.getHeight(), img.getNbBands());
		
		// create a compatible colour model
		ColorModel colorModel = PlanarImage.createColorModel(sampleModel);

		TiledImage output = new TiledImage(0, 0, img.getWidth(), img
				.getHeight(), 0, 0, sampleModel, colorModel);

		// we create the image
		for (x = 0; x < img.getWidth(); x++) {
			for (y = 0; y < img.getHeight(); y ++) {
				for (int b = 0; b < img.getNbBands(); b++) {
					output.setSample(x , y , b, (float) img.getPixel(x, y, b));
				}	
			}
		}

		// we create metadata
		TIFFEncodeParam metadata = new TIFFEncodeParam();
		metadata.setCompression(TIFFEncodeParam.COMPRESSION_NONE);


		try {
			ParameterBlockJAI storePB = new ParameterBlockJAI("FileStore");
			storePB.addSource(output);
			storePB.setParameter("fileName", path);
			storePB.setParameter("format", "tiff");
			storePB.setParameter("param", metadata);
			storePB.setParameter("checkFileLocally", false); // don't check
																// if this file
																// exists
			JAI.create("filestore", storePB);
		} catch (Exception e) {
			PiLLiTools.error("Error while writing the geotiff image",
					"GEOTiffImage.java", "save(RawImage, path)");
			return;
		}
	}

	/**
	 * 
	 * @return The number of bands.
	 */
	public int getNbBands() {
		return this.nbBands;
	}

	/**
	 * 
	 * @return The height of the image.
	 */
	public int getHeight() {
		return this.height;
	}

	/**
	 * 
	 * @return The width of the image.
	 */
	public int getWidth() {
		return this.width;
	}
	
}
