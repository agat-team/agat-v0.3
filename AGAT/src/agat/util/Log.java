package agat.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

/**
 * 
 * Log printing information according to its importance.
 * There are three levels of printing :
 * - lvl1 (most important message)
 * - lvl2 (normal message)
 * - lvl3 (less important message)
 * For a performance purpose, each printing method of each level are separated although they have actually the same content.
 * 
 * @author T.J.F.R.
 *
 */
public class Log {
	
	/**
	 * Static attributes allowing to print or not a message.
	 */
	public static boolean showLvl1 = false;
	public static boolean showLvl2 = false;
	public static boolean showLvl3 = false;
	
	/**
	 * Version of the AGAT application
	 */
	public static final String VERSION = "AGAT-v0.3";
	
	public static void setOut(String logPath){
		PrintStream printStream;
		try {
			printStream = new PrintStream(new File(logPath));
			System.setOut(printStream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* 
* METHODS FOR THE LVL - 1
* 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	
	/**
	 * Print a lvl1 message according to a specified context.
	 * @param context
	 * @param message
	 */
	public static void printLvl1(String context, String message){
		
		if(Log.showLvl1){
			final StringBuilder info = new StringBuilder("[");
			info.append(context);
			info.append("] ");
			info.append(message);
			System.out.println(info);
		}
		
	}
	
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * METHODS FOR THE LVL - 2
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	
	/**
	 * Print a lvl2 message according to a specified context.
	 * @param context
	 * @param message
	 */
	public static void printLvl2(String context, String message){
		
		if(Log.showLvl2){
			final StringBuilder info = new StringBuilder("[");
			info.append(context);
			info.append("] ");
			info.append(message);
			System.out.println(info);
		}
		
	}
	
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * METHODS FOR THE LVL - 3
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	
	/**
	 * Print a lvl3 message according to a specified context.
	 * @param context
	 * @param message
	 */
	public static void printLvl3(String context, String message){
		
		if(Log.showLvl3){
			final StringBuilder info = new StringBuilder("[");
			info.append(context);
			info.append("] ");
			info.append(message);
			System.out.println(info);
		}
		
	}
	
}
