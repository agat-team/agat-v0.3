package agat.metric.support;

import agat.metric.color.NdviMetric;
import agat.metric.color.NdwiMetric;
import agat.metric.color.RadiometricAverage;
import agat.metric.color.RadiometricMinMax;
import agat.metric.shape.Elongation;
import agat.metric.shape.FastCompactness;
import agat.metric.shape.FastSmoothness;
import agat.metric.shape.Smoothness;
import agat.metric.support.Metric.TypeOfMetric;
import agat.util.image.TiffImage;

/**
 * 
 * Factory building the right metric objects.
 * Each coded metric class should figure in this class as a choice.
 * 
 * @author T.J.F.R.
 *
 */
public class MetricFactory {

	/**
	 * Choose and build the right metric and associate it to an image.
	 * @param metricType
	 * @param tiffImg
	 * @return the right metric object.
	 */
	public static Metric initMetric(TypeOfMetric metricType, TiffImage tiffImg) {

		switch(metricType){
		
			case RADIOMETRIC_MIN_MAX:
				return new RadiometricMinMax(tiffImg);
			case RADIOMETRIC_AVERAGE:
				return new RadiometricAverage(tiffImg);
			case PRECISED_ELONGATION:
				return new Elongation(TypeOfMetric.PRECISED_ELONGATION, tiffImg);
			case SIMPLE_ELONGATION:
				return new Elongation(TypeOfMetric.SIMPLE_ELONGATION, tiffImg);
			case FAST_ELONGATION:
				return new Elongation(TypeOfMetric.FAST_ELONGATION, tiffImg);
			case SMOOTHNESS:
				return new Smoothness(tiffImg);
			case FAST_SMOOTHNESS:
				return new FastSmoothness(tiffImg);	
			case FAST_COMPACTNESS:
				return new FastCompactness(tiffImg);	
			case NDVI:
				return new NdviMetric(tiffImg);
			case NDWI:
				return new NdwiMetric(tiffImg);
			default:
				return new RadiometricMinMax(tiffImg);
				
		}
		
	}


}
