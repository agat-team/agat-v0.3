package agat.metric.support;

import agat.tree.structure.Node;
import agat.util.image.TiffImage;

/**
 * 
 * Parent of all metrics.
 * (!) All metric classes must implement the interface 'MetricInterface' and override all its methods.
 * 
 * @author T.J.F.R.
 *
 */
public class Metric implements MetricInterface{

	public enum Context{
		METRIC
	}
	
	/**
	 *
	 * Each metric class must be associated to a precise type.
	 * The type will help the factory to build the right metric object.
	 *
	 */
	public static enum TypeOfMetric{
		
		METRIC,
		RADIOMETRIC_MIN_MAX,
		RADIOMETRIC_AVERAGE,
		PRECISED_ELONGATION,
		SIMPLE_ELONGATION,
		FAST_ELONGATION,
		SMOOTHNESS,
		FAST_SMOOTHNESS,
		FAST_COMPACTNESS,
		NDVI,
		NDWI
		
	}
	
	/**
	 * All attributes of the metric.
	 */
	public TypeOfMetric type;
	public TiffImage img;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* 
* IMPLEMENTED METHODS FROM THE METRIC INTERFACE
* 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	
	/**
	 * Prepare all the Metric Features (MF) corresponding to the chosen metric.
	 * @param n
	 */
	@Override
	public void prepareMF(Node n) {

		System.err.println(String.valueOf(Context.METRIC) +"[WARNING] the method 'agat.metric.support.MetricInterface.prepareMf(Node n)' is not implemented!");
		System.exit(0);
		
	}
	
	/**
	 * Initiate or update the values of the Metric Features (MF).
	 * @param n
	 */
	@Override
	public void updateMF(Node n) {

		System.err.println(String.valueOf(Context.METRIC) +"[WARNING] the method 'agat.metric.support.MetricInterface.updateMF(Node n)' is not implemented!");
		System.exit(0);
		
	}
	
	/**
	 * Compute a distance between 'n1' and 'n2'.
	 * @param n1
	 * @param n2
	 * @return A score (~ distance) between 'n1' and 'n2'.
	 */
	@Override
	public double computeDistances(Node n1, Node n2) {

		System.err.println(Context.METRIC +"[WARNING] the method 'agat.metric.support.MetricInterface.computeDistances(Node n1, Node n2)' is not implemented!");
		System.exit(0);
		
		return Double.MAX_VALUE;
		
	}
	
}
