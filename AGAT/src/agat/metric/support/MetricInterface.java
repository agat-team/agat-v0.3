package agat.metric.support;

import agat.tree.structure.Node;

/**
 * 
 * Interface containing all methods that all metric class should implement.
 * 
 * @author T.J.F.R.
 *
 */
public interface MetricInterface {

	/**
	 * Prepare all the Metric Features (MF) corresponding to the chosen metric.
	 * @param n
	 */
	public void prepareMF(Node n);
	
	/**
	 * Initiate or update the values of the Metric Features (MF).
	 * @param n
	 */
	public void updateMF(Node n);
	
	/**
	 * Compute a distance between 'n1' and 'n2'.
	 * @param n1
	 * @param n2
	 * @return A score (~ distance) between 'n1' and 'n2'.
	 */
	public double computeDistances(Node n1, Node n2);
	
}
