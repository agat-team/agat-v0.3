package agat.metric.color;

import java.util.ArrayList;

import agat.metric.support.Metric;
import agat.metric.support.MetricInterface;
import agat.tree.structure.Node;
import agat.util.Log;
import agat.util.image.TiffImage;

/**
 * 
 * Metric based on the radiometric intensity of each region (combination of pixels) of each region.
 * The computation of the distance between two regions requires the average of the intensity values of the whole pixels of the image and among the channels (~ bands).
 * (!) All metric classes must inherit from the 'Metric' class and implement the interface 'MetricInterface' and override all its methods.
 * 
 * @author T.J.F.R.
 *
 */
public class RadiometricAverage extends Metric implements MetricInterface{

	/**
	 * All attributes of the radiometric average metric.
	 */
	ArrayList<Integer> avgPos = new ArrayList<Integer>(); // FOR EACH BAND, LOCATION OF THE AVERAGE FEATURE IN EACH NODE.
	
	/**
	 * Register an image within the metric and create the radiometric object based on the average values of the pixels.
	 * @param tiffImg
	 */
	public RadiometricAverage(TiffImage tiffImg) {
		
		this.type = TypeOfMetric.RADIOMETRIC_AVERAGE;
		this.img = tiffImg;
		
		/**
		 * Store the metric in the list of metrics in the image object.
		 */
		this.img.metrics.add(this.type);
		
		/**
		 * Allocate spaces for the 'avgPos' array list.
		 */
		this.avgPos = new ArrayList<Integer>(this.img.nbBands);
		
		Log.printLvl1(String.valueOf(this.type), "Metric prepared!");
		
	}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* 
* IMPLEMENTED METHODS FROM THE METRIC INTERFACE
* 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	
	/**
	 * Prepare all the Metric Features (MF) corresponding to the radiometric intensity of the specified region (~ node):</br>
	 * - average: average of the pixels of the region among the channels (~ bands).</br>
	 * @param n Concerned node.
	 */
	@Override
	public void prepareMF(Node n) {

		for(int b = 0; b < this.img.nbBands; ++b){
		
			/**
			 * - Define the average pixel value position in the list of MF.
			 * - Initialize the average value with '0.0'.
			 */
			if(this.avgPos.size() < this.img.nbBands){
				this.avgPos.add(n.metricFeatures.size());
			}
			n.metricFeatures.add(0.0); // ADDING FICTIONNAL INITIAL AVERAGE VALUE.
		
		}
		
	}

	/**
	 * Initiate or update the values of the Metric Features (MF):</br>
	 * - average: average value of the pixels of the region among the channels (~ bands).</br>
	 * @param n Concerned node.
	 */
	@Override
	public void updateMF(Node n) {
		
		switch(n.type){
		
			case LEAF: /* GET THE AVERAGE PIXEL VALUE FOR EACH CHANNEL (~ BAND). */
				for(int b = 0; b < this.img.nbBands; ++b){
					
					int avgPosb = this.avgPos.get(b);
					double sumPixelValues = 0.0;
					for(Integer point: n.getPoints()){
						
						double pixelValue = this.img.getPixel(point % this.img.width, point / this.img.width, b);
						sumPixelValues += pixelValue;
						
					}
					n.metricFeatures.set(avgPosb, sumPixelValues);
					
				}
				break;
			default: /* COMPUTE THE AVERAGE PIXEL VALUES BETWEEN THE TWO DIRECT SUB-REGIONS (CHILDREN) */
				for(int b = 0; b < this.img.nbBands; ++b){
					
					int avgPosb = this.avgPos.get(b);
					Node leftChild = n.leftChild;
					Node rightChild = n.rightChild;
					double radiometricAvg = ((n.leftChild.metricFeatures.get(avgPosb) * leftChild.size)
							+ (rightChild.metricFeatures.get(avgPosb) * rightChild.size))
							/ (leftChild.size + rightChild.size);
					n.metricFeatures.set(avgPosb, radiometricAvg);
					
				}
		}
	}

	/**
	 * Compute a distance between 'n1' and 'n2' using the Metric Features (MF):
	 * - average: average value of the pixels of the region among the channels (~ bands).
	 * @param n1 First Node.
	 * @param n2 Second Node.
	 * @return A score (~ distance) between 'n1' and 'n2'.
	 */
	@Override
	public double computeDistances(Node n1, Node n2) {
		
		double score = 0;

		for(int b = 0; b < this.img.nbBands; ++b){
			
			/**
			 * Compute the difference between the two nodes (~ regions) and to the sum of all values of each channel (~ band).
			 */
			int avgPosb = this.avgPos.get(b);
			score += Math.abs(n2.metricFeatures.get(avgPosb) - n1.metricFeatures.get(avgPosb));
			
		}
		
		return score;
		
	}
	
}
