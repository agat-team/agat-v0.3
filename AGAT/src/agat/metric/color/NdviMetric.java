package agat.metric.color;

import agat.metric.support.Metric;
import agat.metric.support.MetricInterface;
import agat.tree.structure.Node;
import agat.util.Log;
import agat.util.image.TiffImage;

/**
 * This class compute NDVI values of each region (~ node). </br>
 * The NDVI value is between -1 (no vegetation) and 1 (full of vegetation).
 * @author T.J.F.R
 *
 */
public class NdviMetric extends Metric implements MetricInterface{

	/**
	 * Position of the ndvi in the metric features (MF) list.
	 */
	int ndviPos;
	
	int rindex;
	int nirindex;
	
	/**
	 * Register an image within the metric and create the ndvi metric based on the difference-sum ratio between the Red and the Near Infrared Red bands.
	 * @param tiffImg
	 */
	public NdviMetric(TiffImage tiffImg) {
		
		this.type = TypeOfMetric.NDVI;
		this.img = tiffImg;
		
		/**
		 * Store the metric in the list of metrics in the image object.
		 */
		this.img.metrics.add(this.type);
		
		/*
		 * (!) For this prototype, the R and NIR bands are not accurates.
		 */
		this.rindex = 0;
		this.nirindex = 0;
		if(this.img.nbBands > 1){
			this.rindex = 0;
			this.nirindex = this.img.nbBands -1;
		}
		
		Log.printLvl1(String.valueOf(this.type), "Metric prepared!");
		
	}
	
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* 
* IMPLEMENTED METHODS FROM THE METRIC INTERFACE
* 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	/**
	 * Prepare all the Metric Features (MF) corresponding to the NDVI value of the specified region (~ node).</br>
	 * @param n Concerned node.
	 */
	@Override
	public void prepareMF(Node n) {

		this.ndviPos = n.metricFeatures.size();
		n.metricFeatures.add(-1.0); // ADDING FICTIONNAL INITIAL NDVI VALUE.

	}

	/**
	 * Initiate or update the NDVI values in the Metric Features list (MF). </br>
	 * (!) For this prototype, let's assume that the R band index and the NIR band index are respectively the first and the last bands.
	 * @param n Concerned node.
	 */
	@Override
	public void updateMF(Node n) {

		double ndvi; /* NDVI = (NIR - R) / (NIR + R) */
		double r; // value on the Red band.
		double nir; // value on the NIR band.
		
		switch(n.type){

		/* Compute the NDVI value corresponding to the region (~ node). */
		case LEAF: 

			double meanR = 0.0; // mean of the Red pixels values of the region.
			double meanNIR = 0.0; // mean of the NIR pixels values of the region.
			for(Integer point: n.getPoints()){

				int x = point % this.img.width;
				int y = point / this.img.width;
				r = this.img.getPixel(x, y, this.rindex);
				nir = this.img.getPixel(x, y, this.nirindex);
				meanR += r;
				meanNIR += nir;
				
			}
			meanR /= n.size;
			meanNIR /= n.size;
			
			ndvi = (meanNIR - meanR) / (meanNIR + meanR);
			n.metricFeatures.set(this.ndviPos, ndvi);

			break;
		default: // node case.
			ndvi = (n.leftChild.metricFeatures.get(this.ndviPos) * n.leftChild.size + n.rightChild.metricFeatures.get(this.ndviPos) * n.rightChild.size) / (n.leftChild.size +n.rightChild.size) ;
			n.metricFeatures.set(this.ndviPos, ndvi);
		}
	}

	/**
	 * Compute a distance between 'n1' and 'n2' using the Metric Features (MF).
	 * @param n1 First Node.
	 * @param n2 Second Node.
	 * @return A score (~ distance) between 'n1' and 'n2'.
	 */
	@Override
	public double computeDistances(Node n1, Node n2) {

		return Math.abs(n1.metricFeatures.get(this.ndviPos) - n2.metricFeatures.get(this.ndviPos));

	}
	
}
