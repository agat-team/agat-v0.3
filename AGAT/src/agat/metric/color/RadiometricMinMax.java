package agat.metric.color;

import java.util.ArrayList;

import agat.metric.support.Metric;
import agat.metric.support.MetricInterface;
import agat.tree.structure.Node;
import agat.util.Log;
import agat.util.image.TiffImage;

/**
 * 
 * Metric based on the radiometric intensity of each region (combination of pixels) of each region.
 * The computation of the distance between two regions requires the minimum and the maximum values of the whole pixels of the image and among the channels (~ bands).
 * (!) All metric classes must inherit from the 'Metric' class and implement the interface 'MetricInterface' and override all its methods.
 * 
 * @author T.J.F.R.
 *
 */
public class RadiometricMinMax extends Metric implements MetricInterface {
	
	public enum Context{
		RADIOMETRIC_MIN_MAX
	}
	
	/**
	 * All attributes of the radiometric min max metric.
	 */
	ArrayList<Integer> minPos; // FOR EACH BAND, LOCATION OF THE MIN FEATURE IN EACH NODE.
	ArrayList<Integer> maxPos; // FOR EACH BAND, LOCATION OF THE MAX FEATURE IN EACH NODE.
	
	/**
	 * Register an image within the metric and create the radiometric object based on the min and the max values of the pixels.
	 * @param tiffImg
	 */
	public RadiometricMinMax(TiffImage tiffImg) {
		
		this.type = TypeOfMetric.RADIOMETRIC_MIN_MAX;
		this.img = tiffImg;
		
		/**
		 * Store the metric in the list of metrics in the image object.
		 */
		this.img.metrics.add(this.type);
		
		/**
		 * Allocate spaces for the 'minPos' and 'maxPos' array lists.
		 */
		this.minPos = new ArrayList<Integer>(this.img.nbBands);
		this.maxPos = new ArrayList<Integer>(this.img.nbBands);
		
		Log.printLvl1(String.valueOf(Context.RADIOMETRIC_MIN_MAX), "Metric prepared!");
		
	}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* 
* IMPLEMENTED METHODS FROM THE METRIC INTERFACE
* 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	
	/**
	 * Prepare all the Metric Features (MF) corresponding to the radiometric intensity of the specified regions (~ node) such as:</br>
	 * - min: minimum value of the pixels of the region among the channels (~ bands).</br>
	 * - max: maximum value of the pixels of the among the channels (~ bands).</br>
	 * @param n Concerned node.
	 */
	@Override
	public void prepareMF(Node n) {

		for(int b = 0; b < this.img.nbBands; ++b){
		
			// TODO - create another method preparing the positions and only keep here the initialization.
			
			/**
			 * - Define the minimum pixel value position in the list of MF.
			 * - Initialize the minimum value with the possible maximum value of double.
			 */
			if(this.minPos.size() < this.img.nbBands){
				this.minPos.add(n.metricFeatures.size());
			}
			n.metricFeatures.add(Double.MAX_VALUE); // ADDING FICTIONNAL INITIAL MINIMUM VALUE.
			
			/**
			 * - Define the maximum pixel value position in the list of MF.
			 * - Initialize the maximum value with the possible minimum value of double.
			 */
			if(this.maxPos.size() < this.img.nbBands){
				this.maxPos.add(n.metricFeatures.size());
			}
			n.metricFeatures.add(Double.MIN_VALUE); // ADDING FICTIONNAL INITIAL MAXIMUM VALUE.
		
		}
		
	}

	/**
	 * Initiate or update the values of the Metric Features (MF) such as:</br>
	 * - min: minimum value of the pixels of the region among the channels (~ bands).</br>
	 * - max: maximum value of the pixels of the among the channels (~ bands).</br>
	 * @param n Concerned node.
	 */
	@Override
	public void updateMF(Node n) {
		
		int minPosb;
		int maxPosb;
		switch(n.type){
		
			case LEAF: /* GET THE MIN AND MAX FOR EACH CHANNEL (~ BAND). */
				double pixelValue;
				for(Integer point: n.getPoints()){
					for(int b = 0; b < this.img.nbBands; ++b){
						
						minPosb = this.minPos.get(b);
						pixelValue = this.img.getPixel(point % this.img.width, point / this.img.width, b);
						if(n.metricFeatures.get(minPosb) > pixelValue){
							
							n.metricFeatures.set(minPosb, pixelValue);

						}
						maxPosb = this.maxPos.get(b);
						if(n.metricFeatures.get(maxPosb) < pixelValue){
							
							n.metricFeatures.set(maxPosb, pixelValue);
							
						}
						
					}
				}
				break;
			default: /* GET THE MIN OF MIN AND THE MAX OF MAX OF THE VALUES BETWEEN THE TWO DIRECT SUB-REGIONS (CHILDREN) */
				for(int b = 0; b < this.img.nbBands; ++b){
					
					minPosb = this.minPos.get(b);
					n.metricFeatures.set(minPosb, Math.min(n.leftChild.metricFeatures.get(minPosb), n.rightChild.metricFeatures.get(minPosb)));

					maxPosb = this.maxPos.get(b);
					n.metricFeatures.set(maxPosb, Math.max(n.leftChild.metricFeatures.get(maxPosb), n.rightChild.metricFeatures.get(maxPosb)));
					
				}
		}
	}

	/**
	 * Compute a distance between 'n1' and 'n2' using the Metric Features (MF) such as:
	 * - min: minimum value of the pixels of the region among the channels (~ bands).
	 * - max: maximum value of the pixels of the among the channels (~ bands).
	 * @param n1 First Node.
	 * @param n2 Second Node.
	 * @return A score (~ distance) between 'n1' and 'n2'.
	 */
	@Override
	public double computeDistances(Node n1, Node n2) {
		
		double score = 0;
		double miniMini, maxiMaxi;

		for(int b = 0; b < this.img.nbBands; ++b){
			
			/**
			 * Sum the differences between the max of max and the min of min of each channel (~ band).
			 */
			miniMini = Math.min(n1.metricFeatures.get(this.minPos.get(b)), n2.metricFeatures.get(this.minPos.get(b)));
			maxiMaxi = Math.max(n1.metricFeatures.get(this.maxPos.get(b)), n2.metricFeatures.get(this.maxPos.get(b)));
			score += Math.abs(maxiMaxi - miniMini);
			
		}
		
		return score;
		
	}

}
