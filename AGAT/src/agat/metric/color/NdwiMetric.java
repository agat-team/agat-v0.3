package agat.metric.color;

import agat.metric.support.Metric;
import agat.metric.support.MetricInterface;
import agat.tree.structure.Node;
import agat.util.Log;
import agat.util.image.TiffImage;

/**
 * This class compute NDWI values of each region (~ node). </br>
 * The NDWI is a normalized water index for remote sensing.
 * @author T.J.F.R
 *
 */
public class NdwiMetric extends Metric implements MetricInterface{

	/**
	 * Position of the ndwi in the metric features (MF) list.
	 */
	int ndwiPos;
	
	int gindex;
	int nirindex;
	
	/**
	 * Register an image within the metric and create the ndwi metric based on the difference-sum ratio between the Near Infrared Red and the Green bands.
	 * @param tiffImg
	 */
	public NdwiMetric(TiffImage tiffImg) {
		
		this.type = TypeOfMetric.NDWI;
		this.img = tiffImg;
		
		/**
		 * Store the metric in the list of metrics in the image object.
		 */
		this.img.metrics.add(this.type);
		
		/*
		 * (!) For this prototype, the G and NIR bands are not accurates.
		 */
		this.gindex = 0;
		this.nirindex = 0;
		if(this.img.nbBands > 1){
			this.gindex = 1;
			this.nirindex = this.img.nbBands -1;
		}
		
		Log.printLvl1(String.valueOf(this.type), "Metric prepared!");
		
	}
	
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* 
* IMPLEMENTED METHODS FROM THE METRIC INTERFACE
* 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	/**
	 * Prepare all the Metric Features (MF) corresponding to the NDWI value of the specified region (~ node).</br>
	 * @param n Concerned node.
	 */
	@Override
	public void prepareMF(Node n) {

		this.ndwiPos = n.metricFeatures.size();
		n.metricFeatures.add(-1.0); // ADDING FICTIONNAL INITIAL NDVI VALUE.

	}

	/**
	 * Initiate or update the NDWI values in the Metric Features list (MF). </br>
	 * (!) For this prototype, let's assume that the G band index and the NIR band index are respectively the second and the last bands.
	 * @param n Concerned node.
	 */
	@Override
	public void updateMF(Node n) {

		double ndwi; /* NDWI = (G - NIR) / (G + NIR) */
		double g; // value on the Green band.
		double nir; // value on the NIR band.
		
		switch(n.type){

		/* Compute the NDVI value corresponding to the region (~ node). */
		case LEAF: 

			double meanG = 0.0; // mean of the Red pixels values of the region.
			double meanNIR = 0.0; // mean of the NIR pixels values of the region.
			for(Integer point: n.getPoints()){

				int x = point % this.img.width;
				int y = point / this.img.width;
				g = this.img.getPixel(x, y, this.gindex);
				nir = this.img.getPixel(x, y, this.nirindex);
				meanG += g;
				meanNIR += nir;
				
			}
			meanG /= n.size;
			meanNIR /= n.size;
			
			ndwi = (meanG - meanNIR) / (meanG + meanNIR);
			n.metricFeatures.set(this.ndwiPos, ndwi);

			break;
		default: // node case.
			ndwi = (n.leftChild.metricFeatures.get(this.ndwiPos) * n.leftChild.size + n.rightChild.metricFeatures.get(this.ndwiPos) * n.rightChild.size) / (n.leftChild.size +n.rightChild.size) ;
			n.metricFeatures.set(this.ndwiPos, ndwi);
		}
	}

	/**
	 * Compute a distance between 'n1' and 'n2' using the Metric Features (MF).
	 * @param n1 First Node.
	 * @param n2 Second Node.
	 * @return A score (~ distance) between 'n1' and 'n2'.
	 */
	@Override
	public double computeDistances(Node n1, Node n2) {

		return Math.abs(n1.metricFeatures.get(this.ndwiPos) - n2.metricFeatures.get(this.ndwiPos));

	}
	
}
