package agat.metric.shape;

import java.util.TreeSet;

import agat.metric.support.Metric;
import agat.metric.support.MetricInterface;
import agat.tree.structure.Node;
import agat.util.Log;
import agat.util.image.TiffImage;

/**
 * This class compute rapidly the compactness values of each region (~ node). </br>
 * @author T.J.F.R
 *
 */
public class FastCompactness extends Metric implements MetricInterface{

	/**
	 * Position of the compactness value in the metric features (MF) list.
	 */
	int compactPos;

	/**
	 * Register an image within the metric and create the compactness metric.
	 * @param tiffImg
	 */
	public FastCompactness(TiffImage tiffImg) {
		
		this.type = TypeOfMetric.FAST_COMPACTNESS;
		this.img = tiffImg;
		
		/**
		 * Store the metric in the list of metrics in the image object.
		 */
		this.img.metrics.add(this.type);
		
		Log.printLvl1(String.valueOf(this.type), "Metric prepared!");
		
	}
	
	/**
	 * Compute a fast compactness.
	 * @param size Size of the region in terms of pixel.
	 * @param borderLength Length of the border. 
	 * @return
	 */
	public static double computeCompactness(int size, int borderLength) {
		
		return borderLength / Math.sqrt(size);
		
	}
	
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* 
* IMPLEMENTED METHODS FROM THE METRIC INTERFACE
* 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	/**
	 * Prepare all the Metric Features (MF) corresponding to the compactness value of the specified region (~ node).</br>
	 * @param n Concerned node.
	 */
	@Override
	public void prepareMF(Node n) {

		this.compactPos = n.metricFeatures.size();
		n.metricFeatures.add(-1.0); // ADDING FICTIONNAL INITIAL NDVI VALUE.

	}

	/**
	 * Initiate or update the compactness values in the Metric Features list (MF). </br>
	 * @param n Concerned node.
	 */
	@Override
	public void updateMF(Node n) {
		
		/*
		 * Compute region compactness.
		 */
		double compactness = FastCompactness.computeCompactness(n.size, n.borderPoints.size());
		
		/*
		 * Set or Update the node metric feature (~ MF).
		 */
		n.metricFeatures.set(this.compactPos, compactness);
		
	}

	/**
	 * Compute a distance between 'n1' and 'n2' using the Metric Features (MF).
	 * @param n1 First Node.
	 * @param n2 Second Node.
	 * @return A score (~ distance) between 'n1' and 'n2'.
	 */
	@Override
	public double computeDistances(Node n1, Node n2) {

		double score = 0.0;

		double compactness1 = n1.metricFeatures.get(this.compactPos);
		double compactness2 = n2.metricFeatures.get(this.compactPos);
	
		double averageChildren = (compactness1 + compactness2)/2.0;//((compactness1 * n1.points.size()) + (compactness2 * n2.points.size())) / (n1.points.size() + n2.points.size());
			
		int sizeFakeFather = n1.size + n2.size;
		
		// Border points fake father.
		TreeSet<Integer> borderPoints = new TreeSet<Integer>();
		borderPoints.addAll(n1.borderPoints);
		borderPoints.addAll(n2.borderPoints);
		for(Integer p:n1.borderPoints){
			if(n2.borderPoints.contains(p)){
				borderPoints.remove(p);
			}
		}
		
		double compactnessPotentialFather = FastCompactness.computeCompactness(sizeFakeFather, borderPoints.size());
		
		score = Math.abs(compactnessPotentialFather - averageChildren);
		
		return score;
		
	}
	
}
