package agat.metric.shape;

import java.util.TreeSet;

import agat.metric.support.Metric;
import agat.metric.support.MetricInterface;
import agat.tree.structure.Node;
import agat.util.Log;
import agat.util.image.Morphological;
import agat.util.image.TiffImage;

/**
 * 
 * Metric based on the shape of the regions. Precisely, this metric focuses on the smoothness of the regions.
 * The computation of the distance between two regions is determined by their respective smoothness value.
 * (!) All metric classes must inherit from the 'Metric' class and implement the interface 'MetricInterface' and override all its methods.
 * 
 * @author C. Kurtz
 *
 */
public class Smoothness extends Metric implements MetricInterface{

	/**
	 * All attributes of the smoothness metric.
	 */
	int smoothPos = -1; // LOCATION OF THE SMOOTHNESS FEATURE IN EACH NODE.
	
	/**
	 * Register an image within the metric and create the metric object based on the elongation of a region (~ NODE).
	 * @param tiffImg
	 */
	public Smoothness(TiffImage tiffImg) {
		
		this.type = TypeOfMetric.SMOOTHNESS;
		this.img = tiffImg;
		
		/**
		 * Store the metric in the list of metrics in the image object.
		 */
		this.img.metrics.add(this.type);
		
		Log.printLvl1(String.valueOf(this.type), "Metric prepared!");
		
	}
	
	/**
	 * Return smoothness of a list of Points with Morpho.
	 * 
	 * @param listOfPoints List of points forming the region (~ node).
	 * @param imgWidth Width of the image.
	 * @param imgHeight Height of the image.
	 * @return Value between 0 and 1 associated with the smoothness of the region.
	 */
	public static double computeSmoothnessWithMorpho(TreeSet<Integer> listOfPoints,int imgWidth,int imgHeight) {	
	
		//------------Morpho
		Morphological morpho = new Morphological(imgWidth, imgHeight,5);
		double smoothness_morpho = morpho.morphologicalSmoothness(listOfPoints);
		return smoothness_morpho;
		
	}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* 
* IMPLEMENTED METHODS FROM THE METRIC INTERFACE
* 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	
	/**
	 * Prepare all the Metric Features (MF) corresponding to the elongation value of the specified region (~ node):</br>
	 * - elongation: value associated with the elongation shape of the region (~ node).</br>
	 * @param n Concerned node.
	 */
	@Override
	public void prepareMF(Node n) {

		/**
		 * - Define the smoothness value position in the list of MF.
		 * - Initialize the smoothness value with '1.0'.
		 */
		if(this.smoothPos < 0)
			this.smoothPos = n.metricFeatures.size();
		n.metricFeatures.add(1.0); // ADDING FICTIONNAL INITIAL SMOOTHNESS VALUE.
		
	}

	/**
	 * Initiate or update the values of the Metric Features (MF):</br>
	 *- smoothness: value associated with the smoothness of the region (~ node).</br>
	 * @param n Concerned node.
	 */
	@Override
	public void updateMF(Node n) {
		
		/*
		 * Compute region smoothness.
		 */
		double smoothness = Smoothness.computeSmoothnessWithMorpho(n.getPoints(), this.img.width, this.img.height);
		
		/*
		 * Set or Update the node metric feature (~ MF).
		 */
		n.metricFeatures.set(this.smoothPos, smoothness);
		
	}

	/**
	 * Compute a distance between 'n1' and 'n2' using the Metric Features (MF):
	 * - smoothness: value associated with the smoothness of the region (~ node).</br>
	 * @param n1 First Node.
	 * @param n2 Second Node.
	 * @return A score (~ distance) between 'n1' and 'n2'.
	 * (!) TODO - think again about it.
	 */
	@Override
	public double computeDistances(Node n1, Node n2) {
		
		double score = 0.0;

		double smoothness1 = n1.metricFeatures.get(this.smoothPos);
		double smoothness2 = n2.metricFeatures.get(this.smoothPos);
		
		score =  Math.abs(smoothness2 - smoothness1);
		
		return score;
		
	}
	
}
