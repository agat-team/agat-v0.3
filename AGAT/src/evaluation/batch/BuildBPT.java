package evaluation.batch;

import agat.metric.support.Metric.TypeOfMetric;
import agat.tree.MBPT;
import agat.util.Log;
import agat.util.image.TiffImage;

/**
 * Build a standard bpt from an image.
 * @author T.J.F.R
 *
 */
public class BuildBPT {

	public static void main(String[] args) {

		/**
		 * Show lvl1 messages.
		 */
		Log.showLvl1 = true;
		
		/**
		 * Create an empty tree and save it in a file.
		 */
		String path = "experiments//icip-2017//examples";
		String bptPath = path + "//img.bpt"; 
		MBPT tree = new MBPT(bptPath, true);
		
		/**
		 * - Register an image by linking it with the tree.
		 * - Associate a metric with the image.
		 */
		String imgPath = path + "//img.tif"; 
		TiffImage tiffImg = new TiffImage(imgPath);
		tree.registerImage(tiffImg);
		tree.linkMetricToAnImage(tiffImg, TypeOfMetric.RADIOMETRIC_MIN_MAX);
		
		/**
		 * Start to build a standard BPT.
		 */
		tree.grow();

	}

}
