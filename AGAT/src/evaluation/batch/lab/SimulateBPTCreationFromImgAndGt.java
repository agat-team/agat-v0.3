package evaluation.batch.lab;

import agat.metric.support.Metric.TypeOfMetric;
import agat.tree.MBPT;
import agat.tree.structure.Node;
import agat.util.Log;
import agat.util.image.TiffImage;
import evaluation.DistanceMap;
import evaluation.HierarchyEval;
import evaluation.HierarchyEval.EvalMetric;
import evaluation.HierarchyEval.EvalType;
import evaluation.SegReference.BgColor;
import evaluation.SegReference;

/**
 * 
 * Create 2 Bpts 
 * i)  from an image 'img1.tif' 
 * ii) from and image 'gt1.tif' 
 * 
 * @author T.J.F.R.
 *
 */
public class SimulateBPTCreationFromImgAndGt {
	
	public static void main(String[] args) {
		
		/**
		 * Show lvl1 messages.
		 */
		Log.showLvl1 = true;
		
		/** For the image
		 * Create an empty tree and save it in a file.
		 */
		String path = "experiments//icip-2017//tests";
		String bptPath = path + "//img1.bpt"; 
		MBPT tree = new MBPT(bptPath, true);
		
		/**
		 * - Register an image by linking it with the tree.
		 * - Associate a metric with the image.
		 */
		String imgPath = path + "//img1.tif"; 
		TiffImage tiffImg = new TiffImage(imgPath);
		tree.registerImage(tiffImg);
		tree.linkMetricToAnImage(tiffImg, TypeOfMetric.RADIOMETRIC_MIN_MAX);
		
		/**
		 * Start to build a standard BPT.
		 */
		tree.grow();
		
		/** For the gt
		 * Create an empty tree and save it in a file.
		 */
		String bptgtPath = path + "//gt1.bpt"; 
		MBPT gttree = new MBPT(bptgtPath, true);
		Node.nodeCounter = 0;
		
		/**
		 * - Register an image by linking it with the tree.
		 * - Associate a metric with the image.
		 */
		String gtPath = path + "//gt1.tif"; 
		TiffImage tiffGt = new TiffImage(gtPath);
		gttree.registerImage(tiffGt);
		gttree.linkMetricToAnImage(tiffGt, TypeOfMetric.RADIOMETRIC_MIN_MAX);
		
		/**
		 * Start to build a standard BPT from the gt.
		 * (!) just for having a node for the test.
		 */
		gttree.grow();
		
		/**
		 * Show 'img1.bpt' sizes of the first children of the root.
		 * Show 'gt1.bpt' sizes of the first children of the root.
		 */
		Log.printLvl1(bptPath, "leftNode size: "+ tree.root.leftChild.size +"px, rightNode size: "+ tree.root.rightChild.size +"px");
		Log.printLvl1(bptgtPath, "leftNode size: "+ gttree.root.leftChild.size +"px, rightNode size: "+ gttree.root.rightChild.size +"px\n");
		
		//==> according to the experiment, the leftnodes are the segments
		
		/**
		 * Compare the node of the bpt form img1 and the segment of reference of the GT.
		 */
		Node n = tree.root.leftChild;
		int alpha = 0;
		SegReference s = new SegReference(0, gttree.root.leftChild.getPoints(), 0, BgColor.WHITE) ;
		DistanceMap.compute(s, alpha, true);
		
		/* 1st comparison */
		double jaccardSimilarityScore = HierarchyEval.comparison(n, s, EvalType.HARD, EvalMetric.JACCARD); // ^(n,s)
		Log.printLvl1("hard J'(n,s_"+ n.index +")", ""+ jaccardSimilarityScore);
		
		/* 2nd comparison */
		Log.printLvl1("hard J'(n,s_"+ tree.root.rightChild.index +")", ""+ HierarchyEval.comparison(tree.root.rightChild, s, EvalType.HARD, EvalMetric.JACCARD) +"\n");
		
//		double diceSimilarityScore = HierarchyEval.Comparison(n, s, EvalType.HARD, EvalMetric.DICE); // ^(n,s)
//		Log.printLvl1("hard D(n,s)", ""+ diceSimilarityScore);
		
		System.out.println("best matching node with s: "+ s.bestN.index +"  similarityScore: "+ s.bestSimilarityScore);
		
	}

}
