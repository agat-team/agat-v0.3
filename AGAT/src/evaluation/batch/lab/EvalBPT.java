package evaluation.batch.lab;

import java.io.FileReader;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import com.sun.prism.paint.Color;

import agat.metric.support.Metric.TypeOfMetric;
import agat.tree.MBPT;
import agat.tree.structure.Node;
import agat.util.Log;
import agat.util.image.TiffImage;
import evaluation.HierarchyEval;
import evaluation.HierarchyEval.EvalMetric;
import evaluation.HierarchyEval.EvalType;
import evaluation.HierarchyEval.WeightFormula;
import evaluation.SegReference;
import evaluation.SegReference.BgColor;
import pelican.DoubleImage;
import pelican.Image;
import pelican.IntegerImage;
import pelican.util.visualisation.Viewer2D;

/**
 * 
 * Example of BPT evaluation with simple example.
 * 
 * @author T.J.F.R.
 *
 */
public class EvalBPT {

	public static void main(String[] args) {

		/* alpha param */
		double alpha = 1.2;
		
		/* pixel margin around the real bounding box */
		int margin = 20; /* % */
		
		/* size range */
		int pRange = 50; /* % */
//		int sRange = 0; 
		
		/*
		 * Show lvl1 messages.
		 */
		Log.showLvl1 = false;
//		Log.showLvl2 = true;
		
		/* paths */
//		String path = "experiments//icip-2017//examples";
//		String imgPath = path +"//imgBruit.tif";
//		String bptPath = path +"//imgBruit.bpt";
//		String imgGtPath = path +"//gt.tif";

		/* terrible case */
//		String path = "experiments//icip-2017//examples";
////		String imgPath = path +"//case1.tif";
////		String bptPath = path +"//case1.bpt";
//		String imgGtPath = path +"//case1.tif";
		
//		String imgPath = path +"//img.tif";
//		String bptPath = path +"//img.bpt";
//		String imgGtPath = path +"//gt.tif";
		
//		String path = "experiments//icip-2017//stbg";
//		String imgPath = path +"//stbg-1000.tif";
////		String bptPath = path +"//stbg-1000.bpt";
////		String bptPath = path +"//stbg-1000-NDVI.bpt";
//		String bptPath = path +"//stbg-1000-NDWI.bpt";
//		String imgGtPath = path +"//gt-1000.tif";
		
		String path = "experiments//icip-2017//stbg";
		String imgPath = path +"//stbg2-1000.tif";
//		String bptPath = path +"//stbg2-1000.bpt";
		String bptPath = path +"//stbg2-1000-NDVI.bpt";
//		String bptPath = path +"//stbg2-1000-NDWI.bpt";
		String imgGtPath = path +"//gt2-1000.tif";
		
//		String imgPath = path +"//stbg-200.tif";
//		String bptPath = path +"//stbg-200.bpt";
////		String bptPath = path +"//stbg-200-NDVI.bpt";
////		String bptPath = path +"//stbg-200-NDWI.bpt";
//		String imgGtPath = path +"//gt-200.tif";
				
//		String imgPath = path +"//stbg-100.tif";
//		String bptPath = path +"//stbg-100.bpt";
//		String imgGtPath = path +"//gt-100.tif";
		
//		String path = "experiments//icip-2017//stbg";
//		String imgPath = path +"//stbg3-1000.tif";
//		String bptPath = path +"//stbg3-1000.bpt";
////		String bptPath = path +"//stbg-2000-NDVI.bpt";
////		String bptPath = path +"//stbg-2000-NDWI.bpt";
//		String imgGtPath = path +"//gt3-1000.tif";
		
		/* Compute a bpt from the image */
		MBPT tree = new MBPT(bptPath, true);
		TiffImage tiffImg = null;
		
		try {
			new FileReader(tree.mBptFileNamePath);
			tree.reGrow();			
			tiffImg = tree.listOfImages.get(0);
		}catch(Exception e){
			 tiffImg= new TiffImage(imgPath);
				tree.registerImage(tiffImg);
//				tree.linkMetricToAnImage(tiffImg, TypeOfMetric.RADIOMETRIC_MIN_MAX);
				tree.linkMetricToAnImage(tiffImg, TypeOfMetric.NDVI);
//				tree.linkMetricToAnImage(tiffImg, TypeOfMetric.NDWI);
			 tree.grow();
		}
		
		/* gather all segments in one list */
		TreeMap<Integer, SegReference> listOfSegments = SegReference.getSegReferences(imgGtPath, BgColor.BLACK, alpha, margin, true, false);
		System.out.println("List of segments of reference's size: "+ listOfSegments.size());
				
		/* prepare time counting */
		ThreadMXBean searchingTimeThread = ManagementFactory.getThreadMXBean();
		long startingTime = searchingTimeThread.getCurrentThreadCpuTime();
		
		/* compare each segment to each node of the bpt */
		listOfSegments.entrySet()
		.parallelStream()
		.forEach(entry->{
			
			System.out.println("S-"+ entry.getKey());

			SegReference s = entry.getValue();
			int segSize = s.getGtPoints().size();
			
			int sRange = segSize * pRange / 100;
			
			int maxSizeThreshold = segSize + sRange;
			int minSizeThreshold = segSize - sRange;
			
			/* brute force comparison */
//			for(int ns = 0; ns < tree.rag.listOfNodes.length; ++ns){
//				
//				Node n = tree.rag.listOfNodes[ns];
//				HierarchyEval.comparison(n, s, EvalType.SOFT, EvalMetric.DICE);
//				
//			}}
			
			/* list of nodes to compare */
			ArrayList<Node> nodeList = new ArrayList<Node>();

			/* section 2.2 of the icip 2017 paper */
			TreeSet<Integer> sPoints = s.getGtPoints();
			for(int ls = 0; ls < tree.rag.nbLeaves; ++ls){

				Node leaf = tree.rag.listOfNodes[ls];
				TreeSet<Integer> leavesPoints = leaf.getPoints();
				
				/* find the leaves intersecting the segment 's' */
				for(Integer sPoint : sPoints){

					/* work on the leaves containing at least one pixel of the segment 's' */
					if(leavesPoints.contains(sPoint)){

						/* treat the leaf and its ancestors */
//						nodeList.add(leaf);
						boolean progress = true;
						Node currentN = leaf.father;
//						Node currentN = leaf;
						while(progress){
							if(nodeList.contains(currentN)) progress = false;
							if(currentN.type == Node.TypeOfNode.ROOT) progress = false;
							else if(currentN.size > maxSizeThreshold) progress = false;
							else{
								nodeList.add(currentN);
								if(currentN.size >= minSizeThreshold)
								HierarchyEval.comparison(currentN, s, EvalType.SOFT, EvalMetric.DICE);
//								HierarchyEval.comparison(currentN, s, EvalType.SOFT, EvalMetric.JACCARD);
								currentN = currentN.father;
							}
						}

						break;

					}

				}

			}
			
		}
		);
		
		/* searching time */
		long endingTime = searchingTimeThread.getCurrentThreadCpuTime();
		System.out.println("research time: "+ (endingTime - startingTime)/1000000 +"ms"); // TIME OF FUSION IN MS.
				
		/* Visualize the matched nodes */
		IntegerImage matchedNode = new IntegerImage(tiffImg.width, tiffImg.height, 1, 1, 1);
		IntegerImage segRefVisu = new IntegerImage(tiffImg.width, tiffImg.height, 1, 1, 1);
		IntegerImage overLap = new IntegerImage(tiffImg.width, tiffImg.height, 1, 1, 3);
		overLap.setColor(true);

		for(int y = 0; y < tiffImg.height; ++y){
			for(int x = 0; x < tiffImg.width; ++x){
				
				/* fill the background with black */
				matchedNode.setPixelXYByte(x, y, 0);
				segRefVisu.setPixelXYByte(x, y, 0);
				
				/* fill the background with white for the overLapping map */
				overLap.setPixelXYBByte(x, y, 0, 255); // R
				overLap.setPixelXYBByte(x, y, 1, 255); // G
				overLap.setPixelXYBByte(x, y, 2, 255); // B
								
			}
		}
		int nbMatchedNodes = 0;
		for(Map.Entry<Integer, SegReference> entry: listOfSegments.entrySet()){
			SegReference s = entry.getValue();
			
			/* paint the pixels of the segments of reference for the overlapping map */
			s.getGtPoints()
			.stream()
			.parallel()
			.forEach(p->{
				int xs = p % s.imgGtWidth;
				int ys = p / s.imgGtWidth;
				overLap.setPixelXYBByte(xs, ys, 0, 0); // R
				overLap.setPixelXYBByte(xs, ys, 1, 162); // G
				overLap.setPixelXYBByte(xs, ys, 2, 232); // B
				segRefVisu.setPixelXYByte(xs, ys, s.semanticLabel);
			});
			
			Node n = s.bestN;
			if(n != null){
				for(Integer npoints : n.getPoints()){
					int xn = npoints % tiffImg.width;
					int yn = npoints / tiffImg.width;
					matchedNode.setPixelXYByte(xn, yn, s.semanticLabel); /* paint the nodes with their corresponding semantic label */
					
					if(overLap.getPixelXYBByte(xn, yn, 0) != 255){
						/* paint the overlapping pixels */
						overLap.setPixelXYBByte(xn, yn, 0, 80); // R
						overLap.setPixelXYBByte(xn, yn, 1, 260); // G
						overLap.setPixelXYBByte(xn, yn, 2, 40); // B
					}
					else{
						/* paint the nodes */
						overLap.setPixelXYBByte(xn, yn, 0, 255); // R
						overLap.setPixelXYBByte(xn, yn, 1, 74); // G
						overLap.setPixelXYBByte(xn, yn, 2, 74); // B

					}
				}
				nbMatchedNodes++;
			}
		}
		int nbSegs = listOfSegments.size();
		System.out.println("Number of matched nodes : "+ nbMatchedNodes +" for "+ nbSegs +" segments of reference.");
		Viewer2D.exec(matchedNode, "Matched Node");
		Viewer2D.exec(overLap, "Overlap");
		Viewer2D.exec(segRefVisu, "Segment of Ref");
				
		/* compute global score */
		double bptScoref1 = HierarchyEval.computeGlobalScore(listOfSegments, WeightFormula.NB_OF_SEGMENTS); // F1			
		double bptScoref2 = HierarchyEval.computeGlobalScore(listOfSegments, WeightFormula.SIZE_OF_SEGMENTS); // F2
		System.out.println("Nb semantic class: "+ listOfSegments.firstEntry().getValue().ListOfAllSemanticLabels.size());
		for(Map.Entry<Integer, Double[]> entry : listOfSegments.firstEntry().getValue().ListOfAllSemanticLabels.entrySet()){
			int classLabel = entry.getKey();
			double nbSegsInClass = entry.getValue()[0];
			double totalSizeOfClass = entry.getValue()[1];
			double classScoreF1 = entry.getValue()[2];
			double classScoreF2 = entry.getValue()[3];
			System.out.println("Class-"+ classLabel + " | nb-segments: "+ nbSegsInClass +" | total-size: "+ totalSizeOfClass +" | class-score-f1: "+ classScoreF1 +" | class-score-f2: "+ classScoreF2);
		}
		System.out.println("BPT score f1 (alpha = "+ alpha +"): " + bptScoref1);
		System.out.println("BPT score f2 (alpha = "+ alpha +"): " + bptScoref2);
		
	}

}
