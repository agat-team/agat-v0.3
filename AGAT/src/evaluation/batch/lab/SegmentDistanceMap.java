package evaluation.batch.lab;

import agat.util.Log;
import evaluation.DistanceMap;
import evaluation.SegReference;
import evaluation.SegReference.BgColor;

/**
 * 
 * Test the computation the distance map for a segment. 
 * 
 * @author T.J.F.R.
 *
 */
public class SegmentDistanceMap {
	
	public static void main(String[] args) {
		
		/**
		 * Show lvl1 messages.
		 */
		Log.showLvl1 = true;
		
		
		/*
		 * Prepare the segment of reference.
		 */
//		String path = "experiments//icip-2017//tests";
//		String imgPath = path +"//gt5.tif";
		
		String path = "experiments//icip-2017//examples";
		String imgPath = path +"//gt.tif";
		SegReference s = new SegReference(0, imgPath, 0, BgColor.BLACK, true) ;
		
		/*
		 * Create a distance map from this segment of reference.
		 */
		s.margin = 20;
		DistanceMap.compute(s, 1.0, true); // if true visualize
				
	}

}
