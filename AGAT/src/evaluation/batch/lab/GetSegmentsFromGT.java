package evaluation.batch.lab;

import java.util.Map;
import java.util.TreeMap;
import agat.util.Log;
import evaluation.SegReference;

/**
 * 
 * Example getting the list of segments from a GT image having a white background.
 * 
 * @author T.J.F.R.
 *
 */
public class GetSegmentsFromGT {

	public static void main(String[] args) {

		/*
		 * Show lvl1 messages.
		 */
		Log.showLvl1 = true;
		
		/* gt image path */
//		String path = "experiments//icip-2017//tests";
//		String imgPath = path +"//gt7.tif";
		
		/* gt image path */
		String path = "experiments//icip-2017//examples";
		String imgPath = path +"//gt.tif";
		
		/* pixel margin around the real bounding box */
		int margin = 20;
		
		/* alpha for the distance tranfrosm */
		double alpha = 1.0;
		
		/* gather all segments in one list */
		TreeMap<Integer, SegReference> listOfSegments = SegReference.getSegReferences(imgPath, evaluation.SegReference.BgColor.BLACK, alpha, margin, true, true);
		System.out.println("List of segments of reference's size: "+ listOfSegments.size());
		
		/* Compute distance map for each segment */
		for(Map.Entry<Integer, SegReference> entry : listOfSegments.entrySet()){
			
			Integer seglabel = entry.getKey();
			SegReference s = entry.getValue();
			Integer semlabel = s.semanticLabel;
			
			Log.printLvl1(""+ seglabel +" _ "+ semlabel, ""+ s.gtPoints.size());
						
		}
		
	}

}
