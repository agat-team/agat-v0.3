package evaluation;

import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import agat.tree.structure.Node;
import agat.util.Log;
import agat.util.image.TiffImage;

/**
 * 
 * Hierarchy Evaluation.
 * 
 * @author T.J.F.R
 *
 */
public class HierarchyEval {
	
	public enum EvalType{
		HARD,
		SOFT
	}
	
	public enum EvalMetric{
		JACCARD,
		DICE
	}
	
	public enum WeightFormula{
		
		SIZE_OF_SEGMENTS,
		NB_OF_SEGMENTS
		
	}
	
	/**
	 * Preparing a supervised hierarchy evaluation.
	 * @param bptFile
	 * @param gtImage
	 * @param evalType (HARD or SOFT)
	 * @param evalMetric (eg : JACCARD, DICE)
	 */
	public HierarchyEval(String bptFile, TiffImage gtImage, EvalType evalType, EvalMetric evalMetric){
		
		
		
	}
	
	/**
	 * 
	 * Compare a node 'n' with a segment of reference 's' in a hard or soft way.
	 * 
	 * @param n : node of the BTP
	 * @param s : segment of the GT
	 * @param evalType (HARD or SOFT)
	 * @param evalMetric (eg : JACCARD, DICE)
	 * @return a similarity score ^(n,s) between the node and the segment of reference.
	 */
	public static double comparison(Node n, SegReference s, EvalType evalType, EvalMetric evalMetric){
		
		double localSimilarityScore = 0.0;
		
		/*
		 * Regroup the pixels ~ points from 'n' and 's'.
		 */
		TreeSet<Integer> nPoints = n.getPoints();
		TreeSet<Integer> sPoints = s.getGtPoints();
		
		/*
		 * Compute True Positives (TP), False Positives (FP) and False Negatives (FN).
		 */
		double tp = 0; // number of true positives.
		double fp = nPoints.size(); // number of false positives.
		double fn = 0; // number of false negatives.
		switch(evalType){
		
			case HARD:
				
				for(Integer spixel : sPoints){
					
					if(nPoints.contains(spixel)){
						tp++;
						fp--;
					}
					else fn++;
					
				}
				
				break;
			case SOFT:
				
				/* mapping decal between the GT image and the crop from the bounding box */
				int xdecal = s.boundingBox[0];
				int ydecal = s.boundingBox[2];
				
				/* compute the membership values 'mu' of the points in the segmetns 's' */
//				double[][][] muMap = DistanceMap.compute(s, alpha, false); 
				double[][][] muMap = s.muMap; // already computed and only once.
				
				/* explore the matrix containing 'delta S' super-set of 'S' */
				for(int ybb = 0; ybb < s.bbHeight; ++ybb){
					for(int xbb = 0; xbb < s.bbWidth; ++xbb){
						
						int xgt = xbb + xdecal;
						int ygt = ybb + ydecal;
						int gtpoint = (ygt * s.imgGtWidth) + xgt;
						
						double mux = muMap[xbb][ybb][1];

						if(nPoints.contains(gtpoint)){ /* compute TP */
						
							tp += mux;
							
						}else{ /* compute FN */
							
							fn += mux;
							
						}
						
					}
				}
				
				/* compute FP */
				fp = nPoints.size() - tp;
								
				break;
				
		}
		
		/*
		 * Compute the similarity score.
		 */
		switch(evalMetric){
		
			case JACCARD: /* Jaccard index */
				localSimilarityScore = tp / (tp + fp + fn);
								
				break;
			case DICE: /* Dice coefficient */
				
				localSimilarityScore = 2*tp / (2*tp + fp + fn);
				
				break;
		
		}
		
		/* retain the best matching node and its similarity score */
		if(Double.compare(s.bestSimilarityScore, localSimilarityScore) < 0) s.setBestMatchedNode(n, localSimilarityScore);
		
		/*
		 * Just show tp, fp and fn and the best score values.
		 */
		Log.printLvl1("compare N-"+ n.index +" and S-"+ s.index, "TP: "+ tp +" FP: "+ fp +" FN: "+ fn +" | lambda(S-"+ s.index +"): "+ s.bestSimilarityScore +" | localScore: "+ localSimilarityScore);

		return localSimilarityScore;
		
	}

	/**
	 * Computhe the global score of the hierarchy structure.
	 * @param listOfSegments : list of segments of reference.
	 * @param wform : formulat applied for the weight wi and wl.
	 * @return A score assessing the ability of the hierarchy to allow acceptable segmentation.
	 */
	public static double computeGlobalScore(TreeMap<Integer, SegReference> listOfSegments, WeightFormula wform) {

		/* set the first segment which contains informations about all semantic classes */
		SegReference ref = listOfSegments.firstEntry().getValue();

		/* number of semantic class */
		double nbCl = ref.ListOfAllSemanticLabels.size();
		
		/* variable used for double sums */
		double bptScore = 0.0;
				
		/* get total size of segments for each semantic class and also the total size of all segments*/
		double segsTotalSize = 0.0;
		
		if(!ref.sizeAndNbSet){
			for(Map.Entry<Integer, SegReference> en : listOfSegments.entrySet()){


				SegReference se = en.getValue();
				Integer semlab = se.semanticLabel;

				/* get size of all segments of the specified semantic class */
				int ssize = se.getGtPoints().size();
				double size = ref.ListOfAllSemanticLabels.get(semlab)[1] + ssize;
				ref.ListOfAllSemanticLabels.get(semlab)[1] = size;

				/* get number of segments in the semantic class */
				double nbSegOfClass = ref.ListOfAllSemanticLabels.get(semlab)[0] + 1;
				ref.ListOfAllSemanticLabels.get(semlab)[0] = nbSegOfClass;

			}
			ref.sizeAndNbSet = true;
		}
		
		Log.printLvl2("BPT global score", "segsTotalSize: "+ segsTotalSize);

		/* formula */
		double sumWl = 0.0;
		for(Map.Entry<Integer, Double[]> ent : ref.ListOfAllSemanticLabels.entrySet()){ // all semantic labels

			int semlabel = ent.getKey();
			
			double sumWi = 0.0;
			double sum = 0.0;
			for(Map.Entry<Integer, SegReference> entry : listOfSegments.entrySet()){ //  all segments

				SegReference s = entry.getValue();

				double n, d;
				
				switch(wform){
				case NB_OF_SEGMENTS:
					n = 1.0;
					d = (double) ent.getValue()[0]; // number of segments of this semantic class.
					break;
				case SIZE_OF_SEGMENTS:
					n = (double) s.getGtPoints().size(); // size of the current segment.
					d = (double) ent.getValue()[1]; // total size of all segments of this semantic class.
					break;
				default :
					n = 1.0;
					d = (double) ent.getValue()[0]; // case 1
				}

				if(s.semanticLabel == semlabel){
					
					double wival = n / d;
					sumWi += wival;
					double val = wival * s.bestSimilarityScore;
					sum += val;
					System.out.println("Lambda(S-"+ entry.getKey() +"): "+ s.bestSimilarityScore +" | Class-"+ s.semanticLabel);
					
					/* save the score for each semantic class */
					switch(wform){
					case NB_OF_SEGMENTS:
						ref.ListOfAllSemanticLabels.get(semlabel)[2] += val;
						break;
					case SIZE_OF_SEGMENTS:
						ref.ListOfAllSemanticLabels.get(semlabel)[3] += val;
						break;
					default:
						ref.ListOfAllSemanticLabels.get(semlabel)[2] += val;
					}
			
				}

			}
			Log.printLvl2("BPT w_i", "sum wi: "+ sumWi);
					
			sumWl += (1.0/(double)nbCl);
			bptScore += (1.0/(double)nbCl) * sum;
				
		}

		
		Log.printLvl2("BPT global score", "sum wl: "+ sumWl); 

		return bptScore;
		
	}

}
