package evaluation;

import fr.unistra.pelican.Image;
import fr.unistra.pelican.IntegerImage;
import fr.unistra.pelican.algorithms.io.ImageLoader;
import fr.unistra.pelican.algorithms.visualisation.Viewer2D;

/**
 * 
 * Distance map for segment of references.
 * 
 * @author T.J.F.R.
 *
 */
public class DistanceMap {
	
	/**
	 * Computing a distance map.
	 * @param path : path of the binary image used as input for a discrete distance transform. The image is a crop containing only a segment.
	 * @return an IntegerImage containing the distance map.
	 */
	public static IntegerImage compute(String path){
		
		/* load image input */
		Image input = ImageLoader.exec(path);
		
		/* input image width and height. */
		int w = input.xdim;
		int h = input.ydim;
		
		/*
		 * Init the output containing a distance transform.
		 */
		int black = 0;
		int white = 255;
		IntegerImage inOutMap = new IntegerImage(input, false);
		IntegerImage outMap = new IntegerImage(input, false);
		
		for (int i = 0; i < input.size(); ++i){
			if (!input.getPixelBoolean(i)){
				
				/* Init for the intern distance computation */
				inOutMap.setPixelByte(i, white);
				
				/* Init for the extern distance computation */
				outMap.setPixelByte(i, black);
				
			}
			else{
				/* Init for the intern distance computation */
				inOutMap.setPixelByte(i, black);				
				
				/* Init for the extern distance computation */
				outMap.setPixelByte(i, white);
			}
		}
		
		/*
		 * Compute the distance map.
		 */
		for(int y = 1; y <h-1; ++y){
			for(int x = 1; x < w-1; ++x){

				if(inOutMap.getPixelXYByte(x, y)!=black){
					for(int y1 = 0; y1 < h; ++y1){
						for(int x1 = 0; x1 < w; ++x1){

							/* Intern distance computation */
							if(inOutMap.getPixelXYByte(x1, y1)==black){

								/* Discrete distance for the inside */
								int d = Math.abs(x-x1) + Math.abs(y-y1);

								if(d < inOutMap.getPixelXYByte(x, y)){
									inOutMap.setPixelXYByte(x, y, d);
								}

							}

						}
					}
				}
				
			}
		}
		
		for(int y = 1; y <h-1; ++y){
			for(int x = 1; x < w-1; ++x){

				if(outMap.getPixelXYByte(x, y)!=black){
					for(int y1 = 0; y1 < h; ++y1){
						for(int x1 = 0; x1 < w; ++x1){

							/* Extern distance computation */
							if(outMap.getPixelXYByte(x1, y1)==black){
								
								/* Discrete distance for the outside */
								int d = Math.abs(x-x1) + Math.abs(y-y1);

								if(d < outMap.getPixelXYByte(x, y)){
									outMap.setPixelXYByte(x, y, d);
									inOutMap.setPixelXYByte(x, y, d);
								}
								
							}

						}
					}
				}	
				
			}
		}
		
		/* view result in windows */
		Viewer2D.exec(ImageLoader.exec(path));
		Viewer2D.exec(inOutMap, "In and out distance map");
		Viewer2D.exec(outMap, "Outside distance map");
		 
		return inOutMap;
		
	}
	
	/**
	 * Generating a crop containing only a segment 's' and then compute from it a distance map.
	 * @param s : the segments of interest.
	 * @param alpha : >0 allow controlling the degree of uncertainty.
	 * @param visu : if true, show the result in windows.
	 * @return : a matrix containing the real valor of 'sigma_s(x)' i.e. < 0 for x in the segment, 0 on the border of the segment and > 0 outside the segment.
	 * The result contains also the value of 'Mu_alpha'.
	 */
	public static double[][][] compute(SegReference s, double alpha, boolean visu){
		
		/*
		 * Compute the bounding box.
		 */
		s.computeRegionBoundingBox();
		
		/* Bounding box width and height */
		int w = s.bbWidth;
		int h = s.bbHeight;
		
		/* Prepare the resulting matrix */
		double[][][] distanceMap = new double[s.bbWidth][s.bbHeight][2];
		
		/*
		 * Init the output containing a distance transform.
		 */
		IntegerImage inOutMap = new IntegerImage(w, h, 1, 1, 1);
		IntegerImage outMap = new IntegerImage(w, h, 1, 1, 1);
		
		/*
		 * Prepare some supports.
		 */
		int black = 0;
		int white = 255;
		for (int j = 0; j < h; ++j){
			for(int i = 0; i < w; ++i){
			
				int tmpPoint = (j * w) + i;
				if(s.getBbPoints().contains(tmpPoint)) { /* foreground */
					inOutMap.setPixelXYByte(i, j, white);
					outMap.setPixelXYByte(i, j, black);
				}
				else{
					inOutMap.setPixelXYByte(i, j, black); /* background */
					outMap.setPixelXYByte(i, j, white);
				}
				
			}
		}
		
		/*
		 * Compute the intra-distance map.
		 */
		for(int y = 1; y <h-1; ++y){
			for(int x = 1; x < w-1; ++x){

				if(inOutMap.getPixelXYByte(x, y)!=black){
					for(int y1 = 0; y1 < h; ++y1){
						for(int x1 = 0; x1 < w; ++x1){

							/* Intern distance computation */
							if(inOutMap.getPixelXYByte(x1, y1)==black){

								/* Discrete distance for the inside */ 
								int d = (Math.abs(x-x1) + Math.abs(y-y1));
//								double d = Math.sqrt((x-x1)*(x-x1) + (y-y1)*(y-y1));

								if(d < inOutMap.getPixelXYByte(x, y)){
									
									inOutMap.setPixelXYByte(x, y, d);
									
									/* the 'sigma_s(x)' is negative when 'x' is inside the segment */
									distanceMap[x][y][0] = d * (-1);
									
									/* compute the membership function 'Mu_alpha' */
									distanceMap[x][y][1] = 1 / (1 + Math.exp(alpha * distanceMap[x][y][0]));
									
								}

							}

						}
					}
				}
				
			}
		}
		
		/*
		 * Compute the extra-distance map.
		 */
		for(int y = 1; y <h-1; ++y){
			for(int x = 1; x < w-1; ++x){

				if(outMap.getPixelXYByte(x, y)!=black){
					for(int y1 = 0; y1 < h; ++y1){
						for(int x1 = 0; x1 < w; ++x1){

							/* Extern distance computation */
							if(outMap.getPixelXYByte(x1, y1)==black){
								
								/* Discrete distance for the outside */
								int d = Math.abs(x-x1) + Math.abs(y-y1);
//								double d = Math.sqrt((x-x1)*(x-x1) + (y-y1)*(y-y1));

								if(d < outMap.getPixelXYByte(x, y)){
									outMap.setPixelXYByte(x, y, d);
									inOutMap.setPixelXYByte(x, y, d);
									
									/* store the value of 'sigma_s(x)' when 'x' is outside the segment 's'. */
									distanceMap[x][y][0] = d;
									
									/* compute the membership function 'Mu_alpha' */
									distanceMap[x][y][1] = 1 / (1 + Math.exp(alpha * distanceMap[x][y][0]));
									
								}
								
							}

						}
					}
				}	
				
			}
		}

		
		/* view result in windows */
		if(visu){
			
			/* generate the supposed input image */
			IntegerImage crop = new IntegerImage(w, h, 1, 1, 1);
			for (int j = 0; j < h; ++j){
				for(int i = 0; i < w; ++i){
				
					int tmpPoint = (j * w) + i;
					if(s.getBbPoints().contains(tmpPoint)) {
						crop.setPixelXYByte(i, j, white);
					}
					else{
						crop.setPixelXYByte(i, j, black);
					}
					
				}
			}
			
			/* generate an image for the membership values */
			IntegerImage muImg = new IntegerImage(w, h, 1, 1, 1);
			for (int j = 0; j < h; ++j){
				for(int i = 0; i < w; ++i){
				
					muImg.setPixelXYByte(i, j, (int) (distanceMap[i][j][1] * 255));
					
				}
			}
			
			Viewer2D.exec(crop, "Crop for the seg-"+ s.semanticLabel);
			Viewer2D.exec(inOutMap, "In and out distance map");
			Viewer2D.exec(outMap, "Outside distance map");
			Viewer2D.exec(muImg, "Mu map");
		
			/* print information : real value | byte value | mu value | byte mu value */
			for(int j=0; j<distanceMap[0].length; ++j){
				for(int i=0; i<distanceMap.length; ++i){
					
					System.out.println(distanceMap[i][j][0] +" | "+ inOutMap.getPixelXYByte(i, j) +" | "+ distanceMap[i][j][1] +" | "+ distanceMap[i][j][1] * 255);
					
				}
			}
			
		}
				 
		return distanceMap;
		
	}
	public static void main(String args[]) {
		
		String path="experiments//icip-2017//tests//gt6.png";
		DistanceMap.compute(path);
	
	}

}
