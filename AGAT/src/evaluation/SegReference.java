package evaluation;

import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import agat.tree.structure.Node;
import agat.util.Log;
import agat.util.image.TiffImage;
import agat.util.image.sbc.SegmentByConnexityRaw;
import fr.unistra.pelican.Image;
import fr.unistra.pelican.algorithms.io.ImageLoader;

/**
 * 
 * Segment of reference from a ground truth GT.
 * 
 * @author T.J.F.R.
 *
 */
public class SegReference{
	
	/**
	 *  Best matching node.
	 */
	public Node bestN = null;
	
	/**
	 * Best score of similarity associated with the best matched node 'lamda(S_i)'.
	 */
	public double bestSimilarityScore = 0.0;
	
	/**
	 * Thematic label.
	 */
	public int semanticLabel;

	/**
	 * Path of the GT image associated to the segment.
	 */
	public String imgGtPath;
			
	/**
	 * Width of the GT image containing only the segment.
	 */
	public int imgGtWidth;

	/**
	 * Height of the crop image containing only the segment.
	 */
	public int imgGtHeight;
	
	/**
	 * Pixels of a segment contained in a GT image.
	 */
	public TreeSet<Integer> gtPoints;
	
	/**
	 * Bounding box.
	 */
	public int[] boundingBox = new int[4]; // minX maxX minY maxY 
	
	/**
	 * Width of the bounding box.
	 */
	public int bbWidth;

	/**
	 * Height of the bounding box.
	 */
	public int bbHeight;

	/**
	 * Pixels of the segment according to its bounding box.
	 */
	public TreeSet<Integer> bbPoints;

	/**
	 * Number of pixels that will be added around the bounding box when computing a crop of the segment.
	 */
	public  int margin;
	
	/**
	 * Number of all semantic labels of the study. in value[0]
	 * Size of all segments in the semantic class. in value[1]
	 * Only the first segment of a list should contain this value.
	 */
	public TreeMap<Integer, Double[]> ListOfAllSemanticLabels;
	
	/**
	 * If true. the number of segments of each class and the total size of each class are done.
	 * Only the first segment of the list should contain this value.
	 */
	public boolean sizeAndNbSet;
	
	/**
	 * Possible background color of the GT image.
	 *
	 */
	public enum BgColor{
		WHITE,
		BLACK
	}
	
	/**
	 * Background color of the GT image.
	 */
	public int bgColor;

	/**
	 * Index of the segment.
	 */
	public int index;

	/**
	 * A matrix containing the real valor of 'sigma_s(x)' i.e. < 0 for x in the segment, 0 on the border of the segment and > 0 outside the segment and contains also the value of 'Mu_alpha'.
	 */
	public double[][][] muMap;
	
	/**
	 * Create a segment of reference having a specified label.
	 * @param index : identification of the segment.
	 * @param semanticLabel : thematic label.
	 * @param gtWidth : width of the GT image.
	 * @param gtHeight : height of the GT image.
	 * @param gtImgPath : path of the GT image.
	 * @param bgColor : background color of the GT image.
	 * @param margin : number of pixels that will be added around the bounding box when computing a crop of the segment.
	 */
	public SegReference(int index, int semanticLabel, int gtWidth, int gtHeight, String gtImgPath, BgColor bgColor, int margin){
		
		this.index = index;
		this.semanticLabel = semanticLabel;
		this.imgGtWidth = gtWidth;
		this.imgGtHeight = gtHeight;
		this.imgGtPath = gtImgPath;
		this.initBgColor(bgColor);
		this.margin = margin;
		
		this.gtPoints = new TreeSet<Integer>();

	}
	
	/**
	 * Initialize the background color picked from the GT image.
	 * @param bgColor
	 */
	private void initBgColor(BgColor bgColor) {

		switch(bgColor){
			case WHITE:
				this.bgColor = 255;
				break;
			case BLACK:
				this.bgColor = 0;
				break;
			default :
				this.bgColor = 0;
				break;
		}
		
	}

	/**
	 * Prepare a segment of reference.
	 * @param index : identification of the segment.
	 * @param points ~ pixels contained in the segment of reference.
	 * @param semanticLabel : thematic label.
	 * @param bgColor : background color of the GT image.
	 */
	public SegReference(int index, TreeSet<Integer> points, int semanticLabel, BgColor bgColor) {
		
		this.index = index;
		this.gtPoints = points;
		this.semanticLabel = semanticLabel;
		this.initBgColor(bgColor);

	}
	
	/**
	 * Prepare a segment of reference.
	 * @param index : identification of the segment.
	 * @param imgGtPath : Path of an image containing only the segment having a white background.
	 * @param semanticLabel : thematic label
	 * @param bgColor : background color of the GT image.
	 * @param computeBB : if 'true', compute the bounding box and also fill a list of points having coordinates according to the bounding box.
	 */
	public SegReference(int index, String imgGtPath, int semanticLabel, BgColor bgColor, boolean computeBB){
		
		this.index = index;
		
		/* Remember the gt crop image path.
		 */
		this.imgGtPath = imgGtPath;
		
		/*
		 * Load the GT image.
		 */
		Image img = ImageLoader.exec(this.imgGtPath);
		this.imgGtWidth= img.xdim;
		this.imgGtHeight= img.ydim;

		/*
		 * Set the background color of the initial GT image containing the segments.
		 */
		this.initBgColor(bgColor);
		
		/*
		 * Get the list of integer points.
		 */
		this.gtPoints = new TreeSet<Integer>();
		for(int x=0;x<this.imgGtWidth;x++)
			for(int y=0;y<this.imgGtHeight;y++)
				if(img.getPixelXYByte(x, y) != this.bgColor)
					this.gtPoints.add((y * this.imgGtWidth)+x);
		
		/*
		 * Store a semantic label. 
		 */
		this.semanticLabel = semanticLabel;
		
		if(computeBB){
			
			/*
			 * Compute the bounding box.
			 */
			this.computeRegionBoundingBox();
			
		}
		
	}
	
	/**
	 * Get the set of pixels by considering their location in the bounding box.
	 * @return TreeSet of Integer.
	 */
	public TreeSet<Integer> getBbPoints(){

		return this.bbPoints;

	}
	
	/**
	 * Get the set of pixels by considering their location in the GT image.
	 * @return TreeSet of Integer.
	 */
	public TreeSet<Integer> getGtPoints(){

		return this.gtPoints;

	}
	
	/**
	 * Add a pixel from a GT image.
	 * @param point ~ pixel to add.
	 */
	public void addGtGtPoint(Integer point){
		
		this.gtPoints.add(point);
		
	}

	
	/**
	 * Retain the best matching node 'n' and the associated similarity score lambda(S_i).
	 * @param n
	 * @param lambda
	 */
	public void setBestMatchedNode(Node n, double lambda){
		
		this.bestN = n;
		this.bestSimilarityScore = lambda;
		
	}
	
	/**
	 * Compute the bounding box BB of the segment and translate the pixels' location according to this BB.
	 */
	public void computeRegionBoundingBox() {
		
		this.boundingBox = new int[4];

		/* Compute a bounding box */
		int minX = Integer.MAX_VALUE;
		int maxX = Integer.MIN_VALUE;
		int minY = Integer.MAX_VALUE;
		int maxY = Integer.MIN_VALUE;

		for(int p : this.gtPoints) {	

			int x= p % this.imgGtWidth;
			int y= p / this.imgGtWidth;

			if(x < minX)
				minX = x;
			if(x > maxX)
				maxX = x;
			if(y < minY)
				minY = y;
			if(y > maxY)
				maxY = y;
		}

		/* compute the real margin */
		int widthMargin = ((maxX - minX) * this.margin) / 100;
		int heightMargin = ((maxY - minY) * this.margin) / 100;
		
		/* Remember bounding box */
		int minXMargin = minX - widthMargin;
		int maxXMargin = maxX + widthMargin;
		int minYMargin = minY - heightMargin;
		int maxYMargin = maxY + heightMargin;
		
		if(minXMargin >= 0) this.boundingBox[0] = minXMargin;
		else this.boundingBox[0] = 0;
		if(maxXMargin <= this.imgGtWidth) this.boundingBox[1] = maxXMargin;
		else this.boundingBox[1] = this.imgGtWidth;
		if(minYMargin >= 0) this.boundingBox[2] = minYMargin;
		else this.boundingBox[2] = 0;
		if(maxYMargin <= this.imgGtHeight) this.boundingBox[3] = maxYMargin;
		else this.boundingBox[3] = this.imgGtHeight;
		
		/* Compute the width and the height of the bounding box */
		this.bbWidth = this.boundingBox[1] - this.boundingBox[0];
		this.bbHeight = this.boundingBox[3] - this.boundingBox[2];
		
		/* Update the pixel location values according to the bounding box */
		this.gtToBB();
		
	}
	
	/**
	 * Convert the value of each point from the GT image to the bounding box;
	 */
	public void gtToBB(){
		
		int xdecal = this.boundingBox[0];
		int ydecal = this.boundingBox[2];
		
		this.bbPoints = new TreeSet<Integer>();
		for(Integer pixel : this.gtPoints){
			
			int xGt = pixel % this.imgGtWidth; // X POSITION OF THE POINT IN THE GT IMAGE.
			int yGt = pixel / this.imgGtWidth; // Y POSITION OF THE POINT IN THE GT IMAGE.
			Integer newPixel = ((yGt - ydecal) * this.bbWidth) + (xGt - xdecal);
			this.bbPoints.add(newPixel);
			
		}
		
	}
	
	/**
	 * From an image GT, get each segments.
	 * @param gtImgPath : Path of the image GT having white background. 
	 * @param bgColor : background color of the GT image.
	 * @param alpha : >0 allow controlling the degree of uncertainty.
	 * @param margin : number of pixels to add around the bounding box of each segment.
	 * @param computeDistanceMap : if true, compute the matrix of distance map and containing also the membership function.
	 * @param showMaps : if true, show the distance and the mu maps.
	 * @return
	 */
	public static TreeMap<Integer, SegReference> getSegReferences(String gtImgPath, BgColor bgColor, double alpha, int margin, boolean computeDistanceMap, boolean showMaps){
		
		/* Load the image */
		TiffImage gtImg = new TiffImage(gtImgPath);
		Image gtPiaf = gtImg.getPiafImage();
		
		/* GT image width and height */
		int gtWidth = gtImg.getWidth();
		int gtHeight = gtImg.getHeight();
		
		/* set the background color value */
		int bgC;
		switch(bgColor){
			case WHITE:
				bgC = 255;
				break;
			case BLACK:
				bgC = 0;
				break;
			default :
				bgC = 0;
				break;
		}
		
		/* Set containing list of segments of reference */
		TreeMap<Integer, SegReference> listOfSegs = new TreeMap<Integer, SegReference>();
		
		/* get all the segments contained in the GT image including its background */
		SegmentByConnexityRaw sbc = new SegmentByConnexityRaw(gtImg);
		Image inputImg = sbc.runForFullImage();
		
		/* get all segments and leave the background */
		TreeMap<Integer, Double[]> semanticLabels = new TreeMap<Integer, Double[]>();
		for(int y=0; y < gtHeight; ++y){
			for(int x=0; x < gtWidth; ++x){
				
				int label = inputImg.getPixelXYInt(x, y); //(int) gtImg.getPixel(x, y, 0);//gtPiaf.getPixelXYByte(x, y);
				
				/* drop the background */
				if(gtPiaf.getPixelXYByte(x, y) != bgC){
					
					if(!listOfSegs.containsKey(label)){
						
						/* create a segment of reference object, add it to the list with its semantic label */
						int semanticLabel = gtPiaf.getPixelXYByte(x, y);
						listOfSegs.put(label, new SegReference(label, semanticLabel, gtWidth, gtHeight, gtImgPath, bgColor, margin));
						if(!semanticLabels.containsKey(semanticLabel)){
							Double[] val = new Double[4];
							val[0] = 0.0;
							val[1] = 0.0;
							val[2] = 0.0;
							val[3] = 0.0;
							semanticLabels.put(semanticLabel, val);
						}
						
					}	
					
					/* store the point ~ pixel in the segment of reference object */ 
					listOfSegs.get(label).addGtGtPoint((y * gtWidth) + x);	
					
				}
				
			}
		}
		
		/* Use the first segment to store the information about the semantic labels */
		listOfSegs.firstEntry().getValue().ListOfAllSemanticLabels = semanticLabels;
		
		/* Compute or not the distance map */
		if(computeDistanceMap)
			for(Map.Entry<Integer, SegReference> entry : listOfSegs.entrySet()){

				SegReference s = entry.getValue();
				s.muMap = DistanceMap.compute(s, alpha, showMaps);

			}
		
		int nbRegions = (int) inputImg.getProperty("nbRegions");
		Log.printLvl1("Segment of reference", "number of segments : "+ (nbRegions - 1));
		Log.printLvl1("Segment of reference", "number of semantic labels : "+ semanticLabels.size());
		
		return listOfSegs;
		
	}

}
